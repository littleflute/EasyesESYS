﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Content-Language" content="zh-cn" />
<meta name="author" content="EasyesESYS" />
<meta name="Copyright" content="Easyes.com.cn" />
<meta name="Description" content="易讯网络国内顶级企业建站软件开发商， 是目前中国ASP建站综合服务提供商之一。" />
<title>易讯建站管理系统(EasyesESYS)-http://www.Easyes.com.cn</title>
<link href="Images/about.css" rel="stylesheet" type="text/css" media="screen" />
</head>
<body>
<div id="main">
<!--#include file="Head.asp"-->
<div class="banner">
	<img alt="易讯建站系统" src="Images/apply_index.jpg" height="112" width="950" /></div>
<div class="wrap">
<div class="sidebar">
<dl class="list_menu">
<dt>关于我们</dt>
<dd><a href="About.asp">公司简介</a></dd>
<dd><a href="About1.asp">联系我们</a></dd>
<dd class="on"><a href="About2.asp">招贤纳士</a></dd>
<dd><a href="About3.asp">企业文化</a></dd>
</dl>
</div>
<div class="const_main">
<div class="gb_mode_title"><h3>招贤纳士</h3><div class="fun_r"><a href="#">更多</a></div></div>
<div class="main_txt">
<!--招贤纳士:开始-->
软件工程师 - 职责描述 <br />
创新工场的软件工程师将参与开发出最有市场价值又实用酷炫的产品，我们专注于在互联网、移动互联网和云计算等领域，这将要求计算机科学的扎实基础以及丰富的实际编程经验。软件工程师需要有热情与兴趣利用计算机来开发创新的产品，例如：<br />
1、编写服务器端和web端的程序，开发出强壮的大规模互联网应用或迅速的创建产品原型等；<br />
2、利用分布式系统，操作系统，网络和大规模存储技术创建云计算的系统、平台与架构等；<br />
3、手机等移动终端的程序开发和移动互联网应用的建设等。<br />
&nbsp;<br />
软件工程师 - 职位要求 <br />
1、计算机科学或相关技术学科的学士，硕士或博士学位（或同等学历）；<br />
2、扎实的计算机知识基础，丰富的实际代码编写经验；<br />
3、丰富的C，C + +或Java 编程经验或有Python, Perl或JavaScript/AJAX技术方面的编码技能；<br />
4、有实际的互联网/移动/系统软件设计与开发的深厚经验者优先；<br />
5、热爱设计编写程序，对IT行业充满激情，有独立的用代码解决问题的能力；</p>
<!--招贤纳士:结束-->
</div>			
</div>
</div>
<!--#include file="Bottom.asp"-->
</div>
</body>
</html>
