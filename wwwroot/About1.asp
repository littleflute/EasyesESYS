﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Content-Language" content="zh-cn" />
<meta name="author" content="EasyesESYS" />
<meta name="Copyright" content="Easyes.com.cn" />
<meta name="Description" content="易讯网络国内顶级企业建站软件开发商， 是目前中国ASP建站综合服务提供商之一。" />
<title>易讯建站管理系统(EasyesESYS)-http://www.Easyes.com.cn</title>
<link href="Images/about.css" rel="stylesheet" type="text/css" media="screen" />
</head>
<body>
<div id="main">
<!--#include file="Head.asp"-->
<div class="banner"><img alt="易讯建站系统" src="Images/About.jpg" /></div>
<div class="wrap">
<div class="sidebar">
<dl class="list_menu">
<dt>关于我们</dt>
<dd><a href="About.asp">公司简介</a></dd>
<dd class="on"><a href="About1.asp">联系我们</a></dd>
<dd><a href="About2.asp">招贤纳士</a></dd>
<dd><a href="About3.asp">企业文化</a></dd>
</dl>
</div>
<div class="const_main">
<div class="gb_mode_title"><h3>联系我们</h3><div class="fun_r"><a href="#">更多</a></div></div>
<div class="main_txt">
<!--联系我们:开始-->
<p>&nbsp;</p>
<p>&nbsp;</p>
地&nbsp; 址：中国北京市海淀区清华科技园C座<br />
邮&nbsp; 编：100084<br />
网&nbsp; 址：<a href="http://www.innovation-works.com">www.innovation-works.com</a><br />
E-mail：<a href="mailto:contact@innovation-works.com">contact@innovation-works.com</a>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<!--联系我们:结束-->
</div>			
</div>
</div>
<!--#include file="Bottom.asp"-->
</div>
</body>
</html>
