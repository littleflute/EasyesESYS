﻿<%@language=vbscript codepage=65001%>
<%Option Explicit%>
<%
Response.CodePage=65001
Response.Charset="UTF-8"
Session.CodePage = 65001
'**************************************************************
' Software Name: EasyesESYS
' Web: http://www.Easyes.com.cn
'**************************************************************
Response.buffer=true	
Const PurviewLevel=3    '操作权限 1超级用户2高级管理员3信息录入员
%>
<!--#include file="Config/ChkPurview.asp"-->
<!--#include file="Config/Config.asp"-->
<!--#include file="Config/Conn.asp"-->
<!--#include file="Config/function.asp"-->
<!--#include file="Config/ClassList_Fun.asp"--><html>
<head>
<title>单页单页管理</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="Version" content="<%=SystemVer%>">
<link href="Images/Style.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="Config/Button_Skin.js"></script>
<script type="text/javascript" src="Config/ClassTree.js"></script>
<script type="text/javascript" src="Config/Tooltip.js"></script>
</head>
<body>
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="tabTitleBar" height="26"><tr><td width="20" align="center"><img border="0" src="Images/file.png"></td><td>当前位置 &gt; <a href="Admin_Centont.asp">单页资源管理</a></td></tr></table><%
dim Action,ParentID,i,FoundErr,ErrMsg
Action=trim(request("Action"))
if Action="Add" then
	call AddCentont()
elseif Action="SaveAdd" then
	call SaveAdd()
elseif Action="Modify" then
	call Modify()
elseif Action="SaveModify" then
	call SaveModify()
else
	call main()
end if
if FoundErr=True then
	call WriteErrMsg(ErrMsg)
end if
call CloseConn()
Sub main()
%><table width="100%" border="0" cellpadding="3" cellspacing="1" class="tabBgColor">
<tr> 
<td width="100%" bgcolor="#f6f6f6" class="tdbg">当前位置：<%
Dim ParentID
	ParentID=trim(request("ParentID"))
if ParentID="" then
	ParentID=0
else
	ParentID=CLng(ParentID)
end if
	Response.write ""&GetClassPath1(ParentID)&""
%></td>
</tr>
<tr> 
<td width="100%" bgcolor="#f6f6f6" class="tdbg"><%=GetClassPath2(ParentID,1)%></td>
</tr>
</table>
<%
Dim Rs
    Set Rs=Server.CreateObject("ADODB.RecordSet")
Dim Sql
   	Sql="SELECT * From [All_Content] Where ClassID In ("& ParentID&AllChildClass(ParentID) &") Order By UpTime DESC"
 	Rs.PageSize=20
	Rs.CacheSize=Rs.PageSize
    Rs.Open Sql,Conn,1,1
Dim CurrentPage
    If Request("CurrentPage")="" Then
        CurrentPage=1
    Else
        CurrentPage=Request("CurrentPage")
    End If    
    If Not(Rs.Eof And Rs.Bof) Then
        Rs.AbsolutePage=CurrentPage
    End If
%><table border="0" cellpadding="2" cellspacing="1" class="tabBgColor" width="100%">
<tr> 
<td height="25" align="center" class="tabheadfont" width="40">ID</td>
<td height="25" align="center" class="tabheadfont" width="30">选择</td>
<td height="25" align="center" class="tabheadfont">单页资源名称</td>
<td width="120" height="22" align="center" class="tabheadfont">所属主类别</td>
<td width="100" height="22" align="center" class="tabheadfont">添加时间</td>
<td width="100" height="22" align="center" class="tabheadfont">更新时间</td>
<td align="center" class="tabheadfont" width="40">审核</td>
<td align="center" class="tabheadfont" width="80">操作</td>
</tr>
<%
  Dim I
  For I=1 To Rs.PageSize
      If Rs.Eof Then
        Exit For
    End If
%><tr class="tdbg">
<td align="center" width="40"><%=rs("ClassID")%></td>
<td align="center"><input type="checkbox" name="Id" value="<%=Rs("ID")%>" title="记录编号：<%=Rs("ID")%>" HaveChecked="<%=CBool(Rs("Passed"))%>"></td>
<td><img border="0" src="Images/arrow_r.gif"><%Response.write "&nbsp;<a href='Admin_Centont.asp?Action=Modify&ID=" & rs("ID") &"'>"&Rs("Title")&"</a>"%></td>
<td align="center"><%=rs("ClassName")%></td>
<td align="center"><label title="更新时间:<%=FormatDateTime(Rs("UpTime"),1)%>" style="cursor:hand"><font color="#808080"><%=Rs("AddTime")%></font></label></td>
<td align="center"><label title="更新时间:<%=FormatDateTime(Rs("UpTime"),1)%>" style="cursor:hand"><font color="#808080"><%=Formatdatetime(Rs("UpTime"),1)%></font></label></td>
<td align="center"><%
    If CBool(Rs("Passed")) Then
        Response.write("<a href=""Admin_CentontMdy.asp?Work=CheckReco&Id="& Rs("ID") &""">已审</a>")
    Else
        Response.write("<a href=""Admin_CentontMdy.asp?Work=CheckReco&Id="& Rs("ID") &"""><font color='green'>未审</font></a>")
    End If
%></td>
<td align="center"><%
	response.write "<a href='Admin_Centont.asp?Action=Modify&ID=" & rs("ID") &"'>编辑</a>&nbsp;"
	response.write "<a href='Admin_CentontMdy.asp?Work=DelReco&ID=" & rs("ID") & "' onClick=""return confirm('确定要删除此资源吗？');"">删除</a>"
%></td>
</tr><%
  Rs.MoveNext
  Next
If ParentID<>0 Then
	If Rs.Eof And Rs.Bof Then
		Response.write "<tr><td colspan=""8"" class=""tdbg"">"
		Response.write "<input name=""Submit32"" type=""button"" class=""button01-out"" value=""设定内容"" onClick=""window.location='Admin_Centont.asp?Action=Add&ParentID="&ParentID&"'"" title=""在当前单页设置资源内容"">"
		Response.write "</td></tr>"
	End If
End If
%></table> 
<table border="0" cellpadding="2" cellspacing="0" width="100%">
<tr> 
<td align="right" bgcolor="#f6f6f6">
<script type="text/javascript" src="Config/PageList.js"></script>
<script type="text/javascript" language="JavaScript">
	<!--
	var plb = new PageListBar('plb',<%=Rs.PageCount%>,<%=CurrentPage%>,'&ParentID=<%=ParentID%>',20);
	document.write(plb);
	//-->
</script>
</td>
</tr>
</table><%
Rs.Close
Set Rs=Nothing
End sub
Sub AddCentont
Call ajax()
%><form name="form1" method="post" action="Admin_Centont.asp?Work=SaveAddReco" class="required-validate">
<input name="Action" type="hidden" id="Action" value="SaveAdd">
<input name="radioBoxItem" type="hidden" ID="radioBoxItem" value="<%=trim(request("ParentID"))%>">
<table width="100%" border="0" cellpadding="2" cellspacing="1" class="tabBgColor">
<tr> 
<td colspan="2" align="center" class="tabheadfont">设定单页资源内容</td>
</tr>
<tr> 
<td align="right" class="tdbgfont">单页标题:</td>
<td class="tdbg">
<input name="Title" type="text" class="input_text required" size="30" ></td>
</tr>
<tr>
<td align="right" class="tdbgfont" valign="top">资源特性:</td>
<td class="tdbg" valign="top"><script>
var root3
root3=CreateRoot("myTree3","·请选择[资源特性]")
<%Call SpecialityList_Add(0,1)%>
</script></td>
</tr>
<tr> 
<td class="tdbgfont" colspan="2">
<font color="#0000FF">单页内容</font>: </td>
</tr>
<tr> 
<td valign="top" colspan="2" class="tdbgfont">
<%
Response.write"<textarea name=""Content"" style=""display:none""></textarea>"&chr(13)
call Admin_HtmlEditor()
%>
</td>
</tr>
<tr>
<td class="tdbgfont" align="right">简单描述:</td>
<td class="tdbgfont">
<textarea rows="4" name="Rxplain" cols="84" class="input_text required"></textarea></td>
</tr>
<tr class="tdbg"> 
<td align="right"></td>
<td>
<input name="SaveAddButton" id="SaveAddButton" class="button02-out" type="submit" value=" 保 存 ">
<input name="Submit2" class="button02-out" type="reset" value=" 还 原 ">
<input name="Submit2" type="reset" class="button02-out" value="返 回" onClick="window.location.href='Admin_Centont.asp'"></td>
</tr>
</table>
</form><%
End Sub
Sub Modify()
Call ajax()
	dim ID
	ID=trim(request("ID"))
	if ID="" then
		FoundErr=True
		ErrMsg=ErrMsg & "·请指定要修改单页单页内容！\n"
		exit sub
	end if
Dim rs
	set rs=conn.execute("select ID,Title,Content,Rxplain,ClassID,SpecialID from Easyes_Content where ID=" &ID)
	if rs.bof and rs.eof then
		FoundErr=True
		ErrMsg=ErrMsg & "·找不到指定单页单页内容！\n"
	else
%><form name="form2" method="post" action="?Work=SaveAddReco" class="required-validate">
<table width="100%" border="0" cellpadding="2" cellspacing="1" class="tabBgColor">
<tr><td colspan="2" align="center" class="tabheadfont">编辑单页资源</td></tr>
<tr> 
<td class="tdbgfont" colspan="2">
<font color="#0000FF">单页内容</font>: <span style="cursor:hand" Id="ShowHiddenHtmlEdit" Title="显示/隐藏" onClick="if(trNewsContent.style.display==''){trNewsContent.style.display='none';ShowHiddenHtmlEdit.innerHTML='[显示]'}else{trNewsContent.style.display='';ShowHiddenHtmlEdit.innerHTML='[隐藏]'}">[隐藏]</span></td>
</tr>
<tr id="trNewsContent"> 
<td valign="top" colspan="2" class="tdbgfont">
<%
Response.write"<textarea name=""Content"" style=""display:none"">"&Server.HTMLEncode(Rs("Content"))&"</textarea>"&chr(13)
call Admin_HtmlEditor()
%></td>
</tr>
<tr>
<td class="tdbgfont" align="right">简单描述:</td>
<td class="tdbgfont"><textarea rows="5" name="Rxplain" cols="84" class="input_text  required"><%=rs("Rxplain")%></textarea></td>
</tr>
<tr class="tdbg"> 
<td align="right">
<input name="ID" type="hidden" id="ClassID" value="<%=rs("ID")%>">
<input name="Action" type="hidden" id="Action" value="SaveModify">
</td>
<td>
<input name="SaveAddButton" id="SaveAddButton" class="button02-out" type="submit" value=" 保 存 ">
<input name="Submit2" class="button02-out" type="reset" value=" 还 原 ">
<input name="Submit2" type="reset" class="button02-out" value="返 回" onClick="window.location.href='Admin_Centont.asp'">
</td>
</tr>
</table></form><%
End if
	rs.close
	set rs=nothing
End sub
%>
</body>
</html><%
Sub SaveAdd() '添加
	Dim Title,Content,ClassID,Special,Rxplain
	ClassID=Request.Form("radioBoxItem")
	Special=Replace(Request("Speciality")," ","")
	Title=trim(request("Title"))
	Content=request("Content")
	Rxplain=request("Rxplain")
if Content="" then
	FoundErr=True
	ErrMsg=ErrMsg & "·请输入单页内容！\n"
End if
if FoundErr=True then
	Exit sub
End if
    Dim Sql
        Sql="Select Top 1 * From Easyes_Content where ClassID=" & ClassID & ""
    Dim Rs
    Set Rs=Server.CreateObject("ADODB.RecordSet")
        Rs.Open Sql,Conn,1,3
If not(rs.bof and rs.eof) then
		FoundErr=True
		ErrMsg=ErrMsg & "·对不起单页模块内容不能重复添加，请返回查看内容是否已经存在！\n"
End if
    Rs.AddNew
    Rs("ClassID")= ClassID
	Rs("SpecialID")=Special
    Rs("Title")= Title
If sSaveFileSelect="Yes" Then
	Rs("Content")=ReplaceRemoteUrl(Content,sSaveFilePath,sFileExt)
Else
   	Rs("Content")= Content
End If
	Rs("Rxplain")=Rxplain
If AddChecked="Yes" Then
	Rs("Passed")=False
Else
	Rs("Passed")=True
End If
    Rs("upTime")= Now
    Rs.Update
    Rs.Close
    Set Rs=Nothing
	Response.Redirect "Admin_Centont.asp?ParentID="&ClassID&""   
End sub
sub SaveModify()  '修改
	Dim Content,ID,ClassID,Rxplain
	ID=trim(request("ID"))
	Content=request("Content")
	Rxplain=request("Rxplain")
	if Content="" then
		FoundErr=True
		ErrMsg=ErrMsg & "·请输入单页单页内容！\n"
	end if
	if FoundErr=True then
		Exit sub
	End if
    Dim Sql
        Sql="Select * From Easyes_Content Where ID=" & ID
    Dim Rs
    Set Rs=Server.CreateObject("ADODB.RecordSet")
        Rs.Open Sql,Conn,1,3
	If Rs.Eof And Rs.Bof Then
		FoundErr=True
		ErrMsg=ErrMsg & "·找不到修改的单页单页内容\n"
		rs.close
		set rs=Nothing
    	Exit sub
	end if
	If sSaveFileSelect="Yes" Then
		Rs("Content")=ReplaceRemoteUrl(Content,sSaveFilePath,sFileExt)
	Else
    	Rs("Content")=Content
	End If
	    Rs("Rxplain")=Rxplain
	If AddChecked="Yes" Then
	   	Rs("Passed")=False
	Else
	   	Rs("Passed")=True
	End If
	   	Rs("upTime")=Now
		rs.update
			ClassID=rs("ClassID")
		rs.Close
		set rs=Nothing
		Response.Redirect "Admin_Centont.asp?ParentID="&ClassID&""  
End sub
%>