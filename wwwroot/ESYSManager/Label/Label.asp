﻿<!--#include file="StringClass.asp"-->
<!--#include file="Label_Function.asp"-->
<!--#include file="TemplateFunction.asp"-->
<%
'**************************************************************
' Software Name: EasyesESYS
' Web: http://www.Easyes.com.cn
'**************************************************************
'/★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★
'/栏目标签
'/★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★
'////////////////////////////////////////
'//【功能】实际替换标签的类（栏目列表）
Class TemplateSkin_Class   'Lable_Function.asp
Public ESLable,ESClass,ESParent,ESlock,ESNList,ESulS,ESulE,ESliS,ESliE,OrderField,OrderType

'//【功能】获取栏目列表信息
Function Easyes_Class(strColumn)
'//转换标签中HTML“{}”为“<>”和“()”为“""”
'//Lable_Function.asp
ESulS=LableEncode(ESulS)
ESulE=LableEncode(ESulE)
ESliS=LableEncode(ESliS)
ESliE=LableEncode(ESliE)
'//判断赋值方法,是否浏览器赋值进入
If ESParent=1 and ESlock=0 Then
Dim UrlID
	UrlID=Request.ServerVariables("QUERY_STRING")   '获取来源地址
	UrlID=Replace(UrlID,".html","")
Dim ClassID  '判读数组
	If UrlID<>"" Then
		UrlID=split(UrlID,"_")
		ClassID=UrlID(1)
	End If
	If ClassID<>"" Then
		ESClass=ClassID
	End If
End If
'//判断ID是否空和安全数值
If isnumeric(ESClass)=True Then
 '//子分类判断
If ESParent=1 Then
	ESClass="ParentID="&ESClass&" "
Else
	ESClass="ClassID="&ESClass&" "
End If
'//如果用ID排序则OrderField为空
If OrderField="ClassID" Then 
	OrderField="ClassID"
Else
	OrderField=""&OrderField&" "&OrderType&",ClassID"
End If
'//开始读库
Dim rs
	Set rs=Server.CreateObject("ADODB.RecordSet")
Dim Sql
    Sql = "select ClassID From [Easyes_Class] Where "&ESClass&" Order By "&OrderField&" "&OrderType&""
Dim Template
	rs.open sql,conn,1,1
		Template = Template& ""&ESulS&""
Dim n
	n=0
	Do while not rs.eof
	If (n Mod ESNList) = 0 Then Template = Template& ""&ESliS&""
		Template = Template& ""&FormatLable_Class(ESLable,rs("ClassID"))&""  '读取模板ID和替换本信息的ID
	n = n + 1
	If (n Mod ESNList) = 0 Then Template = Template& ""&ESliE&""
	rs.movenext
	loop
		Template = Template& ""&ESulE&""
		rs.close
		set rs = nothing
	Easyes_Class = Template
End If     '判断ID非法性
End Function
End Class

'//#################################################
'//栏目资源替换处理
'//#################################################
Function FormatLable_Class(LabelID,ID)  '替换替换处理
Dim Formatrs
	Set Formatrs=Server.CreateObject("ADODB.RecordSet")
Dim FormatSql
	FormatSql = "select ClassID,ClassUrl,ClassName,ClassContent From [Easyes_Class] Where ClassId="&ID
	Formatrs.open Formatsql,Conn,1,1
Dim FormatLable
If FormatRs.Eof And FormatRs.Bof Then
    FormatRs.Close
    Set FormatRs=Nothing
    FormatLable_News="·超出范围！"
    Exit Function
End If
'调用类(StringClass.asp)
Dim StrClass
Set StrClass = New ESYS_StringClass
'读取自定义标签(TemplateFunction.asp) 
Dim Template
	Session("buffer_Template")=""
	Template="" 
	Template=GetLableTemplate(LabelID)
    If Trim(Template)="" Or ISNULL(Template) Then
         Formatrs.Close
         Set Formatrs=Nothing
         FormatLable_News=False
        Exit Function
    End If
 '开始替换   
Dim Asp_Lable
	Asp_Lable="{\$Class_Title\$}"   '标题
	Template = StrClass.ReplaceTest(Asp_Lable,Template,CStr(""&Formatrs("ClassName")))
	Asp_Lable="{\$Class_Url\$}"  '栏目连接地址
	Template = StrClass.ReplaceTest(Asp_Lable,Template,""&CStr(""&Formatrs("ClassUrl"))&"?1_"&Formatrs("ClassID")&".html")
	Asp_Lable="{\$Class_Content\$}"   '标题
	Template = StrClass.ReplaceTest(Asp_Lable,Template,CStr(""&Formatrs("ClassContent")))	
	SystemLable Template	'系统标签(TemplateFunction.asp) 
	Template = TemplateTags(Template)
	FormatLable_Class=Template
	Formatrs.close
	Set Formatrs = nothing
End Function


'/★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★
'/文章标签
'/★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★
'////////////////////////////////////////
'//【功能】实际替换标签的类（常规模块）
'//#################################################
'//常规资源--列表没有分页
'//#################################################
Class TemplateSkin_News
Public ESLable,ESClass,ESParent,ESLock,ESSpecial,ESNPage,ESNList,ESulS,ESulE,ESliS,ESliE,OrderField,OrderType
'//【功能】获取常规的列表信息
Function Easyes_NewsList(strColumn)	
'//转换标签中HTML“{}”为“<>”和“()”为“""”
'//Lable_Function.asp
ESulS=LableEncode(ESulS)
ESulE=LableEncode(ESulE)
ESliS=LableEncode(ESliS)
ESliE=LableEncode(ESliE)
 '判断赋值方法,是否浏览器赋值进入
If ESParent=1 and ESlock=0 Then
Dim UrlID
	UrlID=Request.ServerVariables("QUERY_STRING")   '获取来源地址
	UrlID=Replace(UrlID,".html","")
Dim ClassID  '判读数组(页数和栏目ID)
	If UrlID<>"" Then
		UrlID=split(UrlID,"_")
		ClassID=UrlID(1)
	End If
	If ClassID<>"" Then
		ESClass=ClassID
	End If
End If
'判断ID是否空和安全数值
If isnumeric(ESClass)=True Then
'读取栏目
	If ESParent=0 Then
		ESClass="ClassID="&ESClass&" and "
	Else
		ESClass="ClassID In ("& ESClass&AllChildClass(ESClass) &") and"
	End If
'读取特性
IF ESSpecial=0 Then
	ESSpecial=""
Else
	ESSpecial="Instr(','+SpecialID+',',',"&ESSpecial&",')<>0 and"
End If
'排序方法
If OrderField="ArticleID" Then 
	OrderField="ArticleID"
Else
	OrderField=""&OrderField&" "&OrderType&",ArticleID"
End If
'开始查询数据
Dim rs
	Set rs=Server.CreateObject("ADODB.RecordSet")
Dim Sql
    Sql = "select top "&ESNPage&" ArticleId From [All_News] Where "&ESSpecial&" "&ESClass&" Passed=True Order By "&OrderField&" "&OrderType&""    
	rs.open Sql,conn,1,1
Dim Template
		Template = Template& ""&ESulS&""
Dim n
	n=0
	Do while not rs.eof
	If (n Mod ESNList) = 0 Then Template = Template& ""&ESliS&""
		Template = Template& ""&FormatLable_News(ESLable,rs("ArticleId"))&""  '读取模板ID和替换本信息的ID(TemplateFunction.asp) 
	n = n + 1
	If (n Mod ESNList) = 0 Then Template = Template& ""&ESliE&""
	rs.movenext
	loop
		Template = Template& ""&ESulE&""
		rs.close
		set rs = nothing
	Easyes_NewsList = Template
End If     '判断ID非法性
End Function
End Class

'//#################################################
'//常规资源--列表有分页
'//#################################################
Class TemplateSkin_NewsN
Public ESLable,ESClass,ESParent,ESLock,ESSpecial,ESSearch,ESNPage,ESNList,ESulS,ESulE,ESliS,ESliE,ESPageS,ESPageE,OrderField,OrderType
'//【功能】获取常规的列表信息
Function Easyes_NewsNList(strColumn)
'//转换标签中HTML“{}”为“<>”和“()”为“""”
'//Lable_Function.asp
ESulS=LableEncode(ESulS)
ESulE=LableEncode(ESulE)
ESliS=LableEncode(ESliS)
ESliE=LableEncode(ESliE)
ESPageS=LableEncode(ESPageS)
ESPageE=LableEncode(ESPageE)
ESPageS=LableEncode(ESPageS)
ESPageE=LableEncode(ESPageE)
 '判断赋值方法,是否浏览器赋值进入
Dim UrlID
	UrlID=Request.ServerVariables("QUERY_STRING")   '获取来源地址
	UrlID=Replace(UrlID,".html","")
Dim StrID,StrI
	StrID=split(UrlID,"_")	
If UBound(StrID)=1 or UBound(StrID)=2 then '判断分页和分类ID
		CurrentPage=StrID(0)
	If ESlock=0 Then
		ESClass=StrID(1)
	End If	
End If
'判断ID是否空和安全数值
If isnumeric(CurrentPage)=True and isnumeric(ESClass)=True Then
'读取栏目
Dim PageID
	PageID=ESClass  '分页ID
	If ESParent=0 Then
		ESClass="ClassID="&ESClass&" and "
	Else
		ESClass="ClassID In ("& ESClass&AllChildClass(ESClass) &") and"
	End If	
'读取特性
IF ESSpecial=0 Then
	ESSpecial=""
Else
	ESSpecial="Instr(','+SpecialID+',',',"&ESSpecial&",')<>0 and"
End If
'是否开启搜索
IF ESSearch=1 Then
	ESSearch=""
Dim KeyWord,PageKeyWord  '判断关键字
	If UBound(StrID)=2 Then
		PageKeyWord="_"&StrID(2)&""
		ESSearch="Title Like '%"&StrID(2)&"%' and"
	End If	
	   	KeyWord=Checkstr(jencode(trim(Request.form("KeyWord"))))
	If KeyWord<>"" Then
		PageKeyWord="_"&KeyWord&""
		ESSearch="Title Like '%"&KeyWord&"%' and"
	End If
Else
	ESSearch=""
End If
'排序方法
If OrderField="ArticleID" Then 
	OrderField="ArticleID"
Else
	OrderField=""&OrderField&" "&OrderType&",ArticleID"
End If
'开始查询数据
Dim rs
	Set rs=Server.CreateObject("ADODB.RecordSet")
Dim Sql
    Sql = "Select ArticleId From [All_News] Where "&ESSearch&" "&ESSpecial&" "&ESClass&" Passed=True Order By "&OrderField&" "&OrderType&""
    Rs.Open Sql,Conn,1,1
Dim Template
'翻页处理开始
    Rs.PageSize=ESNPage  
	Rs.CacheSize=Rs.PageSize
    If CurrentPage="" Then
        CurrentPage=1
    Else
        CurrentPage=CurrentPage
    End If
    If Not(Rs.Eof And Rs.Bof) Then
        Rs.AbsolutePage=CurrentPage
    End If
'翻页处理结束
		Template = Template& ""&ESulS&""
Dim N,I
	N=0
  For I=1 To Rs.PageSize
    If Rs.Eof Then
        Exit For
    End If
	If (n Mod ESNList) = 0 Then Template = Template& ""&ESliS&""
		Template = Template& ""&FormatLable_News(ESLable,rs("ArticleId"))&""  '读取模板ID和替换本信息的ID
	N = N + 1
	If (n Mod ESNList) = 0 Then Template = Template& ""&ESliE&""
  	Rs.MoveNext
  	Next
		Template = Template& ""&ESulE&""&chr(13)
		Template = Template& ""&ESPageS&""&chr(13)
		Template = Template& "<script src="""&SiteFile&"ESYSPassport/PageList.js""></script>"&chr(13)
		Template = Template& "<script type=""text/javascript"" language=""JavaScript"">"&chr(13)
		Template = Template& "	<!--"&chr(13)
		Template = Template& "	var plb = new PageListBar('plb',"&Rs.PageCount&","&CurrentPage&",'_"&PageID&""&PageKeyWord&".html',20);"&chr(13)
		Template = Template& "	document.write(plb);"&chr(13)
		Template = Template& "	//-->"&chr(13)
		Template = Template& "</script>"&chr(13)
		Template = Template& ""&ESPageE&""&chr(13)
	Rs.Close
	Set Rs=Nothing
    Easyes_NewsNList = Template
End If     '判断ID非法性
End Function
End Class

'//#################################################
'//常规资源--内容查看
'//#################################################
Class TemplateSkin_NewsPage
Public ESLable,ESClass
'//【功能】获取常规的列表信息
Function Easyes_NewsPage(strColumn)	
Dim UrlID
	UrlID=Request.ServerVariables("QUERY_STRING")   '获取来源地址
	UrlID=Replace(UrlID,".html","")
If isnumeric(UrlID)=True Then   '判断ID非法性
IF ESClass=0 Then  '子分类判断
	ESClass=""
Else
	ESClass="ClassID="&ESClass&" and "
End If
Dim rs
	Set rs=Server.CreateObject("ADODB.RecordSet")
Dim Sql
    Sql = "select ArticleID From [All_News] Where "&ESClass&" Passed=True and ArticleID="&UrlID&""
Dim Template
	rs.open sql,conn,1,1
If Not(rs.eof or rs.bof) then
	Template = Template& ""&FormatLable_News(ESLable,rs("ArticleId"))&""  '读取模板ID和替换本信息的ID
	rs.movenext
End If
	rs.close
	set rs = nothing
	Easyes_NewsPage = Template
End If     '判断ID非法性
End Function
End Class
'//#################################################
'//常规资源替换处理
'//#################################################
Function FormatLable_News(LabelID,ID)  '替换处理
Dim Formatrs
	Set Formatrs=Server.CreateObject("ADODB.RecordSet")
Dim FormatSql
	FormatSql = "select ArticleId,ClassID,TitleUrl,ClassUrl,ClassName,PageUrl,Title,Content,Author,Editor,Images,Text,Count,TitleFontType,TitleFontColor,UpdateTime,Date From [All_News] Where ArticleId="&ID
	Formatrs.open Formatsql,Conn,1,1
Dim FormatLable
If FormatRs.Eof And FormatRs.Bof Then
    FormatRs.Close
    Set FormatRs=Nothing
    FormatLable_News="·超出范围！"
    Exit Function
End If
'调用类
Dim StrClass
Set StrClass = New ESYS_StringClass
'读取自定义标签    'TemplateFunction.asp
Dim Template
	Session("buffer_Template")=""
	Template="" 
	Template=GetLableTemplate(LabelID)
    If Trim(Template)="" Or ISNULL(Template) Then
         Formatrs.Close
         Set Formatrs=Nothing
         FormatLable_News=False
        Exit Function
    End If
Dim Asp_Lable  '开始替换
	Asp_Lable="{\$News_ID\$}"   'ID资源
	Template = StrClass.ReplaceTest(Asp_Lable,Template,CStr(""&Formatrs("ArticleID")))
	Asp_Lable="{\$News_TitleHtml:(\d+)\$}"   '标题带参数
	Template = ReplaceTestDiy(Asp_Lable,Template,CStr(""&juncode(Formatrs("Title"))),Formatrs("TitleFontColor"),Formatrs("TitleFontType"))
	Asp_Lable="{\$News_Title\$}"   '标题
	Template = StrClass.ReplaceTest(Asp_Lable,Template,CStr(""&juncode(Formatrs("Title"))))
If Formatrs("TitleUrl")<>"" Then
	Asp_Lable="{\$News_PageUrl\$}"   '阅读地址(标题连接)
	Template = StrClass.ReplaceTest(Asp_Lable,Template,CStr(""&Formatrs("TitleUrl")))
Else
	Asp_Lable="{\$News_PageUrl\$}"   '阅读地址
	Template = StrClass.ReplaceTest(Asp_Lable,Template,""&CStr(""&Formatrs("PageUrl"))&"?"&Formatrs("ArticleID")&".html")
End If
	Asp_Lable="{\$News_ClassUrl\$}"  '栏目连接地址
	Template = StrClass.ReplaceTest(Asp_Lable,Template,""&CStr(""&Formatrs("ClassUrl"))&"?1_"&Formatrs("ClassID")&".html")
	Asp_Lable="{\$News_Content\$}"  '内容
	Template = StrClass.ReplaceTest(Asp_Lable,Template,CStr(""&Formatrs("Content")))
	Asp_Lable="{\$News_Author\$}"  '作者
	Template = StrClass.ReplaceTest(Asp_Lable,Template,CStr(""&juncode(Formatrs("Author"))))
	Asp_Lable="{\$News_Editor\$}"  '责任编辑
	Template = StrClass.ReplaceTest(Asp_Lable,Template,CStr(""&Formatrs("Editor")))
	Asp_Lable="{\$News_Images\$}"  '图片
	Template = StrClass.ReplaceTest(Asp_Lable,Template,CStr(""&Formatrs("Images")))
	Asp_Lable="{\$News_ShortContent\$}"  '简述
	Template = StrClass.ReplaceTest(Asp_Lable,Template,CStr(""&Formatrs("Text")))
	Asp_Lable="{\$News_AddTime\$}" '添加时间
	Template = StrClass.ReplaceTest(Asp_Lable,Template,""&StrClass.FormatMyDate(Formatrs("Date"),"{m}/{d}"))
	Asp_Lable="{\$News_UpTime\$}"  '更新时间
	Template = StrClass.ReplaceTest(Asp_Lable,Template,""&StrClass.FormatMyDate(Formatrs("UpdateTime"),"{m}/{d}"))
	Asp_Lable="{\$News_Count1\$}"  '阅读次数(不增加)
	Template = StrClass.ReplaceTest(Asp_Lable,Template,""&Formatrs("Count")&"")
	Asp_Lable="{\$News_Count2\$}"  '阅读次数(增加)
	Template = StrClass.ReplaceTest(Asp_Lable,Template,"<script src="""&SiteFile&"ESYSPassport/Count.asp?Display=News&ID="&Formatrs("ArticleId")&"""></script>")
	Asp_Lable="{\$News_ClassName\$}"  '分类名称
	Template = StrClass.ReplaceTest(Asp_Lable,Template,CStr(""&Formatrs("ClassName")))
	SystemLable Template	'系统标签
	Template = TemplateTags(Template)
	FormatLable_News=Template
	Formatrs.close
	Set Formatrs = nothing
End Function

'/★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★
'/产品标签
'/★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★
'////////////////////////////////////////
'//【功能】实际替换标签的类（产品模块）
'//#################################################
'//产品资源--列表没有分页
'//#################################################
Class TemplateSkin_Prod
Public ESLable,ESClass,ESParent,ESLock,ESSpecial,ESNPage,ESNList,ESulS,ESulE,ESliS,ESliE,OrderField,OrderType
'//【功能】获取常规的列表信息
Function Easyes_ProdList(strColumn)	
'//转换标签中HTML“{}”为“<>”和“()”为“""”
'//Lable_Function.asp
ESulS=LableEncode(ESulS)
ESulE=LableEncode(ESulE)
ESliS=LableEncode(ESliS)
ESliE=LableEncode(ESliE)
 '判断赋值方法,是否浏览器赋值进入
If ESParent=1 and ESlock=0 Then
Dim UrlID
	UrlID=Request.ServerVariables("QUERY_STRING")   '获取来源地址
	UrlID=Replace(UrlID,".html","")
Dim ClassID  '判读数组(页数和栏目ID)
	If UrlID<>"" Then
		UrlID=split(UrlID,"_")
		ClassID=UrlID(1)
	End If
	If ClassID<>"" Then
		ESClass=ClassID
	End If
End If
'判断ID是否空和安全数值
If isnumeric(ESClass)=True Then
'读取栏目
	If ESParent=0 Then
		ESClass="ClassID="&ESClass&" and "
	Else
		ESClass="ClassID In ("& ESClass&AllChildClass(ESClass) &") and"
	End If
'读取特性
IF ESSpecial=0 Then
	ESSpecial=""
Else
	ESSpecial="Instr(','+SpecialID+',',',"&ESSpecial&",')<>0 and"
End If
'排序方法
If OrderField="ProductsID" Then 
	OrderField="ProductsID"
Else
	OrderField=""&OrderField&" "&OrderType&",ProductsID"
End If
'开始读取数据
Dim rs
	Set rs=Server.CreateObject("ADODB.RecordSet")
Dim Sql
    Sql = "select top "&ESNPage&" ProductsID From [All_Products] Where "&ESSpecial&" "&ESClass&" Passed=True Order By "&OrderField&" "&OrderType&"" 
Dim Template
	rs.open sql,conn,1,1
		Template = Template& ""&ESulS&""
Dim N
	N=0
	Do while not rs.eof
	If (n Mod ESNList) = 0 Then Template = Template& ""&ESliS&""
		Template = Template& ""&FormatLable_Prod(ESLable,rs("ProductsId"))&""  '读取模板ID和替换本信息的ID
	N = N + 1
	If (n Mod ESNList) = 0 Then Template = Template& ""&ESliE&""
	rs.movenext
	loop
		Template = Template& ""&ESulE&""
		rs.close
		set rs = nothing
	Easyes_ProdList = Template
End If     '判断ID非法性
End Function
End Class
'//#################################################
'//产品资源--列表有分页
'//#################################################
Class TemplateSkin_ProdN
Public ESLable,ESClass,ESParent,ESLock,ESSpecial,ESSearch,ESNPage,ESNList,ESulS,ESulE,ESliS,ESliE,ESPageS,ESPageE,OrderField,OrderType
'//【功能】获取产品的列表信息
Function Easyes_ProdNList(strColumn)
'//转换标签中HTML“{}”为“<>”和“()”为“""”
'//Lable_Function.asp
ESulS=LableEncode(ESulS)
ESulE=LableEncode(ESulE)
ESliS=LableEncode(ESliS)
ESliE=LableEncode(ESliE)
ESPageS=LableEncode(ESPageS)
ESPageE=LableEncode(ESPageE)
 '判断赋值方法,是否浏览器赋值进入
Dim UrlID
	UrlID=Request.ServerVariables("QUERY_STRING")   '获取来源地址
	UrlID=Replace(UrlID,".html","")
Dim StrID,StrI
	StrID=split(UrlID,"_")	
If UBound(StrID)=1 or UBound(StrID)=2 then '判断分页和分类ID
		CurrentPage=StrID(0)
	If ESlock=0 Then
		ESClass=StrID(1)
	End If	
End If
'判断ID是否空和安全数值
If isnumeric(CurrentPage)=True and isnumeric(ESClass)=True Then
'读取栏目
Dim PageID
	PageID=ESClass  '分页ID
	If ESParent=0 Then '判断是否包含子分类
		ESClass="ClassID="&ESClass&" and "
	Else
		ESClass="ClassID In ("& ESClass&AllChildClass(ESClass) &") and"
	End If	
'读取特性
IF ESSpecial=0 Then
	ESSpecial=""
Else
	ESSpecial="Instr(','+SpecialID+',',',"&ESSpecial&",')<>0 and"
End If
'是否开启搜索
IF ESSearch=1 Then
	ESSearch=""
Dim KeyWord,PageKeyWord  '判断关键字
	If UBound(StrID)=2 Then
		PageKeyWord="_"&StrID(2)&""
		ESSearch="Title Like '%"&StrID(2)&"%' and"
	End If	
	   	KeyWord=Checkstr(jencode(trim(Request.form("KeyWord"))))
	If KeyWord<>"" Then
		PageKeyWord="_"&KeyWord&""
		ESSearch="Title Like '%"&KeyWord&"%' and"
	End If
Else
	ESSearch=""
End If
'排序方法
If OrderField="ProductsID" Then 
	OrderField="ProductsID"
Else
	OrderField=""&OrderField&" "&OrderType&",ProductsID"
End If
'开始读取数据
Dim rs
	Set rs=Server.CreateObject("ADODB.RecordSet")
Dim Sql
    Sql = "select ProductsID From [All_Products] Where "&ESSearch&" "&ESSpecial&" "&ESClass&" Passed=True Order By "&OrderField&" "&OrderType&"" 
Dim Template
'翻页处理开始
    Rs.PageSize=ESNPage  
	Rs.CacheSize=Rs.PageSize
    Rs.Open Sql,Conn,1,1
    If CurrentPage="" Then
        CurrentPage=1
    Else
        CurrentPage=CurrentPage
    End If
    If Not(Rs.Eof And Rs.Bof) Then
        Rs.AbsolutePage=CurrentPage
    End If
'翻页处理结束
		Template = Template& ""&ESulS&""
Dim N,I
	N=0
  For I=1 To Rs.PageSize
    If Rs.Eof Then
        Exit For
    End If
	If (n Mod ESNList) = 0 Then Template = Template& ""&ESliS&""
		Template = Template& ""&FormatLable_Prod(ESLable,rs("ProductsID"))&""  '读取模板ID和替换本信息的ID
	N = N + 1
	If (n Mod ESNList) = 0 Then Template = Template& ""&ESliE&""
  	Rs.MoveNext
  	Next
		Template = Template& ""&ESulE&""&chr(13)
		Template = Template& ""&ESPageS&""&chr(13)
		Template = Template& "<script src="""&SiteFile&"ESYSPassport/PageList.js""></script>"&chr(13)
		Template = Template& "<script type=""text/javascript"" language=""JavaScript"">"&chr(13)
		Template = Template& "	<!--"&chr(13)
		Template = Template& "	var plb = new PageListBar('plb',"&Rs.PageCount&","&CurrentPage&",'_"&PageID&""&PageKeyWord&".html',20);"&chr(13)
		Template = Template& "	document.write(plb);"&chr(13)
		Template = Template& "	//-->"&chr(13)
		Template = Template& "</script>"&chr(13)
		Template = Template& ""&ESPageE&""&chr(13)
	Rs.Close
	Set Rs=Nothing
    Easyes_ProdNList = Template
End If     '判断ID非法性
End Function
End Class
'//#################################################
'//产品资源--内容查看
'//#################################################
Class TemplateSkin_ProdPage
Public ESLable,ESClass
'//【功能】获取常规的列表信息
Function Easyes_ProdPage(strColumn)	
Dim UrlID
	UrlID=Request.ServerVariables("QUERY_STRING")   '获取来源地址
	UrlID=Replace(UrlID,".html","")
If isnumeric(UrlID)=True Then   '判断ID非法性
IF ESClass=0 Then  '子分类判断
	ESClass=""
Else
	ESClass="ClassID="&ESClass&" and "
End If
Dim rs
	Set rs=Server.CreateObject("ADODB.RecordSet")
Dim Sql
    Sql = "select ProductsID From [All_Products] Where "&ESClass&" Passed=True and ProductsID="&UrlID&""
Dim Template
	rs.open sql,conn,1,1
If rs.eof or rs.bof then
	response.Write("·未找到相关数据！")
Else	
	Template = Template& ""&FormatLable_Prod(ESLable,rs("ProductsID"))&""  '读取模板ID和替换本信息的ID
	rs.movenext
End If
	rs.close
	set rs = nothing
	Easyes_ProdPage = Template
End If     '判断ID非法性
End Function
End Class
'//#################################################
'//产品资源替换处理
'//#################################################
Function FormatLable_Prod(LabelID,ID)  '替换处理
Dim Formatrs
	Set Formatrs=Server.CreateObject("ADODB.RecordSet")
Dim FormatSql
	FormatSql = "select ProductsID,ClassID,ClassUrl,ClassName,Title,PageUrl,ProductNum,ProductsCode,Rmb,Author,Content,Images,Text,Count,UpdateTime,Date From [All_Products] Where ProductsID="&ID
	Formatrs.open Formatsql,Conn,1,1
Dim FormatLable
If FormatRs.Eof And FormatRs.Bof Then
    FormatRs.Close
    Set FormatRs=Nothing
    FormatLable_Prod="·超出范围！"
    Exit Function
End If
'调用类
Dim StrClass
Set StrClass = New ESYS_StringClass
'读取自定义标签    'TemplateFunction.asp
Dim Template
	Session("buffer_Template")=""
	Template="" 
	Template=GetLableTemplate(LabelID)
    If Trim(Template)="" Or ISNULL(Template) Then
         Formatrs.Close
         Set Formatrs=Nothing
         FormatLable_News=False
        Exit Function
    End If
Dim Asp_Lable  '开始替换
	Asp_Lable="{\$Prod_ID\$}"   'ID资源
	Template = StrClass.ReplaceTest(Asp_Lable,Template,CStr(""&Formatrs("ProductsID")))
	Asp_Lable="{\$Prod_Title\$}"   '标题
	Template = StrClass.ReplaceTest(Asp_Lable,Template,CStr(""&juncode(Formatrs("Title"))))
	Asp_Lable="{\$Prod_PageUrl\$}"   '阅读地址
	Template = StrClass.ReplaceTest(Asp_Lable,Template,""&CStr(""&Formatrs("PageUrl"))&"?"&Formatrs("ProductsID")&".html")
	Asp_Lable="{\$Prod_ClassUrl\$}"  '栏目连接地址
	Template = StrClass.ReplaceTest(Asp_Lable,Template,""&CStr(""&Formatrs("ClassUrl"))&"?1_"&Formatrs("ClassID")&".html")
	Asp_Lable="{\$Prod_Num\$}"  '编号
	Template = StrClass.ReplaceTest(Asp_Lable,Template,CStr(""&Formatrs("ProductNum")))
	Asp_Lable="{\$Prod_Rmb\$}"  '价格
	Template = StrClass.ReplaceTest(Asp_Lable,Template,CStr(""&Formatrs("Rmb")))
	Asp_Lable="{\$Prod_ProductsCode\$}"  '型号
	Template = StrClass.ReplaceTest(Asp_Lable,Template,CStr(""&Formatrs("ProductsCode")))
	Asp_Lable="{\$Prod_Content\$}"  '内容
	Template = StrClass.ReplaceTest(Asp_Lable,Template,CStr(""&Formatrs("Content")))
	Asp_Lable="{\$Prod_Author\$}"  '作者
	Template = StrClass.ReplaceTest(Asp_Lable,Template,CStr(""&juncode(Formatrs("Author"))))
	Asp_Lable="{\$Prod_Images\$}"  '图片
	Template = StrClass.ReplaceTest(Asp_Lable,Template,CStr(""&Formatrs("Images")))
	Asp_Lable="{\$Prod_ShortContent\$}"  '简述
	Template = StrClass.ReplaceTest(Asp_Lable,Template,CStr(""&Formatrs("Text")))
	Asp_Lable="{\$Prod_AddTime\$}" '添加时间
	Template = StrClass.ReplaceTest(Asp_Lable,Template,""&StrClass.FormatMyDate(Formatrs("Date"),"{m}/{d}"))
	Asp_Lable="{\$Prod_UpTime\$}"  '更新时间
	Template = StrClass.ReplaceTest(Asp_Lable,Template,""&StrClass.FormatMyDate(Formatrs("UpdateTime"),"{m}/{d}"))
	Asp_Lable="{\$Prod_Count1\$}"  '阅读次数(不增加)
	Template = StrClass.ReplaceTest(Asp_Lable,Template,""&Formatrs("Count")&"")
	Asp_Lable="{\$Prod_Count2\$}"  '阅读次数(增加)
	Template = StrClass.ReplaceTest(Asp_Lable,Template,"<script src="""&SiteFile&"ESYSPassport/Count.asp?Display=Prod&ID="&Formatrs("ProductsID")&"""></script>")
	Asp_Lable="{\$Prod_ClassName\$}"  '分类名称
	Template = StrClass.ReplaceTest(Asp_Lable,Template,CStr(""&Formatrs("ClassName")))
	SystemLable Template	'系统标签
	Template = TemplateTags(Template)
	FormatLable_Prod=Template
	Formatrs.close
	Set Formatrs = nothing
End Function

'/★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★
'/下载标签
'/★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★
'////////////////////////////////////////
'//【功能】实际替换标签的类（下载模块）
'//#################################################
'//下载资源--列表没有分页
'//#################################################
Class TemplateSkin_Down
Public ESLable,ESClass,ESParent,ESLock,ESSpecial,ESNPage,ESNList,ESulS,ESulE,ESliS,ESliE,OrderField,OrderType
'//【功能】获取常规的列表信息
Function Easyes_DownList(strColumn)	
'//转换标签中HTML“{}”为“<>”和“()”为“""”
'//Lable_Function.asp
ESulS=LableEncode(ESulS)
ESulE=LableEncode(ESulE)
ESliS=LableEncode(ESliS)
ESliE=LableEncode(ESliE)
 '判断赋值方法,是否浏览器赋值进入
If ESParent=1 and ESlock=0 Then
Dim UrlID
	UrlID=Request.ServerVariables("QUERY_STRING")   '获取来源地址
	UrlID=Replace(UrlID,".html","")
Dim ClassID  '判读数组(页数和栏目ID)
	If UrlID<>"" Then
		UrlID=split(UrlID,"_")
		ClassID=UrlID(1)
	End If
	If ClassID<>"" Then
		ESClass=ClassID
	End If
End If
If isnumeric(ESClass)=True Then '判断ID是否空和安全数值
'读取栏目
	If ESParent=0 Then
		ESClass="ClassID="&ESClass&" and "
	Else
		ESClass="ClassID In ("& ESClass&AllChildClass(ESClass) &") and"
	End If
'读取特性
IF ESSpecial=0 Then
	ESSpecial=""
Else
	ESSpecial="Instr(','+SpecialID+',',',"&ESSpecial&",')<>0 and"
End If
'排序方法
If OrderField="DownID" Then 
	OrderField="DownID"
Else
	OrderField=""&OrderField&" "&OrderType&",DownID"
End If
'开始读取数据
Dim rs
	Set rs=Server.CreateObject("ADODB.RecordSet")
Dim Sql
    Sql = "select top "&ESNPage&" DownID From [All_Down] Where "&ESSpecial&" "&ESClass&" Passed=True Order By "&OrderField&" "&OrderType&"" 
Dim Template
	rs.open sql,conn,1,1
		Template = Template& ""&ESulS&""
Dim N
	N=0
	Do while not rs.eof
	If (n Mod ESNList) = 0 Then Template = Template& ""&ESliS&""
		Template = Template& ""&FormatLable_Down(ESLable,rs("DownId"))&""  '读取模板ID和替换本信息的ID
	N = N + 1
	If (n Mod ESNList) = 0 Then Template = Template& ""&ESliE&""
	rs.movenext
	loop
		Template = Template& ""&ESulE&""
		rs.close
		set rs = nothing
	Easyes_DownList = Template
End If     '判断ID非法性
End Function
End Class
'//#################################################
'//下载资源--列表有分页
'//#################################################
Class TemplateSkin_DownN
Public ESLable,ESClass,ESParent,ESLock,ESSpecial,ESSearch,ESNPage,ESNList,ESulS,ESulE,ESliS,ESliE,ESPageS,ESPageE,OrderField,OrderType
'//【功能】获取下载的列表信息
Function Easyes_DownNList(strColumn)
'//转换标签中HTML“{}”为“<>”和“()”为“""”
'//Lable_Function.asp
ESulS=LableEncode(ESulS)
ESulE=LableEncode(ESulE)
ESliS=LableEncode(ESliS)
ESliE=LableEncode(ESliE)
ESPageS=LableEncode(ESPageS)
ESPageE=LableEncode(ESPageE)
 '判断赋值方法,是否浏览器赋值进入
Dim UrlID
	UrlID=Request.ServerVariables("QUERY_STRING")   '获取来源地址
	UrlID=Replace(UrlID,".html","")
Dim StrID,StrI
	StrID=split(UrlID,"_")	
If UBound(StrID)=1 or UBound(StrID)=2 then '判断分页和分类ID
		CurrentPage=StrID(0)
	If ESlock=0 Then
		ESClass=StrID(1)
	End If	
End If
'判断ID是否空和安全数值
If isnumeric(CurrentPage)=True and isnumeric(ESClass)=True Then
'读取栏目
Dim PageID
	PageID=ESClass  '分页ID
	If ESParent=0 Then '判断是否包含子分类
		ESClass="ClassID="&ESClass&" and "
	Else
		ESClass="ClassID In ("& ESClass&AllChildClass(ESClass) &") and"
	End If	
'读取特性
IF ESSpecial=0 Then
	ESSpecial=""
Else
	ESSpecial="Instr(','+SpecialID+',',',"&ESSpecial&",')<>0 and"
End If
'是否开启搜索
IF ESSearch=1 Then
	ESSearch=""
Dim KeyWord,PageKeyWord  '判断关键字
	If UBound(StrID)=2 Then
		PageKeyWord="_"&StrID(2)&""
		ESSearch="Title Like '%"&StrID(2)&"%' and"
	End If	
	   	KeyWord=Checkstr(jencode(trim(Request.form("KeyWord"))))
	If KeyWord<>"" Then
		PageKeyWord="_"&KeyWord&""
		ESSearch="Title Like '%"&KeyWord&"%' and"
	End If
Else
	ESSearch=""
End If
'排序方法
If OrderField="DownID" Then 
	OrderField="DownID"
Else
	OrderField=""&OrderField&" "&OrderType&",DownID"
End If
'开始读取数据
Dim rs
	Set rs=Server.CreateObject("ADODB.RecordSet")
Dim Sql
    Sql = "select DownID From [All_Down] Where "&ESSearch&" "&ESSpecial&" "&ESClass&" Passed=True Order By "&OrderField&" "&OrderType&"" 
Dim Template
'翻页处理开始
    Rs.PageSize=ESNPage  
	Rs.CacheSize=Rs.PageSize
    Rs.Open Sql,Conn,1,1
    If CurrentPage="" Then
        CurrentPage=1
    Else
        CurrentPage=CurrentPage
    End If
    If Not(Rs.Eof And Rs.Bof) Then
        Rs.AbsolutePage=CurrentPage
    End If
'翻页处理结束
		Template = Template& ""&ESulS&""
Dim N,I
	N=0
  For I=1 To Rs.PageSize
    If Rs.Eof Then
        Exit For
    End If
	If (n Mod ESNList) = 0 Then Template = Template& ""&ESliS&""
		Template = Template& ""&FormatLable_Down(ESLable,rs("DownID"))&""  '读取模板ID和替换本信息的ID
	N = N + 1
	If (n Mod ESNList) = 0 Then Template = Template& ""&ESliE&""
  	Rs.MoveNext
  	Next
		Template = Template& ""&ESulE&""&chr(13)
		Template = Template& ""&ESPageS&""&chr(13)
		Template = Template& "<script src="""&SiteFile&"ESYSPassport/PageList.js""></script>"&chr(13)
		Template = Template& "<script type=""text/javascript"" language=""JavaScript"">"&chr(13)
		Template = Template& "	<!--"&chr(13)
		Template = Template& "	var plb = new PageListBar('plb',"&Rs.PageCount&","&CurrentPage&",'_"&PageID&""&PageKeyWord&".html',20);"&chr(13)
		Template = Template& "	document.write(plb);"&chr(13)
		Template = Template& "	//-->"&chr(13)
		Template = Template& "</script>"&chr(13)
		Template = Template& ""&ESPageE&""&chr(13)
	Rs.Close
	Set Rs=Nothing
    Easyes_DownNList = Template
End If     '判断ID非法性
End Function
End Class
'//#################################################
'//下载资源--内容查看
'//#################################################
Class TemplateSkin_DownPage
Public ESLable,ESClass
'//【功能】获取常规的列表信息
Function Easyes_DownPage(strColumn)	
Dim UrlID
	UrlID=Request.ServerVariables("QUERY_STRING")   '获取来源地址
	UrlID=Replace(UrlID,".html","")
If isnumeric(UrlID)=True Then   '判断ID非法性
IF ESClass=0 Then  '子分类判断
	ESClass=""
Else
	ESClass="ClassID="&ESClass&" and "
End If
Dim rs
	Set rs=Server.CreateObject("ADODB.RecordSet")
Dim Sql
    Sql = "select DownID From [All_Down] Where "&ESClass&" Passed=True and DownID="&UrlID&""
Dim Template
	rs.open sql,conn,1,1
If rs.eof or rs.bof then
	response.Write("·未找到相关数据！")
Else	
	Template = Template& ""&FormatLable_Down(ESLable,rs("DownID"))&""  '读取模板ID和替换本信息的ID
	rs.movenext
End If
	rs.close
	set rs = nothing
	Easyes_DownPage = Template
End If     '判断ID非法性
End Function
End Class
'//#################################################
'//下载资源替换处理
'//#################################################
Function FormatLable_Down(LabelID,ID)  '替换处理
Dim Formatrs
	Set Formatrs=Server.CreateObject("ADODB.RecordSet")
Dim FormatSql
	FormatSql = "select DownID,ClassID,ClassUrl,ClassName,Title,PageUrl,FileMB,DownUrl,Author,Content,Images,Text,Count,UpdateTime,Date From [All_Down] Where DownID="&ID
	Formatrs.open Formatsql,Conn,1,1
Dim FormatLable
If FormatRs.Eof And FormatRs.Bof Then
    FormatRs.Close
    Set FormatRs=Nothing
    FormatLable_Down="·超出范围！"
    Exit Function
End If
'调用类
Dim StrClass
Set StrClass = New ESYS_StringClass
'读取自定义标签    'TemplateFunction.asp
Dim Template
	Session("buffer_Template")=""
	Template="" 
	Template=GetLableTemplate(LabelID)
    If Trim(Template)="" Or ISNULL(Template) Then
         Formatrs.Close
         Set Formatrs=Nothing
         FormatLable_News=False
        Exit Function
    End If
Dim Asp_Lable  '开始替换
	Asp_Lable="{\$Down_ID\$}"   'ID资源
	Template = StrClass.ReplaceTest(Asp_Lable,Template,CStr(""&Formatrs("DownID")))
	Asp_Lable="{\$Down_Title\$}"   '标题
	Template = StrClass.ReplaceTest(Asp_Lable,Template,CStr(""&juncode(Formatrs("Title"))))
	Asp_Lable="{\$Down_PageUrl\$}"   '阅读地址
	Template = StrClass.ReplaceTest(Asp_Lable,Template,""&CStr(""&Formatrs("PageUrl"))&"?"&Formatrs("DownID")&".html")
	Asp_Lable="{\$Down_ClassUrl\$}"  '栏目连接地址
	Template = StrClass.ReplaceTest(Asp_Lable,Template,""&CStr(""&Formatrs("ClassUrl"))&"?1_"&Formatrs("ClassID")&".html")
	Asp_Lable="{\$Down_MB\$}"  '大小
	Template = StrClass.ReplaceTest(Asp_Lable,Template,CStr(""&Formatrs("FileMB")))
	Asp_Lable="{\$Down_DownUrl\$}"  '下载地址
	Template = StrClass.ReplaceTest(Asp_Lable,Template,""&ShowDownloadUrls(Formatrs("DownUrl")))
	Asp_Lable="{\$Down_Content\$}"  '内容
	Template = StrClass.ReplaceTest(Asp_Lable,Template,CStr(""&Formatrs("Content")))
	Asp_Lable="{\$Down_Author\$}"  '作者
	Template = StrClass.ReplaceTest(Asp_Lable,Template,CStr(""&juncode(Formatrs("Author"))))
	Asp_Lable="{\$Down_Images\$}"  '图片
	Template = StrClass.ReplaceTest(Asp_Lable,Template,CStr(""&Formatrs("Images")))
	Asp_Lable="{\$Down_ShortContent\$}"  '简述
	Template = StrClass.ReplaceTest(Asp_Lable,Template,CStr(""&Formatrs("Text")))
	Asp_Lable="{\$Down_AddTime\$}" '添加时间
	Template = StrClass.ReplaceTest(Asp_Lable,Template,""&StrClass.FormatMyDate(Formatrs("Date"),"{m}/{d}"))
	Asp_Lable="{\$Down_UpTime\$}"  '更新时间
	Template = StrClass.ReplaceTest(Asp_Lable,Template,""&StrClass.FormatMyDate(Formatrs("UpdateTime"),"{m}/{d}"))
	Asp_Lable="{\$Down_Count1\$}"  '阅读次数(不增加)
	Template = StrClass.ReplaceTest(Asp_Lable,Template,""&Formatrs("Count")&"")
	Asp_Lable="{\$Down_Count2\$}"  '阅读次数(增加)
	Template = StrClass.ReplaceTest(Asp_Lable,Template,"<script src="""&SiteFile&"ESYSPassport/Count.asp?Display=Down&ID="&Formatrs("DownID")&"""></script>")
	Asp_Lable="{\$Down_ClassName\$}"  '分类名称
	Template = StrClass.ReplaceTest(Asp_Lable,Template,CStr(""&Formatrs("ClassName")))
	SystemLable Template	'系统标签
	Template = TemplateTags(Template)
	FormatLable_Down=Template
	Formatrs.close
	Set Formatrs = nothing
End Function
'/★★★★★★★★★★★★★★★★★★★★★
'/单页标签
'/★★★★★★★★★★★★★★★★★★★★★
'////////////////////////////////////////
'//【功能】实际替换标签的类（单页模块）
Class TemplateSkin_Content
Public ESLable,ESClass,ESLock

'//【功能】获取常规的列表信息
Function Easyes_ESApage(strColumn)
If ESLock=0 Then
Dim UrlID
	UrlID=Request.ServerVariables("QUERY_STRING")   '获取来源地址
	UrlID=Replace(UrlID,".html","")
Dim ClassID  '判读数组
	If UrlID<>"" Then
		UrlID=split(UrlID,"_")
		ClassID=UrlID(1)
	End If
	If ClassID<>"" Then
		ESClass=ClassID
	End If
End If
If isnumeric(ESClass)=True Then   '判断ID是否空
'替换标签内容
Dim Formatrs
	Set Formatrs=Server.CreateObject("ADODB.RecordSet")
Dim FormatSql
	FormatSql = "select ID,Title,Content,Rxplain,ClassName From [All_Content] Where Passed=True and ClassID="&ESClass
	Formatrs.open FormatSQL,Conn,1,1
Dim FormatLable
If FormatRs.Eof And FormatRs.Bof Then
    FormatRs.Close
    Set FormatRs=Nothing
    Easyes_ESApage="·信息未找到！"
    Exit Function
End If
'调用类
Dim StrClass
Set StrClass = New ESYS_StringClass
'读取模板
Dim Template
	Session("buffer_Template")=""
	Template=""
		Template=GetLableTemplate(ESLable)  '获取自定义标签

    If Trim(Template)="" Or ISNULL(Template) Then
         Formatrs.Close
         Set Formatrs=Nothing
         Easyes_ESApage=False
        Exit Function
    End If
'开始替换
Dim Asp_Lable
	Asp_Lable="{\$Apage_Title\$}"  '标题
	Template = StrClass.ReplaceTest(Asp_Lable,Template,CStr(""&Formatrs("Title")))
	Asp_Lable="{\$Apage_Class\$}"  '分类名称
	Template = StrClass.ReplaceTest(Asp_Lable,Template,CStr(""&Formatrs("ClassName")))
	Asp_Lable="{\$Apage_Content\$}"  '内容
	Template = StrClass.ReplaceTest(Asp_Lable,Template,CStr(""&Formatrs("Content")))
	Asp_Lable="{\$Apage_Rxplain\$}"  '简述
	Template = StrClass.ReplaceTest(Asp_Lable,Template,CStr(""&Formatrs("Rxplain")))
	SystemLable Template	'系统标签
	Template = TemplateTags(Template)
	Easyes_ESApage=Template
	Formatrs.close
	Set Formatrs = nothing
End If     '判断ID非法性
End Function
End Class
%>