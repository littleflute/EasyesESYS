﻿<%@language=vbscript codepage=65001%>
<%Option Explicit%>
<%
Response.CodePage=65001
Response.Charset="UTF-8"
Session.CodePage = 65001
'**************************************************************
' Software Name: EasyesESYS
' Web: http://www.Easyes.com.cn
'**************************************************************
'强制浏览器重新访问服务器下载页面，而不是从缓存读取页面
Response.Buffer = True
Response.Expires = -1
Response.ExpiresAbsolute = Now() - 1
Response.Expires = 0
Response.CacheControl = "no-cache"
response.buffer=true
Const PurviewLevel=1
'判断管理是否登陆
'**************************************************************
if Session("admin")="" then
    Response.Write "<script>top.location='Admin_Logout.asp'</script>"
else
	if session("purview")>PurviewLevel then
		response.write "<br><p align=center><font color='red' size='2'>您没有操作的权限</font></p>"
		response.end
	end if
end if
%>
<!--#include file="Config/Config.asp"-->
<!--#include file="Config/Conn.asp"-->
<!--#include file="Config/function.asp"-->
<!--#include file="Config/ClassList_Fun.asp"--><html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>标签样式库设定</title>
<meta name="Version" content="<%=SystemVer%>">
<script type="text/javascript" src="Config/ClassTree.js"></script>
<script type="text/javascript" src="Config/Tooltip.js"></script>
<link href="Images/Style.css" rel="stylesheet" type="text/css">
<script type="text/javascript"> 
function oCopy(obj){ 
obj.select(); 
js=obj.createTextRange(); 
js.execCommand("Copy") 
} 
</script> 
<style type="text/css">
<!--
BODY {
scrollbar-highlight-color: buttonface;
scrollbar-shadow-color : buttonface;
scrollbar-3dlight-color: buttonhighlight;
scrollbar-track-color: #eeeeee;
scrollbar-darkshadow-color: buttonshadow;
background-color:buttonface;
margin: 3px; padding: 0px;border: none;
}
-->
</style>
</head>
<body scroll="no">
<%
Dim Action
	Action=trim(request("Action"))
Dim LabelID
	LabelID=request("LabelID")
Dim Special
	Special=Request("Special")
if Action="Class" then   '栏目列表
	call ClassLable()
Elseif Action="Apage" then   '单页特性
	call Apage()
Elseif Action="News1" then   '常规特性
	call News1()
Elseif Action="News2" then   '常规特性
	call News2()
Elseif Action="News3" then   '常规特性
	call News3()
Elseif Action="ClassWindows" then  '常规特性
	call ClassWindows()	
Elseif Action="ApageWindows" then  '常规特性
	call ApageWindows()
Elseif Action="Windows1" then  '网站栏目
	call Windows1()
Elseif Action="Windows2" then  '资源列表
	call Windows2()
Elseif Action="Windows3" then  '翻页列表
	call Windows3()	
Elseif Action="Lable" then
	call Lable()
Elseif Action="DiyLable" then
	call DiyLable()	
end if
call CloseConn()
Sub ClassLable()
%>
<form action="Admin_LabelStyle.asp?action=ClassWindows" method="post" name="myform_class" target="ClassWindows">
<table border=0 cellspacing=1 align="center" style="width:100%;height:100%">
<tr><td style="height:100%">
<div style="width:100%;height:100%;overflow:auto;padding:3px">
<table cellpadding="0" cellspacing="1" style="width: 100%;">
<tr>
<td align="right" class="tdbg" style="width: 100px;"><div style="width:80px">标签ID：</div></td>
<td class="tdbg"><%call Label(LabelID)%></td>
</tr>
<tr>
<td align="right" class="tdbg" valign="top">所属分类：</td>
<td class="tdbg">
<script type="text/javascript" language="javascript">
var root1
root1=CreateRoot("myTree1","·请选择[资源类别]")
<%
Dim CurrentClassID
Call CreateClassTree1(0,CurrentClassID,Special)%>
</script>
</td>
</tr>
<tr>
<td align="right" class="tdbg">包含子分类：</td>
<td class="tdbg">
<select name="ESParent" class="input_text">
<option value="1" selected>是</option>
<option value="0">否</option>
</select>(是:显示所选分类下级分类)</td>
</tr>
<tr><td align="right" class="tdbg">锁定分类：</td><td class="tdbg"><select name="ESlock" class="input_text">
<option value="1" selected>是</option>
<option value="0">否</option>
</select>(是:不随页面ID变换显示下级分类)</td></tr>
<tr><td align="right" class="tdbg">HTML元素盒：</td><td class="tdbg">始:<input type="text" name="ESulS" value="" class="input_text" style="width: 140px">尾:<input type="text" name="ESulE" value="" class="input_text" style="width: 60px"></td></tr>
<tr><td align="right" class="tdbg">HTML元素：</td><td class="tdbg">始:<input type="text" name="ESliS" value="" class="input_text" style="width:140px">尾:<input type="text" name="ESliE" value="" class="input_text" style="width: 60px"></td></tr>
<tr><td align="right" class="tdbg">循环HTML元素：</td><td class="tdbg"><input type="text" name="ESNList" size="3" class="input_text" value="1">次(多列,需和"HTML元素"配合)</td></tr>
<tr>
<td align="right" class="tdbg">排序方法：</td>
<td class="tdbg"><select name="OrderField" id="OrderField" class="input_text">
<option value="ClassID" selected>栏目ID</option>
<option value="UpTime">更新时间</option>
</select><select name="OrderType" id="OrderType" class="input_text">
<option value="asc">升序</option>
<option value="desc" selected>降序</option>
</select></td>
</tr>
</table></div></td></tr>
<tr align=center>
<td colspan="2">
<iframe name="ClassWindows" scrolling="no" frameborder="0" style="width: 380px; height: 55px" src="Admin_LabelStyle.asp?action=ClassWindows"></iframe></td>
</tr>
<tr align=center>
<td colspan="2">
<input name="SaveAddButton" type="submit" id="SaveAddButton" value="生成标签控制" class=button> 
<input type="button" value="关闭窗口" class=button onclick="window.opener=null;window.close()"> 
</td>
</tr>
</table>
</form>
<%
End Sub
Sub Apage()
%>
<form action="Admin_LabelStyle.asp?action=ApageWindows" method="post" name="myform_page" target="ApageWindows">
<table border=0 cellspacing=1 align="center" style="width:100%;height:100%;">
<tr><td style="height:100%;">
<div style="width:100%;height:100%;overflow:auto;padding:3px;">
<table cellpadding="0" cellspacing="1" style="width:100%">
<tr>
<td align="right" class="tdbg" style="width: 100px;"><div style="width:80px">标签ID：</div></td>
<td class="tdbg">
<%call Label(LabelID)%></td>
</tr>
<tr>
<td align="right" class="tdbg" valign="top">调用单页：</td>
<td class="tdbg">
<script type="text/javascript" language="javascript">
var root1
root1=CreateRoot("myTree1","·请选择[资源类别]")
<%
Dim CurrentClassID
Call CreateClassTree1(0,CurrentClassID,Special)%>
</script>
</td>
</tr>
<tr><td align="right" class="tdbg">锁定分类：</td><td class="tdbg"><select name="ESlock" class="input_text">
<option value="1" selected>是</option>
<option value="0">否</option>
</select>(是:不随页面ID变换显对应分类资源)</td></tr>
</table></div>
</td></tr>
<tr align=center>
<td colspan="2">
<iframe name="ApageWindows" scrolling="no" frameborder="0" style="width: 380px; height: 55px" src="Admin_LabelStyle.asp?action=ApageWindows"></iframe></td>
</tr>
<tr align=center>
<td colspan="2">
<input name="SaveAddButton" type="submit" id="SaveAddButton" value="生成标签控制" class=button> 
<input type="button" value="关闭窗口" class=button onclick="window.opener=null;window.close()"> 
</td>
</tr>
</table>
</form>
<%
End Sub
Sub News1()
%>
<form action="Admin_LabelStyle.asp?action=Windows1" method="post" name="myform_list" target="Windows1">
<input name="Special" type="hidden" id="Special" value="<%=Special%>">
<table border=0 cellspacing=1 align="center" style="width:100%;height:100%;">
<tr><td style="height:100%;">
<div style="width:100%;height:100%;overflow:auto;padding:3px;">
<table cellpadding="0" cellspacing="1" style="width: 100%">
<tr><td align="right" class="tdbg" valign="top" style="width: 100"><div style="width:80px">标签ID：</div></td><td class="tdbg"><%call Label(LabelID)%></td></tr>
<tr><td align="right" class="tdbg" valign="top">所属分类：</td>
<td class="tdbg">
<script type="text/javascript" language="javascript">
var root1
root1=CreateRoot("myTree1","·请选择[资源类别]")
<%
Dim CurrentClassID
Call CreateClassTree1(0,CurrentClassID,Special)
%>
</script>
</td>
</tr>
<tr>
<td align="right" class="tdbg">包含子分类：</td>
<td class="tdbg">
<select name="ESParent" class="input_text">
<option value="1" selected>是</option>
<option value="0">否</option>
</select>(是:显示所选分类下级分类资源)</td>
</tr>
<tr><td align="right" class="tdbg">锁定分类：</td><td class="tdbg"><select name="ESlock" class="input_text">
<option value="1" selected>是</option>
<option value="0">否</option>
</select>(是:不随页面ID变换显下级分类资源)</td></tr>
<tr>
<td align="right" class="tdbg" valign="top">所属特性：</td>
<td class="tdbg">
<script type="text/javascript" language="javascript">
var root3
root3=CreateRoot("myTree3","·请选择[资源特性]")
<%Call SpecialityList_Add(0,Special)%>
</script>
</td>
</tr>
<tr><td align="right" class="tdbg">显示多少资源：</td><td class="tdbg"><input type="text" name="ESNPage" size="3" class="input_text" value="10">条</td></tr>
<tr><td align="right" class="tdbg">HTML元素盒：</td><td class="tdbg">始:<input type="text" name="ESulS" value="" class="input_text" style="width: 140px">尾:<input type="text" name="ESulE" value="" class="input_text" style="width: 60px"></td></tr>
<tr><td align="right" class="tdbg">HTML元素：</td><td class="tdbg">始:<input type="text" name="ESliS" value="" class="input_text" style="width:140px">尾:<input type="text" name="ESliE" value="" class="input_text" style="width: 60px"></td></tr>
<tr><td align="right" class="tdbg">循环HTML元素：</td><td class="tdbg"><input type="text" name="ESNList" size="3" class="input_text" value="1">次(多列,需和"HTML元素"配合)</td></tr>
<tr>
<td align="right" class="tdbg">排序方法：</td>
<td class="tdbg"><select name="OrderField" id="OrderField" class="input_text">
<%
If Special=2 Then
	Response.write "<option value=""ArticleID"" selected>文章ID</option>"
ElseIf Special=3 Then
	Response.write "<option value=""ProductsID"" selected>产品ID</option>"
ElseIf Special=4 Then
	Response.write "<option value=""DownID"" selected>下载ID</option>"	
End If
%><option value="UpdateTime">更新时间</option>
<option value="Count">点击次数</option>
</select>
<select name="OrderType" id="OrderType" class="input_text">
<option value="asc">升序</option>
<option value="desc" selected>降序</option>
</select></td>
</tr>
</table></div>
</td></tr>
<tr align=center>
<td colspan="2">
<iframe name="Windows1" scrolling="no" frameborder="0" style="width: 380px; height: 55px" src="Admin_LabelStyle.asp?action=Windows1"></iframe></td>
</tr>
<tr align=center>
<td colspan="2">
<input name="SaveAddButton" type="submit" id="SaveAddButton" value="生成标签控制" class=button> 
<input type="button" value="关闭窗口" class=button onclick="window.opener=null;window.close()"> 
</td>
</tr>
</table>
</form>
<%
End Sub
Sub News2()
%>
<form action="Admin_LabelStyle.asp?action=Windows2" method="post" name="myform_nlist" target="Windows2">
<input name="Special" type="hidden" id="Special" value="<%=Special%>">
<table border=0 cellspacing=1 align="center" style="width: 100%;height:100%">
<tr><td style="height:100%">
<div style="width:100%;height:100%;overflow:auto;padding:3px;">
<table cellpadding="0" cellspacing="1" style="width: 100%">
<tr>
<td align="right" class="tdbg" valign="top" style="width: 100"><div style="width:80px">标签ID：</div></td>
<td class="tdbg">
<%call Label(LabelID)%></td>
</tr>
<tr>
<td align="right" class="tdbg" valign="top">所属分类：</td>
<td class="tdbg">
<script language="javascript">
var root1
root1=CreateRoot("myTree1","·请选择[资源类别]")
<%
Dim CurrentClassID
Call CreateClassTree1(0,CurrentClassID,Special)%>
</script>
</td>
</tr>
<tr>
<td align="right" class="tdbg">包含子分类：</td>
<td class="tdbg">
<select name="ESParent" class="input_text">
<option value="1" selected>是</option>
<option value="0">否</option>
</select>(是:显示所选分类下级分类资源)</td>
</tr>
<tr><td align="right" class="tdbg">锁定分类：</td><td class="tdbg"><select name="ESlock" class="input_text">
<option value="1" selected>是</option>
<option value="0">否</option>
</select>(是:不随页面ID变换显下级分类资源)</td></tr>
<tr>
<td align="right" class="tdbg" valign="top">所属特性：</td>
<td class="tdbg">
<script>
var root3
root3=CreateRoot("myTree3","·请选择[资源特性]")
<%Call SpecialityList_Add(0,Special)%>
</script>
</td>
</tr>
<tr>
<td align="right" class="tdbg">开启搜索：</td>
<td class="tdbg">
<select name="Search" class="input_text">
<option selected value="0">禁止</option>
<option value="1">开启</option>
</select></td>
</tr>
<tr><td align="right" class="tdbg">显示多少资源：</td><td class="tdbg"><input type="text" name="ESNPage" size="3" class="input_text" value="10">条</td></tr>
<tr><td align="right" class="tdbg">HTML元素盒：</td><td class="tdbg">始:<input type="text" name="ESulS" value="" class="input_text" style="width: 140px">尾:<input type="text" name="ESulE" value="" class="input_text" style="width: 60px"></td></tr>
<tr><td align="right" class="tdbg">HTML元素：</td><td class="tdbg">始:<input type="text" name="ESliS" value="" class="input_text" style="width:140px">尾:<input type="text" name="ESliE" value="" class="input_text" style="width: 60px"></td></tr>
<tr><td align="right" class="tdbg">循环HTML元素：</td><td class="tdbg"><input type="text" name="ESNList" size="3" class="input_text" value="1">次(多列,需和"HTML元素"配合)</td></tr>
<tr><td align="right" class="tdbg">翻页HTML元素盒：</td><td class="tdbg">始:<input type="text" name="ESPageS" value="" class="input_text" style="width:140px">尾:<input type="text" name="ESPageE" value="" class="input_text" style="width: 60px"></td></tr>
<tr><td align="right" class="tdbg">排序方法：</td>
<td class="tdbg"><select name="OrderField" id="OrderField" class="input_text">
<%
If Special=2 Then
	Response.write "<option value=""ArticleID"" selected>文章ID</option>"
ElseIf Special=3 Then
	Response.write "<option value=""ProductsID"" selected>产品ID</option>"
ElseIf Special=4 Then
	Response.write "<option value=""DownID"" selected>下载ID</option>"	
End If
%><option value="UpdateTime">更新时间</option>
<option value="Count">点击次数</option>
</select>
<select name="OrderType" id="OrderType" class="input_text">
<option value="asc">升序</option>
<option value="desc" selected>降序</option>
</select></td>
</tr>
</table></div>
</td></tr>
<tr align=center>
<td colspan="2">
<iframe name="Windows2" scrolling="no" frameborder="0" style="width: 380px; height: 55px" src="Admin_LabelStyle.asp?action=Windows2"></iframe></td>
</tr>
<tr align=center>
<td colspan="2">
<input name="SaveAddButton" type="submit" id="SaveAddButton" value="生成标签控制" class=button> 
<input type="button" value="关闭窗口" class=button onclick="window.opener=null;window.close()"> 
</td>
</tr>
</table>
</form>
<%
End Sub
Sub News3()
%>
<form action="Admin_LabelStyle.asp?action=Windows3" method="post" name="myform_content" target="Windows3">
<input name="Special" type="hidden" id="Special" value="<%=Special%>">
<table border=0 cellspacing=1 align="center" style="width: 100%;height:100%">
<tr><td style="height:100%">
<div style="width:100%;height:100%;overflow:auto;padding:3px;">
<table cellpadding="0" cellspacing="1" style="width: 100%">
<tr>
<td align="right" class="tdbg" valign="top" style="width: 100"><div style="width:80px">标签ID：</div></td>
<td class="tdbg">
<%call Label(LabelID)%></td>
</tr>
<tr>
<td align="right" class="tdbg" valign="top">所属分类：</td>
<td class="tdbg">
<script language="javascript">
var root1
root1=CreateRoot("myTree1","·请选择[资源类别]")
<%
Dim CurrentClassID
Call CreateClassTree1(0,CurrentClassID,Special)%>
</script>
</td>
</tr>
</table></div>
</td></tr>
<tr align=center>
<td colspan="2">
<iframe name="Windows3" scrolling="no" frameborder="0" style="width: 380px; height: 55px" src="Admin_LabelStyle.asp?action=Windows3"></iframe></td>
</tr>
<tr align=center>
<td colspan="2">
<input name="SaveAddButton" type="submit" id="SaveAddButton" value="生成标签控制" class=button> 
<input type="button" value="关闭窗口" class=button onclick="window.opener=null;window.close()"> 
</td>
</tr>
</table>
</form>
<%
End Sub
Sub ClassWindows()
Dim ESLable,ESClass,ESParent,ESlock,ESNList,ESulS,ESulE,ESliS,ESliE,OrderField,OrderType
	ESLable=Request.Form("ESLable")
	ESClass=Request.Form("radioBoxItem")
If ESClass="" Then
	ESClass=0
End If
	ESParent=Request.Form("ESParent")
	ESNList=Request.Form("ESNList")
	ESlock=Request.Form("ESlock")
	ESulS=HTMLEncode1(Request.Form("ESulS"))
	ESulE=HTMLEncode1(Request.Form("ESulE"))
	ESliS=HTMLEncode1(Request.Form("ESliS"))
	ESliE=HTMLEncode1(Request.Form("ESliE"))
	OrderField=Request.Form("OrderField")
	OrderType=Request.Form("OrderType")
Dim SkinCode
If ESLable="" Then
	SkinCode=""
Else
	SkinCode="<Lable:Class_Lable ESLable="""&ESLable&""" ESClass="""&ESClass&""" ESParent="""&ESParent&""" ESlock="""&ESlock&""" ESNList="""&ESNList&""" ESulS="""&ESulS&""" ESulE="""&ESulE&""" ESliS="""&ESliS&""" ESliE="""&ESliE&""" OrderField="""&OrderField&""" OrderType="""&OrderType&""" />"
End If
%>
<textarea name="SkinCode" readonly class="input_text" onclick="oCopy(this)" style="width: 375px; height: 50px"><%=SkinCode%></textarea>
<%
End Sub
Sub ApageWindows()
Dim ESLable,ESClass,ESlock
	ESLable=Request.Form("ESLable")
	ESClass=Request.Form("radioBoxItem")
	ESlock=Request.Form("ESlock")
If ESClass="" Then
	ESClass=0
End If
Dim SkinCode
If ESLable="" Then
	SkinCode=""
Else
	SkinCode="<Lable:Apage_Lable ESLable="""&ESLable&""" ESClass="""&ESClass&""" ESlock="""&ESlock&""" />"
End If
%> 
<textarea rows="4" name="SkinCode" style="width: 368px; height: 50px" readonly class="input_text" onclick="oCopy(this)"><%=SkinCode%></textarea><%
End Sub
Sub Windows1()
	Special=Request.Form("Special")
Dim LableName
If Special=2 Then
	LableName="News_Lable"
ElseIf Special=3 Then
	LableName="Prod_Lable"
ElseIf Special=4 Then
	LableName="Down_Lable"
End If
Dim ESLable,ESClass,ESParent,ESlock,ESSpecial,ESNPage,ESNList,ESulS,ESulE,ESliS,ESliE,OrderField,OrderType
	ESLable=Request.Form("ESLable")
	ESClass=Request.Form("radioBoxItem")
If ESClass="" Then
	ESClass=0
End If
	ESParent=Request.Form("ESParent")
	ESSpecial=Replace(Request("Speciality")," ","")
If ESSpecial="" Then
	ESSpecial=0
End If
	ESlock=Request.Form("ESlock")
	ESNPage=Request.Form("ESNPage")
	ESNList=Request.Form("ESNList")
	ESulS=HTMLEncode1(Request.Form("ESulS"))
	ESulE=HTMLEncode1(Request.Form("ESulE"))
	ESliS=HTMLEncode1(Request.Form("ESliS"))
	ESliE=HTMLEncode1(Request.Form("ESliE"))
	OrderField=Request.Form("OrderField")
	OrderType=Request.Form("OrderType")
Dim SkinCode
If ESLable="" Then
	SkinCode=""
Else
	SkinCode="<Lable:"&LableName&" ESLable="""&ESLable&""" ESClass="""&ESClass&""" ESlock="""&ESlock&""" ESParent="""&ESParent&""" ESSpecial="""&ESSpecial&""" ESNPage="""&ESNPage&""" ESNList="""&ESNList&""" ESulS="""&ESulS&""" ESulE="""&ESulE&""" ESliS="""&ESliS&""" ESliE="""&ESliE&""" OrderField="""&OrderField&""" OrderType="""&OrderType&""" />"
End If
%>
<textarea rows="4" name="SkinCode" style="width: 368px; height: 50px" readonly class="input_text" onclick="oCopy(this)"><%=SkinCode%></textarea>
<%
End Sub
Sub Windows2()
	Special=Request.Form("Special")
Dim LableName
If Special=2 Then
	LableName="News_NList"
ElseIf Special=3 Then
	LableName="Prod_NList"
ElseIf Special=4 Then
	LableName="Down_NList"
End If
Dim ESLable,ESClass,ESParent,ESlock,ESSpecial,ESSearch,ESNPage,ESNList,ESulS,ESulE,ESliS,ESliE,ESPageS,ESPageE,OrderField,OrderType
	ESLable=Request.Form("ESLable")
	ESClass=Request.Form("radioBoxItem")
If ESClass="" Then
	ESClass=0
End If
	ESParent=Request.Form("ESParent")
	ESlock=Request.Form("ESlock")
	ESSpecial=Replace(Request("Speciality")," ","")
If ESSpecial="" Then
	ESSpecial=0
End If
	ESSearch=Request.Form("Search")
	ESNPage=Request.Form("ESNPage")
	ESNList=Request.Form("ESNList")
	ESulS=HTMLEncode1(Request.Form("ESulS"))
	ESulE=HTMLEncode1(Request.Form("ESulE"))
	ESliS=HTMLEncode1(Request.Form("ESliS"))
	ESliE=HTMLEncode1(Request.Form("ESliE"))
	ESPageS=HTMLEncode1(Request.Form("ESPageS"))
	ESPageE=HTMLEncode1(Request.Form("ESPageE"))
	OrderField=Request.Form("OrderField")
	OrderType=Request.Form("OrderType")
Dim SkinCode
If ESLable="" Then
	SkinCode=""
Else
	SkinCode="<Lable:"&LableName&" ESLable="""&ESLable&""" ESClass="""&ESClass&""" ESlock="""&ESlock&""" ESParent="""&ESParent&""" ESSpecial="""&ESSpecial&"""  ESSearch="""&ESSearch&""" ESNPage="""&ESNPage&""" ESNList="""&ESNList&""" ESulS="""&ESulS&""" ESulE="""&ESulE&""" ESliS="""&ESliS&""" ESliE="""&ESliE&""" ESPageS="""&ESPageS&""" ESPageE="""&ESPageE&""" OrderField="""&OrderField&""" OrderType="""&OrderType&""" />"
End If
%>
<textarea rows="4" name="SkinCode" style="width: 368px; height: 50px" readonly class="input_text" onclick="oCopy(this)"><%=SkinCode%></textarea>
<%
End Sub
Sub Windows3()
	Special=Request.Form("Special")
Dim LableName
If Special=2 Then
	LableName="News_Page"
ElseIf Special=3 Then
	LableName="Prod_Page"
ElseIf Special=4 Then
	LableName="Down_Page"
End If
Dim ESLable,ESSpecial,ESClass
	ESLable=Request.Form("ESLable")
	ESClass=Request.Form("radioBoxItem")
If ESClass="" Then
	ESClass=0
End If
Dim SkinCode
If ESLable="" Then
	SkinCode=""
Else
	SkinCode="<Lable:"&LableName&" ESLable="""&ESLable&""" ESClass="""&ESClass&""" />"
End If
%>
<textarea rows="4" name="SkinCode" style="width: 368px; height: 50px" readonly class="input_text" onclick="oCopy(this)"><%=SkinCode%></textarea>
<%
End Sub
Sub Lable()
Dim Special
Special=Request("Special")
%>
<table width="100%" height="100%" border="0" cellpadding="2" cellspacing="0">
<tr> 
<td valign="top"><textarea name="label" style="width:100%;height:100%" class="input_text">
系统标签全站通用 /// 通用标签
#################################
{$SiteName$}  ---------------------网站名称
{$SiteUrl$}  ----------------------网站地址
{$Copyright$}  --------------------版权信息
栏目列表 /// 支持“栏目标签”对其它无效
#################################
{$Class_Title$}    ----------------分类名称
{$Class_Url$}  --------------------连接地址
{$Class_Content$}  ----------------栏目简述<%
If Special=1 Then
%>
单页模块 /// 单页标签
#################################
{$Apage_Title$}    ----------------单页标题
{$Apage_Content$}  ----------------资源内容
{$Apage_Rxplain$}  ----------------单页简述
{$Apage_Class$}  ------------------分类名称<%
End If
If Special=2 Then
%>
常规模块 /// 常规标签
#################################
{$News_ID$}  ----------------------ID序号
{$News_TitleHtml:10$}  ------------标题(10:字数)
{$News_Title$}  -------------------标题
{$News_PageUrl$} ------------------内容链接
{$News_ClassName$} ----------------分类名称
{$News_ClassUrl$} -----------------分类链接
{$News_ShortContent$}  ------------简述
{$News_Author$}  ------------------作者
{$News_Editor$}  ------------------责任编辑
{$News_Images$}  ------------------图片
{$News_Content$}  -----------------内容
{$News_AddTime$} ------------------添加时间
{$News_UpTime$}  ------------------更新时间
{$News_Count1$}  ------------------点击数--不增加
{$News_Count2$}  ------------------点击数--增加<%
End If
If Special=3 Then
%>
产品模块 /// 产品标签
#################################
{$Prod_ID$}  ----------------------ID序号
{$Prod_Title$}  -------------------产品名称
{$Prod_PageUrl$} ------------------内容链接
{$Prod_ClassName$} ----------------分类名称
{$Prod_ClassUrl$} -----------------分类链接
{$Prod_Num$}  ---------------------产品编号
{$Prod_Rmb$}  ---------------------价格
{$Prod_ProductsCode$}  ------------型号
{$Prod_ShortContent$}  ------------简述
{$Prod_Author$}  ------------------作者
{$Prod_Images$}  ------------------图片
{$Prod_Content$}  -----------------内容
{$Prod_AddTime$} ------------------添加时间
{$Prod_UpTime$}  ------------------更新时间
{$Prod_Count1$}  ------------------点击数--不增加
{$Prod_Count2$}  ------------------点击数--增加<%
End If
If Special=4 Then
%>
下载模块 /// 下载标签
#################################
{$Down_ID$}  ----------------------ID序号
{$Down_Title$}  -------------------产品名称
{$Down_PageUrl$} ------------------内容链接
{$Down_ClassName$} ----------------分类名称
{$Down_ClassUrl$} -----------------分类链接
{$Down_MB$}  ----------------------软件大小
{$Down_DownUrl$}  -----------------下载地址
{$Down_ShortContent$}  ------------简述
{$Down_Author$}  ------------------作者
{$Down_Images$}  ------------------图片
{$Down_Content$}  -----------------内容
{$Down_AddTime$} ------------------添加时间
{$Down_UpTime$}  ------------------更新时间
{$Down_Count1$}  ------------------点击数--不增加
{$Down_Count2$}  ------------------点击数--增加<%
End If
%></textarea></td>
</tr>
<tr>
<td height="20" align="right">
<input type="button" name="Submit3" value="确  认" onclick="window.close();opener.focus();">
<input type="button" name="Submit3" value="关闭" onclick="window.close();opener.focus();"></td>
</tr>
</table>
<%
End Sub
%>
</body></html><%
Sub Label(LabelID)  '判断ID
If LabelID=0 Then
    Set Rs=Server.CreateObject("ADODB.RecordSet")
Dim Sql
	Sql="Select Top 1 ID From [Easyes_Label] Order By ID Desc"
Dim rs
	Set Rs=Conn.ExeCute(Sql)
If Not(rs.eof or rs.bof) then
Dim ID
	ID=Rs("ID")+1
	Response.write "<input type=""text"" size=""3""  name=""ESLable"" class=""input_text"" value="""&ID&"""  readonly>"
Else
	Response.write "<input type=""text"" size=""3""  name=""ESLable"" class=""input_text"" value=""1""  readonly>"
End If
	Rs.close
	set rs=nothing
Else
	Response.write "<input type=""text"" size=""3""  name=""ESLable"" class=""input_text"" value="""&LabelID&"""  readonly>"
End If
End Sub
%>