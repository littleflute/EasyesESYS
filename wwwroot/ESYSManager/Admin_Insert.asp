﻿<%@language=vbscript codepage=65001%>
<%Option Explicit%>
<%
Response.CodePage=65001
Response.Charset="UTF-8"
Session.CodePage = 65001
'**************************************************************
' Software Name: EasyesESYS
' Web: http://www.Easyes.com.cn
'**************************************************************
Response.buffer=true	
Const PurviewLevel=1    '操作权限 1超级用户2高级管理员3信息录入员
%>
<!--#include file="Config/ChkPurview.asp"-->
<!--#include file="Config/Config.asp"-->
<!--#include file="Config/Conn.asp"-->
<!--#include file="Config/function.asp"-->
<!--#include file="Label/TemplateClass.asp"-->
<html>
<head>
<title>资源嵌套管理</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="Version" content="<%=SystemVer%>">
<link href="Images/Style.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="Config/Button_Skin.js"></script>
<%Ajax()%></head>
<body>
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="tabTitleBar" height="26"><tr><td width="20" align="center"><img border="0" src="Images/file.png"></td><td>当前位置 &gt; <a href="Admin_Insert.asp">资源嵌套管理</a></td></tr></table>
<%
Dim Action,ParentID,i,FoundErr,ErrMsg
Action=trim(request("Action"))
if Action="Add" then
	call AddCentont()
elseif Action="SaveAdd" then
	call SaveAdd()
elseif Action="Modify" then
	call Modify()
elseif Action="SaveModify" then
	call SaveModify()
elseif Action="InsertSysActive" then
	call InsertSysActive()
elseif Action="DoInsertSysActive" then
	call DoInsertSysActive()	
elseif Action="Del" then
	call DelLable()
else
	call main()
end if
if FoundErr=True then
	call WriteErrMsg(ErrMsg)
end if
Call CloseConn()
sub main()  '列表
%>
<input name="Submit32" type="button" class="button01-out" value="添加嵌套" onClick="window.location='Admin_Insert.asp?Action=Add'" title="添加新的嵌套">
<input name="Submit32" type="button" class="button01-out" value="执行嵌套" onClick="window.location='Admin_Insert.asp?Action=InsertSysActive'" title="执行嵌套">
<%
Dim Work
    Work=Request("Work")
Dim sKey
    sKey=jencode(Replace(Request("sKey"),"'",""))
Dim Rs
    Set Rs=Server.CreateObject("ADODB.RecordSet")
Dim Sql
    Sql="Select * From Easyes_Insert Where Title Like '%"&sKey&"%' Order By Id DESC"
    Rs.PageSize=20
	Rs.CacheSize=Rs.PageSize
    Rs.Open Sql,Conn,1,1
Dim CurrentPage
    If Request("CurrentPage")="" Then
        CurrentPage=1
    Else
        CurrentPage=Request("CurrentPage")
    End If    
    If Not(Rs.Eof And Rs.Bof) Then
        Rs.AbsolutePage=CurrentPage
    End If
%><table border="0" cellpadding="2" cellspacing="1" class="tabBgColor" width="100%">
<tr> 
<td height="25" align="center" class="tabheadfont" width="40">ID</td>
<td height="25" align="center" class="tabheadfont">嵌套标题名称</td>
<td height="22" align="center" class="tabheadfont" width="120">更新时间</td>
<td height="22" align="center" class="tabheadfont" width="120">添加时间</td>
<td width="80" height="22" align="center" class="tabheadfont">常规操作</td>
</tr>
<%
  Dim I
  For I=1 To Rs.PageSize
      If Rs.Eof Then
        Exit For
    End If
%><tr class="tdbg">
<td align="center" width="40"><%=rs("ID")%></td>
<td><img border="0" src="Images/arrow_r.gif">&nbsp;<%Response.write "&nbsp;<a href='Admin_Insert.asp?Action=Modify&ID=" & rs("ID") &"'>"&juncode(Rs("Title"))&"</a>"%></td>
<td align="center"> <%=Rs("upTime")%></td>
<td align="center"> <%=Rs("AddTime")%></td>
<td align="center"><%
response.write "<a href='Admin_Insert.asp?Action=Modify&ID=" & rs("ID") &"'>编辑</a>&nbsp;"
response.write "<a href='Admin_Insert.asp?Action=Del&ID=" & rs("ID") & "' onClick=""return confirm('确定要删除吗？');"">删除</a>&nbsp;"
%></td>
</tr>
<%
  Rs.MoveNext
  Next
%></table> 
<table border="0" cellpadding="2" cellspacing="0" width="100%">
<tr> 
<td align="right" bgcolor="#f6f6f6">
<script type="text/javascript" src="Config/PageList.js"></script>
<script type="text/javascript" language="JavaScript">
	<!--
	var plb = new PageListBar('plb',<%=Rs.PageCount%>,<%=CurrentPage%>,'&sKey=<%=sKey%>',20);
	document.write(plb);
	//-->
</script>
</td>
</tr>
</table>
<form name="form1" method="post" action="Admin_Insert.asp?">
<table width="100%" border="0" cellpadding="3" cellspacing="1" class="tabBgColor">
<tr><td width="67%" align="right" class="tdBg">
<input name="Work" type="hidden" id="Work" value="">搜索<u>嵌套标题</u>: </td>
<td width="25%" align="right" class="tdBg"><input name="sKey" type="text" class="input_text" id="sKey" style="width:100%" value="<%=Trim(Request("sKey"))%>"></td>
<td width="5%" align="center" class="tdBg"><input name="SearchButton" type="submit" class="button02-out" value="确  定"></td>
</tr>
</table>
</form><%
Rs.Close
Set Rs=Nothing
End sub
Sub AddCentont
%>
<form name="myform" method="post" action="Admin_Insert.asp?Work=SaveAddReco" class="required-validate">
<table width="100%" border="0" cellpadding="2" cellspacing="1" class="tabBgColor">
<tr> 
<td colspan="2" align="center" class="tabheadfont">添加嵌套内容</td>
</tr>
<tr>
<td align="right" class="tdbgfont">嵌套标题:</td>
<td class="tdbg"><input name="Title" type="text" size="40" class="input_text required"></td>
</tr>
<tr>
<td align="right" class="tdbgfont">起始标签:</td>
<td class="tdbg"><input name="StartElement" type="text" class="input_text required" size="40" > <font color="#808080">例:&lt;!--标签名称:开始--&gt;</font></td>
</tr>
<tr>
<td align="right" class="tdbgfont">结束标签:</td>
<td class="tdbg"><input name="EndElement" type="text" class="input_text required" size="40" > <font color="#808080">例:&lt;!--标签名称:结束--&gt;</font></td>
</tr>
<tr> 
<td height="22" class="tdbgfont" bgcolor="#F6F6F6" colspan="2">
<font color="#0000FF">嵌套内容</font>:<span style="cursor:hand" Id="ShowHiddenHtmlEdit" Title="显示/隐藏" onClick="if(trNewsContent.style.display==''){trNewsContent.style.display='none';ShowHiddenHtmlEdit.innerHTML='[显示]'}else{trNewsContent.style.display='';ShowHiddenHtmlEdit.innerHTML='[隐藏]'}">[隐藏]</span></td>
</tr>
<tr id="trNewsContent"> 
<td class="tdbgfont" colspan="2">
<%
Response.write"<textarea name=""Content"" style=""display:none"" class=""input_text required""></textarea>"&chr(13)
call Admin_HtmlEditor()
%>
</td>
</tr>
<tr>
<td align="right" class="tdbgfont">应用文件:<br><font color="#999999">一行一个文件地址</font></td>
<td class="tdbg"><textarea rows="4" name="FileList" cols="73" class="input_text required"></textarea></td>
</tr>
<tr class="tdbg"> 
<td align="right">
<input name="Action" type="hidden" id="Action" value="SaveAdd"></td>
<td>
<input name="SaveAddButton" id="SaveAddButton" class="button02-out" type="submit" value=" 保 存 ">
<input name="Submit2" class="button02-out" type="reset" value=" 还 原 ">
<input name="Submit2" type="reset" class="button02-out" value="返 回" onClick="window.location.href='Admin_Insert.asp'"></td>
</tr>
</table></form>
<%
End Sub
sub Modify()
	dim ID
	ID=trim(request("ID"))
	if ID="" then
		FoundErr=True
		ErrMsg=ErrMsg & "·请指定要修改自定义标签！\n"
		exit sub
	end if
Dim Rs
    Set Rs=Conn.ExeCute("Select * From Easyes_Insert Where Id="&ID)
    If Rs.Eof And Rs.Bof Then
        Rs.Close
        Set Rs=Nothing
        Response.Write("记录未找到")
        Response.End
    End If
    Dim Title,StartElement,EndElement,FileList,Content
    Id=Rs("Id")
    Title=Rs("Title")
    StartElement=Rs("StartElement")
    EndElement=Rs("EndElement")
    FileList=Rs("FileList")
    Content=Rs("Content")
    Rs.Close
    Set Rs=Nothing
%>
<form name="form2" method="post" action="Admin_Insert.asp?Work=SaveAddReco" class="required-validate">
<table width="100%" border="0" cellpadding="2" cellspacing="1" class="tabBgColor">
<tr> 
<td colspan="2" align="center" class="tabheadfont">编辑嵌套内容</td>
</tr>
<tr> 
<td align="right" class="tdbgfont">标签标题:</td>
<td class="tdbg"><input name="Title" type="text" class="input_text required" size="40" value="<%=juncode(Server.HTMLEncode(Title))%>" ></td>
</tr>
<tr> 
<td align="right" class="tdbgfont">起始标签:</td>
<td class="tdbg"><input name="StartElement" type="text" class="input_text required" size="40" value="<%=Server.HTMLEncode(StartElement)%>" ><font color="#808080">例:&lt;!--标签名称:开始--&gt;</font></td>
</tr>
<tr> 
<td align="right" class="tdbgfont">结束标签:</td>
<td class="tdbg"><input name="EndElement" type="text" class="input_text required" size="40" value="<%=Server.HTMLEncode(EndElement)%>" ><font color="#808080">例:&lt;!--标签名称:结束--&gt;</font></td>
</tr>
<tr> 
<td height="22" class="tdbgfont" bgcolor="#F6F6F6" colspan="2">
<font color="#0000FF">嵌套内容</font>:<span style="cursor:hand" Id="ShowHiddenHtmlEdit" Title="显示/隐藏" onClick="if(trNewsContent.style.display==''){trNewsContent.style.display='none';ShowHiddenHtmlEdit.innerHTML='[显示]'}else{trNewsContent.style.display='';ShowHiddenHtmlEdit.innerHTML='[隐藏]'}">[隐藏]</span></td>
</tr>
<tr id="trNewsContent"> 
<td class="tdbgfont" colspan="2"><%
Response.write"<textarea name=""Content"" style=""display:none"" class=""input_text required"">"&Server.HTMLEncode(Content)&"</textarea>"&chr(13)
call Admin_HtmlEditor()
%></td>
</tr>
<tr> 
<td align="right" class="tdbgfont">应用文件:<br><font color="#999999">一行一个文件地址</font></td>
<td class="tdbg"><textarea rows="4" name="FileList" cols="73" class="input_text required"><%=FileList%></textarea></td>
</tr>
<tr class="tdbg"> 
<td align="right">
<input name="ID" type="hidden" id="ClassID" value="<%=ID%>">
<input name="Action" type="hidden" id="Action" value="SaveModify">
</td>
<td>
<input name="SaveAddButton" id="SaveAddButton" class="button02-out" type="submit" value=" 保 存 ">
<input name="Submit2" class="button02-out" type="reset" value=" 还 原 ">
<input name="Submit2" type="reset" class="button02-out" value="返 回" onClick="window.location.href='Admin_Insert.asp'">
</td>
</tr>
</table></form>
<%
end sub
Sub InsertSysActive()
%><form name="form3" method="post" action="Admin_Insert.asp?Action=DoInsertSysActive">
<table width="100%" height="116" border="0" cellpadding="2" cellspacing="1" class="tabBgColor" align="center">
<tr> 
<td height="21" align="center" class="tabheadfont">执行页面内容替换命令(请使用Ctrl\Shift组合键)</td>
</tr>
<tr>
<td height="92" align="center" valign="top" bgcolor="#FFFFFF">
<select name="RecordList" size="25" multiple class="input_text" style="width:100%">
<%
    Dim Sql
        Sql="Select ID,Title,UpTime From Easyes_Insert Order By ID DESC"
    Dim Rs
    Set Rs=Conn.ExeCute(Sql)
    While Not Rs.Eof
%><option value="<%=Rs("Id")%>"><%=juncode(Rs("Title"))%></option>
<%
    Rs.MoveNext
    Wend
    Rs.Close
    Set Rs=Nothing
%></select></td>
</tr>
<tr>
<td align="center" valign="top" bgcolor="#FFFFFF">
<input name="Submit5" type="submit" class="button01-out" value="确  定">
<input name="Submit23" type="reset" class="button01-out" value="还  原">
<input name="Submit2" type="reset" class="button01-out" value="返 回" onClick="window.location.href='Admin_Insert.asp'">
</td>
</tr>
</table>
</form><%
End Sub
%>
</body>
</html><%
Sub SaveAdd() '添加
    Dim Sql
        Sql="Select Top 1 * From Easyes_Insert"
    Dim Rs
    Set Rs=Server.CreateObject("ADODB.RecordSet")
        Rs.Open Sql,Conn,1,3
    Rs.AddNew
    Rs("Title")= jencode(Trim(Request("Title")))
    Rs("StartElement")= Trim(Request("StartElement"))
    Rs("EndElement")= Trim(Request("EndElement"))
    Rs("Content")= Trim(Request("Content"))
    Rs("FileList")= Trim(Request("FileList"))
    Rs("AddTime")= Now
    Rs("upTime")= Now
    Rs.Update
    Rs.Close
    Set Rs=Nothing
    Response.Redirect("Admin_Insert.asp")
End sub
sub SaveModify() '修改
Dim ID
    ID= Trim(Request("ID"))
    Dim Sql
        Sql="Select * From Easyes_Insert Where ID=" & ID
    Dim Rs
    Set Rs=Server.CreateObject("ADODB.RecordSet")
        Rs.Open Sql,Conn,1,3
	If Rs.Eof And Rs.Bof Then
		FoundErr=True
		ErrMsg=ErrMsg & "·找不到自定义嵌套内容！\n"
		rs.close
		set rs=Nothing
    	Exit sub
	end if
    Rs("Title")= jencode(Trim(Request("Title")))
    Rs("StartElement")= Trim(Request("StartElement"))
    Rs("EndElement")= Trim(Request("EndElement"))
    Rs("Content")= Trim(Request("Content"))
    Rs("FileList")= Trim(Request("FileList"))
    Rs("upTime")= Now
    Rs.Update
    Rs.Close
    Set Rs=Nothing
    Response.Redirect("Admin_Insert.asp")
End sub
Sub DelLable() '删除
dim FoundErr,ErrMsg
	if session("purview")>1 then
		call WriteErrMsg("·没有操作权限！\n")
	end if
Dim ComeUrl
	ComeUrl=Request.ServerVariables("HTTP_REFERER")
Dim ID
	ID=trim(request("ID"))
Dim Sql
	Sql="Delete From [Easyes_Insert] Where ID=" &ID
	Conn.ExeCute(Sql)
	Call CloseConn()      
	response.redirect ComeUrl
End Sub
Sub DoInsertSysActive() '执行替换
    Dim RecordList
        RecordList=Trim(Replace(Request("RecordList")," ",""))
    If RecordList="" Then
		Call WriteErrMsg("·请您选择要执行的[内容替换命令]！\n")
    End If
    Dim arrRecordList
        arrRecordList=Split(RecordList,",",-1,1)
    Dim Sql
        Sql=""
    Dim Rs
    Dim arrFileList
    Dim TClass
    Set TClass=New ESYS_TemplateClass
    Dim I,J
    For I=0 To UBound(arrRecordList)
        Sql="Select * From Easyes_Insert Where ID=" & arrRecordList(I)
        Set Rs=Conn.ExeCute(Sql)
        If Not(Rs.Eof And Rs.Bof) Then
            arrFileList=Split(Rs("FileList"),vbCrLf,-1)
            For J=0 To UBound(arrFileList)
                With TClass
                    .OpenTemplate(Server.MapPath(arrFileList(J)))
                    .StartElement=Rs("StartElement")
                    .EndElement=Rs("EndElement")
                    .Value=Rs("Content")
                    .ReplaceTemplate()
                    .Save()
                End With
            Next
        End If
        Rs.Close
    Next
    Set TClass=Nothing
    Set Rs=Nothing
		Call WriteSuccessMsg("·内容替换完毕！\n")
End Sub
%>