﻿<%@language=vbscript codepage=65001%>
<%Option Explicit%>
<%
Response.CodePage=65001
Response.Charset="UTF-8"
Session.CodePage = 65001
'**************************************************************
' Software Name: EasyesESYS
' Web: http://www.Easyes.com.cn
'**************************************************************
Response.buffer=true	
Const PurviewLevel=2    '操作权限 1超级用户2高级管理员3信息录入员
%>
<!--#include file="Config/ChkPurview.asp"-->
<!--#include file="Config/Conn.asp"-->
<!--#include file="Config/function.asp"-->
<html>
<head>
<title>易讯网络建站管理系统--留言管理</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="Images/Style.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="Config/Button_Skin.js"></script>
</head>
<body>
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="tabTitleBar" height="26"><tr><td width="20" align="center"><img border="0" src="Images/file.png"></td><td>当前位置 &gt; <a href="Plug_GuestBook.asp">留言板管理</a></td></tr></table>
<%
Dim Action,FoundErr,ErrMsg
Action=trim(request("Action"))
if Action="Modify" then
	Call Modify()
ElseIf Action="SaveModify" then
	Call SaveModify()
Elseif Action="Del" then
	Call DelChannel()
Else	
	Call main()
End If
If FoundErr=True then
	call WriteErrMsg(ErrMsg)
End if
Call CloseConn()
Sub Main()
Dim Rs
    Set Rs=Server.CreateObject("ADODB.RecordSet")
Dim Sql
    Sql="select * from Easyes_GuestBook order by GuestID desc"
    Rs.PageSize=20
	Rs.CacheSize=Rs.PageSize
    Rs.Open Sql,Conn,1,1
Dim CurrentPage
    If Request("CurrentPage")="" Then
        CurrentPage=1
    Else
        CurrentPage=Request("CurrentPage")
    End If    
    If Not(Rs.Eof And Rs.Bof) Then
        Rs.AbsolutePage=CurrentPage
    End If	
%>
<table border="0" cellpadding="2" cellspacing="1" class="tabBgColor" style="width:100%">
<tr height="22"class="tabheadfont"> 
<td align="center" class="tabheadfont" width="35">ID</td>
<td align="center" class="tabheadfont">留言内容[编辑与回复]</td>
<td align="center" class="tabheadfont" style="width: 60">审核</td>
<td align="center" class="tabheadfont" style="width: 120">日期</td>
<td align="center" class="tabheadfont" style="width: 60">留言属性</td>
<td align="center" class="tabheadfont" style="width: 60">留言状态</td>
<td align="center" class="tabheadfont" width="42">操作</td>
</tr>
<%
'翻页循环开始
  Dim I
  For I=1 To Rs.PageSize
    If Rs.Eof Then
        Exit For
    End If
%>
<tr class="tdbgfont"> 
<td align="center" width="35"><%=rs("GuestID")%></td>
<td><%=rs("Comments")%></td>
<td align="center"><a href='Plug_GuestBook.asp?Action=Modify&amp;ID=<%=rs("GuestID")%>'>回复</a></td>
<td align="center"><%=rs("Postdate")%></td>
<td align="center"><%if rs("Passed")=True then response.write "<font color=red>通过</font>" else response.write "未审"  end if%></td>
<td align="center"><%
if rs("Replay")="" or IsNull(rs("Replay"))=true then
	response.write "<font color=red>新留言</font>"
else				
	response.write "已回复"
end if
%></td>
<td align="center" width="42"><a href='Plug_GuestBook.asp?Action=Del&amp;ID=<%=rs("GuestID")%>'>删除</a></td>
</tr><%
  Rs.MoveNext
  Next
%>
<tr><td height="21" align="center" colspan="7" class="tdbgfont">
<script type="text/javascript" src="Config/PageList.js"></script>
<script type="text/javascript" language="JavaScript">
	<!--
	var plb = new PageListBar('plb',<%=Rs.PageCount%>,<%=CurrentPage%>,'',20);
	document.write(plb);
	//-->
</script>
</td>
</tr>
</table>
<%
end sub
sub Modify()
Dim ID
	ID=Request("ID")
Dim rsPic
	set rsPic=server.createobject("ADODB.Recordset")
Dim sqlPic
	sqlPic="select * from [Easyes_GuestBook] where GuestID=" & ID
	rsPic.Open sqlPic,conn,1,3
%>
<form name="FeedBack" method="post" action="Plug_GuestBook.asp?Action=SaveModify">
<input name="ID" type="hidden" id="ID" value="<%=rsPic("GuestID")%>">
<table width="100%" border="0" cellpadding="3" class="tabBgColor" cellspacing="1">
<tr><TD align="right" width=20% class="tdbgFont">IP地址：</TD><td class="tdbgfont"><%=rsPic("IP")%></td></tr>
<tr><TD align="right" width=20% class="tdbgFont">留言日期：</TD><td class="tdbgfont"><%=rsPic("PostDate")%></td></tr>		 
<tr><TD align="right" width=20% class="tdbgFont">留言人姓名：</TD><td class="tdbgfont"><%=rsPic("UserName")%></td></tr>
<tr><TD align="right" width=20% class="tdbgFont">用户信息：</TD><td class="tdbgfont"><%=rsPic("UserInfo")%></td></tr>
<tr><TD align="right" width=20% class="tdbgFont">内容：</TD><td class="tdbgfont"><textarea style="overflow:auto; height: 74px; width: 476px;" name="comments" class="input_text"><%=rsPic("Comments")%></textarea></td></tr>
<tr><TD align="right" width=20% valign=top class="tdbgFont">回复内容：</TD><td class="tdbgfont"><textarea style="overflow:auto; height: 60px;" name="Replay" cols="60" class="input_text"><%=rsPic("Replay")%></textarea>&nbsp;</td></tr>
<tr><TD align="right" width=20% class="tdbgFont">是否隐藏：</TD><td class="tdbgfont"><input type="radio" name="Passed" value="1" <%if rsPic("Passed")=True then%>checked<%end if%> style="width: 20px">通过 <input type="radio" name="Passed" value="0" <%if rsPic("Passed")=False then%>checked<%end if%> style="width: 20px">未审</td></tr>
<TR><TD align="right" width=20% class="tdbgfont"></TD>
<TD class="tdbgfont"><input type="submit" name="action" value=" 提 交 " class="button01-out"></TD></TR>
</table>
</form>
<%	
rsPic.close
set rsPic=nothing
end sub
%></body>
</html><%
Sub SaveModify()
Dim ID,Comments,Replay,Passed
	ID=trim(request("ID"))
	Comments=trim(request("comments"))
	Replay=request("Replay")
	Passed=request("Passed")
Dim sql
	sql="Select * From [Easyes_GuestBook] Where GuestID=" & ID
Dim rs
	Set rs=Server.CreateObject("Adodb.RecordSet")
	rs.open sql,conn,1,3
	if rs.bof and rs.EOF then
		FoundErr=True
		ErrMsg=ErrMsg & "<br><li>找不到指定的栏目！</li>"
		rs.close
	    set rs=Nothing
    else
	    rs("comments")=comments
	    rs("Replay")=Replay
	    rs("ReplayDate")=Now()
	IF Passed=1 Then
	    rs("Passed")=True
	Else
	    rs("Passed")=False 
	End If	
		rs.update
		rs.Close
		set rs=Nothing
		Response.Redirect "Plug_GuestBook.asp"  
	end if
End Sub

sub DelChannel()
Dim ComeUrl
	ComeUrl=Request.ServerVariables("HTTP_REFERER")
Dim ID
	ID=trim(request("ID"))
	if ID="" then
		FoundErr=True
		ErrMsg=ErrMsg & "<br><li>请指定要删除的留言</li>"
	else
		ID=Clng(ID)
	end if
	conn.Execute "delete from [Easyes_GuestBook] where GuestID=" & Clng(ID)
	call CloseConn()      
	response.redirect ComeUrl
end sub
%>