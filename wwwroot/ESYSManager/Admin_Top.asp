﻿<%@language=vbscript codepage=65001%>
<%Option Explicit%>
<%
Response.CodePage=65001
Response.Charset="UTF-8"
Session.CodePage = 65001
'**************************************************************
' Software Name: EasyesESYS
' Web: http://www.Easyes.com.cn
'**************************************************************
Response.buffer=true	
Const PurviewLevel=3    '操作权限
%>
<!--#include file="Config/Config.asp"-->
<!--#include file="Config/Function.asp"-->
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>菜单</title>
<link href="Images/style.css" rel="stylesheet" type="text/css">
<style type="text/css">
 body{margin:0px;}
</style>
<script type="text/javascript" language="JavaScript">
<!--
var FrameState = true;
var iniFrameCols;
function HiddenMenu()
{
    if(FrameState){
        iniFrameCols = top.Frame1.cols;
        top.Frame1.cols = "0,*";
        FrameState = 0
        document.all["MenuButton"].innerHTML = "展开菜单";
        document.all["MenuButton"].title = "展开菜单";
    }else{
        top.Frame1.cols = iniFrameCols;
        FrameState = 1;
        document.all["MenuButton"].innerHTML = "关闭菜单";
        document.all["MenuButton"].title = "关闭菜单";
    }
}
//-->
</script>
</head>
<body>
<div id="TopFrame">
<div class="menu left">
<a href="javascript:HiddenMenu();void(0);" title="关闭菜单" id="MenuButton" onfocus="this.blur()">关闭菜单</a> | 
<a href="#" onclick="window.history.back()" class="menuitem">后 退</a> | 
<a href="#" onclick="window.history.forward()" class="menuitem">前 进</a> | 
</div>
<div class="menu right">
<ul>
<li><a href="<%=SiteUrl%>" class="menuitem" target="_blank">网站首页</a>┆</li>
<li><a href="Admin_Main.asp" class="menuitem" target="MainFrame">管理首页</a>┆</li>
<li><a href="Admin_ModifyPwd.asp" class="menuitem" target="MainFrame">修改密码</a>┆</li>
<li><a href="Admin_Logout.asp" class="menuitem" target="_top">关闭系统</a>┆</li>
<%
if session("purview")=1 then
	Response.write "<li><a href=""Admin_Help.asp"" class=""menuitem"" target=""MainFrame"">系统帮助</a></li>"
End if
%>
</ul>
</div>
</div>
</body>
</html>