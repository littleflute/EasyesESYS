﻿<%@language=vbscript codepage=65001%>
<%Option Explicit%>
<%
Response.CodePage=65001
Response.Charset="UTF-8"
Session.CodePage = 65001
'**************************************************************
' Software Name: EasyesESYS
' Web: http://www.Easyes.com.cn
'**************************************************************
response.buffer=true	
Const PurviewLevel=1    '操作权限 1超级用户2高级管理员3信息录入员
%>
<!--#include file="Config/ChkPurview.asp"-->
<!--#include file="Config/Config.asp"-->
<!--#include file="Config/function.asp"-->
<!--#include file="Config/Conn.asp"-->
<!--#include file="Config/Class.FSO.asp"-->
<html>
<head>
<title>高级扩展标签</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="Version" content="<%=SystemVer%>">
<link href="Images/Style.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="Config/Button_Skin.js"></script>
<script src="Config/Windows.js" type="text/javascript"></script>
</head>
<body>
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="tabTitleBar" height="26"><tr><td width="20" align="center"><img border="0" src="Images/file.png"></td><td>当前位置 &gt; 扩展功能菜单</td></tr></table><%
Dim Action
	Action=trim(request("Action"))
Select Case Action
    Case "SaveLable"
        Call SaveMdy("Label/MyLabel.asp")
    Case ""
        Call main()
End Select
Sub Main()
	Dim fileCode
	fileCode=FSO.ReadFileStream(Server.MapPath("Label/MyLabel.asp"),"UTF-8")
    set FSO=Nothing
%>
<form name="form2" method="post" action="?">
<input name="Action" type="hidden" id="Action" value="SaveLable">
<table width="100%" border="0" cellpadding="0" cellspacing="1" class="tabBgColor">
<tr><td align="center" class="tabheadfont">高级扩展标签<a href=javascript:openwin2("Admin_Sdk.htm",450,400)>[开发文档]</a>（<span class="red">不懂ASP编程建议不要使用本功能</span>）</td></tr>
<tr class="tdbg"><td height="500" bgcolor="#FFFFFF"><textarea name="Content" wrap="OFF" class="Input" id="Content" style="width:100%;height:100%"><%=fileCode%></textarea></td></tr>
<tr class="tdbg"><td align="center"><input name="Submit" type="submit" class="button01-out" value="确  定"> <input name="Submit2" type="reset" class="button01-out" value="还  原"></td></tr>
</table>
</form> 
<%
End Sub
%>
</body>
</html>
<%
Sub SaveMdy(UpdateFile)
    Dim Content
        Content=Request("Content")
	Call FSO.CreateFileStream(Server.MapPath(UpdateFile),Content,"UTF-8")
	set FSO=Nothing
	Call WriteSuccessMsg("·高级标签修改已保存！\n")
	Response.End()
End Sub
%>