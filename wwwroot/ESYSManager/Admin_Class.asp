﻿<%@language=vbscript codepage=65001%>
<%Option Explicit%>
<%
Response.CodePage=65001
Response.Charset="UTF-8"
Session.CodePage = 65001
'**************************************************************
' Software Name: EasyesESYS
' Web: http://www.Easyes.com.cn
'**************************************************************
Response.buffer=true	
Const PurviewLevel=1    '操作权限 1超级用户2高级管理员3信息录入员
%>
<!--#include file="Config/ChkPurview.asp"-->
<!--#include file="Config/Config.asp"-->
<!--#include file="Config/Conn.asp"-->
<!--#include file="Config/function.asp"-->
<!--#include file="Config/ClassList_Fun.asp"--><html>
<head>
<title>分类管理</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="Version" content="<%=SystemVer%>">
<link href="Images/Style.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="Config/Button_Skin.js"></script>
<%Call ajax()%></head>
<body>
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="tabTitleBar" height="26"><tr><td width="20" align="center"><img border="0" src="Images/file.png"></td><td>当前位置 &gt; <a href="Admin_Class.asp">资源分类管理</a></td></tr></table><%
Dim Action,ParentID,i,FoundErr,ErrMsg
Action=trim(request("Action"))
if Action="Add" then
	call AddClass()
elseif Action="SaveAdd" then
	call SaveAdd()
elseif Action="Modify" then
	call Modify()
elseif Action="SaveModify" then
	call SaveModify()
elseif Action="Del" then
	call DelClass()
else
	call main()
end if
if FoundErr=True then
	call WriteErrMsg(ErrMsg)
end if
call CloseConn()

Sub main()   '栏目列表
Dim ParentID
	ParentID=trim(request("ParentID"))
if ParentID="" then
	ParentID=0
else
	ParentID=CLng(ParentID)
end if

Dim Rs
    Set Rs=Server.CreateObject("ADODB.RecordSet")
Dim Sql
    Sql="SELECT * From [Easyes_Class] Where ParentID="&ParentID&" Order By UpTime desc"
    Rs.PageSize=20
	Rs.CacheSize=Rs.PageSize
    Rs.Open Sql,Conn,1,1
Dim CurrentPage
    If Request("CurrentPage")="" Then
        CurrentPage=1
    Else
        CurrentPage=Request("CurrentPage")
    End If    
    If Not(Rs.Eof And Rs.Bof) Then
        Rs.AbsolutePage=CurrentPage
    End If
%><input name="Submit32" type="button" class="button01-out" value="添加分类" onClick="window.location='Admin_Class.asp?Action=Add&ParentID=<%=ParentID%>'" title="在当前位置添加新分类">
<table border="0" cellpadding="2" cellspacing="1" class="tabBgColor" width="100%"><tr>
<td colspan="7" class="tdbg"><%=GetClassPath1(ParentID)%> 的子分类列表：</td></tr>
<tr class="tabHeadfont"> 
<td width="60" class="tabheadfont">记录ID</td>
<td width="110" class="tabheadfont">资源所属模块</td>
<td class="tabheadfont">资源分类名称</td>
<td width="120" class="tabheadfont">资源地址</td>
<td width="120" class="tabheadfont">栏目地址</td>
<td width="100" class="tabheadfont">资源列表</td>
<td width="80" class="tabheadfont">常规操作</td>
</tr>
<%
  Dim I
  For I=1 To Rs.PageSize
    If Rs.Eof Then
        Exit For
    End If
%><tr class="tdbg"> 
<td align="center" style="background-color: #FBFCFF"><%=rs("ClassID")%></td>
<td align="center"><%Call Channel(rs("Special"))%></td>
<td><img border="0" src="Images/arrow_r.gif"> <a href="?ParentID=<%=Rs("ClassID")%>"><%=rs("ClassName")%></a></td>
<td align="center"><%=rs("PageUrl")%></td>
<td align="center"><%=rs("ClassUrl")%></td>
<td align="center">
<%
Dim Special
	Special=rs("Special")
Select Case Special
	Case 1
		Response.write "<a href=""Admin_Page.asp?ParentID="&rs("ClassID")&""">资源列表</a>"
	Case 2
		Response.write "<a href=""Admin_News.asp?ParentID="&rs("ClassID")&""">资源列表</a>"
	Case 3
		Response.write "<a href=""Admin_Products.asp?ParentID="&rs("ClassID")&""">资源列表</a>"
	Case 4
		Response.write "<a href=""Admin_Down.asp?ParentID="&rs("ClassID")&""">资源列表</a>"
	End Select
%></td>
<td align="center" width="80"><%
	response.write "<a href='Admin_Class.asp?Action=Modify&ClassID=" & rs("ClassID") & "'>编辑</a>&nbsp;"
	response.write "<a href='Admin_Class.asp?Action=Del&ParentID=" & rs("ParentID") & "&ClassID=" & rs("ClassID") & "&Special=" & rs("Special") & "' onClick=""return confirm('确定要删除此分类吗？');"">删除</a>"
%></td>
</tr>
<%
  Rs.MoveNext
  Next
%></table> 
<table border="0" cellpadding="2" cellspacing="0" width="100%" width="24">
<tr> 
<td align="right" bgcolor="#f6f6f6">
<script type="text/javascript" src="Config/PageList.js"></script>
<script type="text/javascript" language="JavaScript">
	<!--
	var plb = new PageListBar('plb',<%=Rs.PageCount%>,<%=CurrentPage%>,'&ParentID=<%=ParentID%>',20);
	document.write(plb);
	//-->
</script>
</td>
</tr>
</table><%
Rs.Close
Set Rs=Nothing
end sub

Sub AddClass()
Dim Special
ParentID=trim(request("ParentID"))
if ParentID="" then
	ParentID=0
else
	ParentID=CLng(ParentID)
end if
%><form name="form1" method="post" action="Admin_Class.asp" class="required-validate">
<table class="tabBgColor" cellspacing="1" width="100%">
<tr> 
<td class="tabheadfont" colspan="2">添加资源分类</td>
</tr>
<tr> 
<td align="right" class="tdbgfont">详细位置:</td>
<td class="tdbg"><%=GetClassPath1(ParentID)%></td>
</tr><%
If ParentID=0 Then
%><tr> 
<td align="right" class="tdbgfont">所属模块:</td>
<td class="tdbg">
<select name="Special" class="input_text required">
<%call Admin_SpecialAdd()%></select>
</td>
</tr><%
End If
%><tr><td align="right" class="tdbgfont">分类名称:</td><td class="tdbg"><input name="ClassName" class="Input_text required" type="text" size="40"></td></tr>
<tr><td align="right" class="tdbgfont">资源地址:</td><td class="tdbg"><input name="PageUrl" type="text" size="40" class="input_text required"></td></tr>
<tr><td align="right" class="tdbgfont">栏目地址:</td><td class="tdbg"><input name="ClassUrl" type="text" size="40" class="input_text required"></td></tr>
<tr><td align="right" class="tdbgfont">分类说明:</td><td class="tdbg"><textarea rows="7" name="ClassContent" cols="67" class="input_text required"></textarea></td></tr>
<tr> 
<td class="tdbg">
<input name="ParentID" type="hidden" id="ParentID" value="<%=Request("ParentID")%>">
<input name="Action" type="hidden" id="Action" value="SaveAdd"></td>
<td class="tdbg">
<input name="SaveAddButton" id="SaveAddButton" class="button02-out" type="submit" value=" 保 存 ">
<input name="Submit2" class="button02-out" type="reset" value=" 还 原 ">
<input name="Submit2" type="reset" class="button02-out" value="返 回" onClick="window.location.href='Admin_Class.asp'">
</td>
</tr>
</table>
</form><%
End sub
Sub Modify()
Dim ClassID
	ClassID=trim(request("ClassID"))
	if ClassID="" then
		FoundErr=True
		ErrMsg=ErrMsg & "·请指定要修改单页栏目ID\n"
		exit sub
	Else
		ClassID=Clng(ClassID)
	End if
Dim Rs
	Set rs=conn.execute("select * from [Easyes_Class] where ClassID=" & ClassID)
	if rs.bof and rs.eof then
		FoundErr=True
		ErrMsg=ErrMsg & "·找不到指定的栏目！\n"
	Else
%><form method="post" action="Admin_Class.asp" name="form2" class="required-validate">
<input name="ParentID" type="hidden" id="ParentID" value="<%=rs("ParentID")%>">
<input name="ClassID" type="hidden" id="ClassID" value="<%=rs("ClassID")%>">
<input name="Action" type="hidden" id="Action" value="SaveModify">
<table class="tabBgColor" cellspacing="1" width="100%">
<tr>
<td align="right" class="tabheadfont" colspan="2">编辑资源分类</td></tr>
<tr>
<td align="right" class="tdbgfont">详细位置:</td><td class="tdbg"><%=GetClassPath1(rs("ParentID"))%></td></tr><%
If rs("ParentID")=0 Then
%><tr> 
<td align="right" class="tdbgfont">所属模块:</td>
<td class="tdbg">
<select name="Special" class="input_text required">
<%call Admin_SpecialMdy(rs("Special"))%></select>
</td>
</tr><%
End If
%><td align="right" class="tdbgfont">分类名称:</td><td class="tdbg"><input name="ClassName" class="Input_text required" type="text" size="40" value="<%=rs("ClassName")%>"></td></tr>
<tr>
<td align="right" class="tdbgfont">资源地址:</td><td class="tdbg"><input name="PageUrl" type="text" id="ClassUrl0" value="<%=rs("PageUrl")%>" size="40" class="input_text required"></td></tr>
<tr>
<td align="right" class="tdbgfont">栏目地址:</td><td class="tdbg"><input name="ClassUrl" type="text" id="ClassUrl0" value="<%=rs("ClassUrl")%>" size="40" class="input_text required"></td></tr>
<tr>
<td align="right" class="tdbgfont">分类说明:</td><td class="tdbg"><textarea rows="7" name="ClassContent" cols="67" class="input_text required"><%=rs("ClassContent")%></textarea></td></tr>
<tr><td align="right" height="24" class="tdbg">
</td>
<td height="24" class="tdbg">
<input name="SaveAddButton" id="SaveAddButton" class="button02-out" type="submit" value=" 保 存 ">
<input name="Submit2" class="button02-out" type="reset" value=" 还 原 ">
<input name="Submit2" type="reset" class="button02-out" value="返 回" onClick="window.location.href='Admin_Class.asp'">
</td>
</tr>
</table>
</form>
<%
End if
	rs.close
	set rs=nothing
End sub
%></body>
</html>
<%
Sub SaveAdd() '添加
Dim ParentID,Special
	ParentID=trim(request.Form("ParentID"))
If ParentID=0 Then
	Special=trim(request.Form("Special"))
	if Special="" then
		FoundErr=True
		ErrMsg=ErrMsg & "·请选择所属模块！\n"
	End if
End If	
	if FoundErr=True then
		Exit Sub
	End if
'############################
'获得根ID和根的特性
If ParentID=0 Then
	RootID=0
Else
Dim RsRoot,RootID
    set RsRoot= server.CreateObject ("adodb.recordset")
Dim sql
   	sql="Select Top 1 RootID,ClassID,Special,ParentID From [Easyes_Class] Where ClassID="&ParentID
	RsRoot.Open sql,conn,1,1
if Not(RsRoot.eof or RsRoot.bof) then
	Special=RsRoot("Special")
If RsRoot("ParentID")=0 Then
	RootID=RsRoot("ClassID")
Else
	RootID=RsRoot("RootID")
End If	
End If
	RsRoot.close
	set RsRoot=nothing
End If
'############################
	Set rs=Server.CreateObject("Adodb.RecordSet") '栏目入库
    Sql="Select Top 1 * From [Easyes_Class]"
Dim rs
    Rs.Open Sql,Conn,1,3
    rs.addnew
    Rs("ClassName")=Request("ClassName")
    Rs("ParentID")=ParentID
    Rs("PageUrl")=Request("PageUrl")
    Rs("ClassUrl")=Request("ClassUrl")
    Rs("ClassContent")=Request("ClassContent")
    Rs("RootID")=RootID
    Rs("Special")=Special
    Rs("upTime")=Now
	rs.update
    rs.Close
    set rs=Nothing 
    Response.Redirect("Admin_Class.asp?ParentID="&ParentID&"")
End sub

Sub SaveModify()  '修改
Dim ClassID
	ClassID=trim(request.Form("ClassID"))
Dim ParentID,Special
	ParentID=trim(request.Form("ParentID"))
If ParentID=0 Then
	Special=trim(request.Form("Special"))
	if Special="" then
		FoundErr=True
		ErrMsg=ErrMsg & "·请选择所属模块！\n"
	End if
End If	
	if FoundErr=True then
		Exit Sub
	End if
Dim Rs
    Set Rs=Server.CreateObject("ADODB.RecordSet")
Dim Sql,rsSpecial
    Sql="Select Top 1 * From [Easyes_Class] Where ClassID=" & CLng(ClassID)
    Rs.Open Sql,Conn,1,3
    Rs("ClassName")=Request("ClassName")
    Rs("PageUrl")=Request("PageUrl")
    Rs("ClassUrl")=Request("ClassUrl")
    Rs("ClassContent")=Request("ClassContent")
If ParentID=0 Then
    Rs("Special")=Special
End If
    Rs("upTime")=Now
    Rs.Update
'判断是否更新根目录下的所有分类
If ParentID=0 Then
Dim RootID
    RootID=Rs("ClassID")
    conn.Execute "Update [Easyes_Class] set Special="&Special&" Where RootID=" & Clng(RootID)
End If
    rs.Close
    set rs=Nothing
    call CloseConn()
    Response.Redirect("Admin_Class.asp?ParentID="&ParentID&"")
End sub

Sub DelClass()  '删除分类
Dim ClassID,ComeUrl,Special
	ComeUrl=Request.ServerVariables("HTTP_REFERER")
	ClassID=trim(request("ClassID"))
		ClassID=Clng(ClassID)
	ParentID=trim(request("ParentID"))
	Special=trim(request("Special"))
Dim Sql,Rs

'判断是否其下还有资料在，如不为空则该分类不能删除
If Special=1 Then
    Sql="Select Count(*) As Num From [Easyes_Content] Where ClassID In ("&ClassID&")"
ElseIf Special=2 Then
    Sql="Select Count(*) As Num From [Easyes_News] Where ClassID In ("&ClassID&")"
ElseIf Special=3 Then
    Sql="Select Count(*) As Num From [Easyes_Products] Where ClassID In ("&ClassID&")"
ElseIf Special=4 Then
    Sql="Select Count(*) As Num From [Easyes_Down] Where ClassID In ("&ClassID&")"
End If

    Set Rs=Conn.ExeCute(Sql)
    If Rs("Num")>=1 Then
        FoundErr=True
		ErrMsg=ErrMsg & "·其下还有[资源]，应此无法删除。\n请先删除其下的资源后再删除此分类！\n"
		Exit sub
        Rs.Close
    End If
    Rs.Close

    Sql="Select Count(*) As Num From [Easyes_Class] Where ParentID="&ClassID
    Set Rs=Conn.ExeCute(Sql)
    If Rs("Num")>=1 Then
        FoundErr=True
		ErrMsg=ErrMsg & "·其下还有[分类]，无法删除！\n"
		Exit sub
 		Rs.Close
	End If
	Rs.Close

	'开始删除分类
	Sql="Delete From [Easyes_Class] Where ClassID=" &ClassID
	Conn.ExeCute(Sql)
	Response.redirect ComeUrl
end sub
%>