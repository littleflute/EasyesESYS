﻿<%@language=vbscript codepage=65001%>
<%Option Explicit%>
<%
Response.CodePage=65001
Response.Charset="UTF-8"
Session.CodePage = 65001
'**************************************************************
' Software Name: EasyesESYS
' Web: http://www.Easyes.com.cn
'**************************************************************
Response.buffer=true	
Const PurviewLevel=1    '操作权限 1超级用户2高级管理员3信息录入员
%>
<!--#include file="Config/ChkPurview.asp"-->
<!--#include file="Config/function.asp"-->
<!--#include file="Config/Config.asp"-->
<!--#include file="Config/Conn.asp"-->
<!--#include file="Config/md5.asp"-->
<%
dim strPurview,iCount
dim Action,FoundErr,ErrMsg
Action=Trim(request("Action"))
%><html>
<head>
<title>管理员管理</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="Version" content="<%=SystemVer%>">
<link href="Images/Style.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="Config/Button_Skin.js"></script>
<%ajax()%></head>
<body>
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="tabTitleBar" height="26"><tr><td width="20" align="center"><img border="0" src="Images/file.png"></td><td>当前位置 &gt; <a href="Admin_MangerUser.asp">管理员设置</a></td></tr></table><%
if Action="Add" then
	call AddAdmin()
elseif Action="SaveAdd" then
	call SaveAdd()
elseif Action="Modify" then
	call Modify()
elseif Action="SaveModify" then
	call SaveModify()
elseif Action="Del" then
	call DelAdmin()
else
	call main()
end if
if FoundErr=True then
	call WriteErrMsg(ErrMsg)
else
	call ShowHelp()
end if
Call CloseConn()
Sub main()
%><input name="Submit32" type="button" class="button01-out" value="添加管理" onClick="window.location='Admin_MangerUser.asp?Action=Add'" title="添加新管理员"><%
Dim rs
	Set rs=Server.CreateObject("Adodb.RecordSet")
Dim Sql
	sql="select * from Easyes_Manage order by ID"
rs.Open sql,conn,1,1
iCount=rs.recordcount
%><table border="0" cellpadding="2" cellspacing="1" class="tabBgColor" width="100%">
<tr align="center">
<td width="30" class="tabHeadFont">选中</td>
<td width="30" class="tabHeadFont">序号</td>
<td height="25" class="tabHeadFont">用户名</td>
<td width="100" class="tabHeadFont">权限</td>
<td width="100" class="tabHeadFont">最后登录IP</td>
<td width="150" class="tabHeadFont">最后登录时间</td>
<td width="60" class="tabHeadFont">登录次数</td>
<td width="100" class="tabHeadFont">操 作</td>
<%do while not rs.EOF %>
<tr class="tdbg">
<td width="30" align="center"><input name="ID" type="checkbox" id="ID" value="<%=rs("ID")%>" <%if rs("UserName")=session("Admin") then response.write " disabled"%> onclick="unselectall()"></td>
<td width="30" align="center"><%=rs("ID")%></td>
<td align="center"><%
if rs("username")=session("Admin") then
response.write "<font color=red><b>" & rs("UserName") & "</b></font>"
else
response.write rs("UserName")
end if
%></td>
<td width="100" align="center"> <%
select case rs("purview")
case 1
strPurview="超级用户"
case 2
strpurview="高级管理员"
case 3
strpurview="信息录入员"
end select
response.write(strPurview)
%></td>
<td width="100" align="center"><%
if rs("LastLoginIP")<>"" then
response.write rs("LastLoginIP")
else
response.write "&nbsp;"
end if
%> </td>
<td width="150" align="center"><%
if rs("LastLoginTime")<>"" then
response.write rs("LastLoginTime")
else
response.write "&nbsp;"
end if
%> </td>
<td width="60" align="center"><%
if rs("LoginTimes")<>"" then
response.write rs("LoginTimes")
else
response.write "0"
end if
%> </td>
<td width="100" align="center"><%
response.write "<a href='Admin_MangerUser.asp?Action=Modify&ID=" & rs("ID") & "'>修改</a>"
if iCount>1 and rs("UserName")<>session("Admin") then
	response.write "&nbsp;&nbsp;<a href='Admin_MangerUser.asp?Action=Del&ID=" & rs("ID") & "' onClick=""return confirm('确定要删除此管理员吗？');"">删除</a>"
end if
%></td>
</tr><%
rs.MoveNext
loop
%></table>
<%
rs.Close
set rs=Nothing
End sub
sub AddAdmin()
%>
<form method="post" action="Admin_MangerUser.asp" name="form1" class="required-validate">
<table border="0" cellspacing="1" cellpadding="0" width="100%" class="tabBgColor">
<tr>
<td class="tabheadfont" colspan="2">添加管理员</td>
</tr>
<tr>
<td width="100" align="right" height="27" class="tdbgFont">用户名：</td>
<td height="26" class="tdbg">
<input name="username" type="text" class="input_text required validate-alphanum"></td></tr><tr>
<td width="100" align="right" height="27" class="tdbgFont">初始密码：</td>
<td height="27" class="tdbg"><input id="Password" type="password" name="Password" class="input_text required"></tr><tr>
<td width="100" align="right" height="27" class="tdbgFont">确认密码：</td>
<td height="27" class="tdbg"><input id="PwdConfirm"type="password" name="PwdConfirm" class="input_text required validate-equals-Password"></td>
</tr>
<tr><td width="100" align="right" height="27" class="tdbgFont">权限设置：</td>
<td height="27" class="tdbg">
<select name="purview" id="purview" class="input_text">
<option value="1" selected>超级用户</option>
<option value="2">高级管理员</option>
<option value="3">信息录入员</option>
</select></td>
</tr>
<tr>
<td height="27" width="100" class="tdbgFont"><input name="Action" type="hidden" id="Action" value="SaveAdd"></td>
<td height="27" align="left" class="tdbg">
<input type="submit" name="Submit1" value=" 添 加 " class="button02-out"> 
<input class="button02-out" type="reset" value="复位" name="Submit"> 
<input class="button02-out" onclick="window.history.back()" type="button" value="返回" name="Submit"></td>
</tr>
</table>
</form>
<%
end sub
sub Modify()
dim UserID,Purview
UserID=trim(Request("ID"))
if UserID="" then
	FoundErr=True
	ErrMsg=ErrMsg & "·请指定要修改的管理员ID！\n"
	exit sub
else
	UserID=Clng(UserID)
end if
Dim sql
	sql="Select * from Easyes_Manage where ID=" & UserID
Dim rs
	Set rs=Server.CreateObject("Adodb.RecordSet")
rs.Open sql,conn,1,3
if rs.Bof and rs.EOF then
	FoundErr=True
	ErrMsg=ErrMsg & "·不存在此用户！\n"
else
	Purview=rs("Purview")
%><form method="post" action="Admin_MangerUser.asp" name="form2" class="required-validate">
<input name="ID" type="hidden" value="<%=rs("ID")%>">
<table width="100%" border="0" cellpadding="0" cellspacing="1" class="tabBgColor">
<tr>
<td class="tabheadfont" colspan="2">修改管理员</td>
</tr>
<tr>
<td width="100" align="right" height="26" class="tdbgFont">用户名：</td>
<td height="26" class="tdbgFont"><%=rs("UserName")%></td></tr>
<tr><td width="100" align="right" height="26" class="tdbgFont">初始密码：</td>
	<td height="26" class="tdbg"><input type="password" name="Password" class="input_text"><font color="#666666">如果不想修改，请保持为空</font></td></tr>
<tr><td width="100" align="right" height="26" class="tdbgFont">确认密码：</td>
	<td height="26" class="tdbg"><input type="password" name="PwdConfirm" class="input_text validate-equals-Password"></td></tr>
<tr>
<td width="100" align="right" height="26" class="tdbgFont">权限设置：</td>
<td height="26" class="tdbg"><select name="purview" id="purview" class="input_text">
<option value="1" <%if purview=1 then %>selected<% end if %>>超级用户</option>
<option value="2" <%if purview=2 then %>selected<% end if %>>高级管理员</option>
<option value="3" <%if purview=3 then %>selected<% end if %>>信息录入员</option>
</select></td>
</tr>
<tr><td height="26" width="100" class="tdbgFont"><input name="Action" type="hidden" id="Action" value="SaveModify">　</td>
	<td height="26" class="tdbg">
	<input  type="submit" name="Submit3" value=" 确 定 " class="button02-out"> 
	<input class="button02-out" type="reset" value="复位" name="Submit4"> 
	<input class="button02-out" onclick="window.history.back()" type="button" value="返回" name="Submit5"></td></tr>
</table>
</form>
<%
end if
rs.close
set rs=nothing
end sub
sub ShowHelp()
%><table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr><td colspan="2" bgcolor="#F6F6F6" style="cursor:hand" onClick="if(HelpTab.style.display=='none'){HelpTab.style.display='';window.scrollTo(window.pageXOffset,2000);}else{HelpTab.style.display='none'}" height="25">&nbsp;<img border="0" src="Images/help.gif" width="45" height="18"></td>
</tr><tr Id="HelpTab" style="display:none"> 
<td width="2%"></td><td width="98%" valign="top">
<strong>信息录入：</strong>只有添加
内容的权限。<strong><br>
高级管理：</strong>除上述权限之外，拥有内容审核和内容扩展功能的权限权限。<strong><br>
超级用户：</strong>拥有所有管理权限。</td>
</tr>
</table><%
End sub
%></body></html><%
sub SaveAdd()
Dim username, password,PwdConfirm,purview
username=trim(Request("username"))
password=trim(Request("Password"))
PwdConfirm=trim(request("PwdConfirm"))
purview=trim(Request("purview"))
if username="" then
	FoundErr=True
	ErrMsg=ErrMsg & "·用户名不能为空！\n"
end if
if password="" then
	FoundErr=True
	ErrMsg=ErrMsg & "·初始密码不能为空！\n"
end if
if PwdConfirm<>Password then
	FoundErr=True
	ErrMsg=ErrMsg & "·确认密码必须与初始密码相同！\n"
end if
if purview="" then
	FoundErr=True
	ErrMsg=ErrMsg & "·用户权限不能为空！\n"
else
	purview=CInt(purview)
end if
if FoundErr=True then
	exit sub
end if
Dim sql
	sql="Select * from Easyes_Manage where username='"&username&"'"
Dim rs
	Set rs=Server.CreateObject("Adodb.RecordSet")
rs.Open sql,conn,1,3
if not (rs.bof and rs.EOF) then
	FoundErr=True
	ErrMsg=ErrMsg & "·数据库中已经存在此管理员！\n"
	rs.close
	set rs=nothing
	exit sub
end if
password=md5(CfsEnCode(password))
rs.addnew
rs("username")=username
rs("password")=password
rs("purview")=purview
rs.update
rs.Close
set rs=Nothing
	Response.redirect "Admin_MangerUser.asp"
end sub

sub SaveModify()
dim UserID, UserName,password,PwdConfirm,Purview
UserID=trim(Request("ID"))
if UserID="" then
	FoundErr=True
	ErrMsg=ErrMsg & "·请指定要修改的管理员ID\n"
else
	UserID=Clng(UserID)
end if
password=trim(Request("Password"))
PwdConfirm=trim(request("PwdConfirm"))
purview=trim(Request("purview"))
if PwdConfirm<>Password then
	FoundErr=True
	ErrMsg=ErrMsg & "·确认密码必须与新密码相同！\n"
end if
if purview="" then
	FoundErr=True
	ErrMsg=ErrMsg & "·用户权限不能为空！\n"
else
	purview=CInt(purview)
end if
if FoundErr=True then
	exit sub
end if
Dim sql
	sql="Select * from Easyes_Manage where ID=" & UserID
Dim rs
	Set rs=Server.CreateObject("Adodb.RecordSet")
rs.Open sql,conn,1,3
if rs.Bof and rs.EOF then
	FoundErr=True
	ErrMsg=ErrMsg & "·不存在此用户！\n"
	rs.close
	set rs=nothing
	exit sub
end if
if Password<>"" then
	rs("password")=md5(CfsEnCode(password))
end if
	rs("purview")=purview
	rs.update
	rs.Close
	set rs=Nothing
	Response.redirect "Admin_MangerUser.asp"
end sub

sub DelAdmin()
Dim ComeUrl
	ComeUrl=Request.ServerVariables("HTTP_REFERER")
Dim UserID
UserID=trim(Request("ID"))
if UserID="" then
	FoundErr=True
	ErrMsg=ErrMsg & "·请指定要删除的管理员ID\n"
	exit sub
end if
	UserID=clng(UserID)
Dim sql
	sql="select * from Easyes_Manage where ID=" & UserID
Dim rs
	Set rs=Server.CreateObject("Adodb.RecordSet")
	rs.Open sql,conn,1,3
	rs.delete
	rs.close
	set rs=nothing
	Response.redirect ComeUrl
end sub
%>