﻿<%
'**************************************************************
' Software Name: EasyesESYS
' Web: http://www.Easyes.com.cn
'**************************************************************
'判断用户输入路径和提交连接地址
'**************************************************************
Dim ComeUrl,cUrl
ComeUrl=lcase(trim(request.ServerVariables("HTTP_REFERER")))
if ComeUrl="" then
	response.write "<center style=""color:#FF0000;font-weight:bold;font-size:14px;margin:50px;"">对不起，为了系统安全，不允许直接输入地址访问本系统的后台管理页面。</center>"
	response.end
else
	cUrl=trim("http://" & Request.ServerVariables("SERVER_NAME"))
	if mid(ComeUrl,len(cUrl)+1,1)=":" then
		cUrl=cUrl & ":" & Request.ServerVariables("SERVER_PORT")
	end if
	cUrl=lcase(cUrl & request.ServerVariables("SCRIPT_NAME"))
	if lcase(left(ComeUrl,instrrev(ComeUrl,"/")))<>lcase(left(cUrl,instrrev(cUrl,"/"))) then
		response.write "<center style=""color:#FF0000;font-weight:bold;font-size:14px;margin:50px;"">对不起，为了系统安全，不允许从外部链接地址访问本系统的后台管理页面。</center>"
		response.end
	end if
end if
'判断管理是否登陆
'**************************************************************
if Session("admin")="" then
	Response.write "<center style=""color:#FF0000;font-weight:bold;font-size:14px;margin:50px;"">已超时！请重新登录后台</center>"
	Response.end
else
	if session("purview")>PurviewLevel then
		Response.write "<center style=""color:#FF0000;font-weight:bold;font-size:14px;margin:50px;"">您没有操作的权限</center>"
		Response.end
	end if
end if
%>