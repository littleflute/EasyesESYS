﻿<%
'**************************************************************
' Software Name: EasyesESYS
' Web: http://www.Easyes.com.cn
'**************************************************************
Class FileSystemObject
	Public Function CheckFileExists(fName)
		CheckFileExists=false
		Dim FSO
		Set FSO=server.CreateObject(UpFileFSO)
		if FSO.FileExists(fName) then
			CheckFileExists=true
		end if
		Set FSO=Nothing
	End Function

	Public Function CreateFileStream(fileName,data,encoding)
		Dim objStream
		Set objStream = Server.CreateObject("ADODB.Stream") 
		With objStream 
			.Type = 2 'adTypeText 
			.Mode = 3 'adModeReadWrite 
			.Open 
			.Charset = encoding
			.Position = objStream.Size 
			.WriteText=data
			.SaveToFile fileName,2 'adSaveCreateOverWrite 
			.Close 
		End With
		Set objStream = NoThing
	End Function

	Public Function ReadFileStream(fileName,encoding)
		Dim objStream
		'On Error Resume Next
		Set objStream = Server.CreateObject("ADODB.Stream") 
		With objStream 
			.Type = 2 'adTypeText 
			.Mode = 3 'adModeReadWrite 
			.Charset = encoding
			.Open 
			.LoadFromFile fileName
			.Position = 0'objStream.Size 
			ReadFileStream=.ReadText
			.Close 
		End With
		Set objStream = NoThing
	End Function
	
	Public Function CreateNewFolder(fName)
		Dim FSO
		Set FSO = Server.CreateObject(UpFileFSO)
		if Not FSO.FolderExists(fName) then
			FSO.CreateFolder(fName)
		end if
		Set FSO=Nothing
		if Err then
			CreateNewFolder=false
		else
			CreateNewFolder=true
		end if
	End Function
	
	Public Function DeleteFile(fName)
		Dim FSO,F
		Set FSO=server.CreateObject(UpFileFSO)
		if FSO.FileExists(fName) then
			Set F=FSO.GetFile(fName)
			F.Delete
			Set F=Nothing
		end if
		Set FSO=Nothing
	End Function
End class
Dim FSO
	set FSO=new FileSystemObject
%>