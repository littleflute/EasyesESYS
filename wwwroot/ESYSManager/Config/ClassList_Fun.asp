﻿<%
'**************************************************************
' Software Name: EasyesESYS
' Web: http://www.Easyes.com.cn
'**************************************************************
'过程名：Channel;Admin_SpecialAdd;Admin_SpecialMdy
'作  用：系统模块设置
'****************************************************
Function Channel(Special)
Select Case Special
	Case 1
		response.write "单页模块"
	Case 2
		response.write "常规模块"
	Case 3
		response.write "产品模块"
	Case 4
		response.write "下载模块"		
	Case Else
		response.write "<font color=Red>模块出错</font>"
End Select
End function 

'过程名：Admin_SpecialAdd
'作  用：栏目列表 添加读取
Sub Admin_SpecialAdd()
	response.write "<option value="" "">请选择模块</option>" & vbcrlf
	response.write "<option value=""1"">单页模块</option>" & vbcrlf
	response.write "<option value=""2"">常规模块</option>" & vbcrlf
	response.write "<option value=""3"">产品模块</option>" & vbcrlf
	response.write "<option value=""4"">下载模块</option>" & vbcrlf
end sub

'过程名：Admin_SpecialMdy
'作  用：栏目列表 修改读取
Sub Admin_SpecialMdy(Special)
	response.write "<option value="" """
if Special="" Then Response.write"  selected"
	response.write ">请选择模块</option>" & vbcrlf
	response.write "<option value=""1"""
if Special=1 Then Response.write"  selected"
	response.write ">单页模块</option>" & vbcrlf
	response.write "<option value=""2"""
if Special=2 Then Response.write"  selected"
	response.write ">常规模块</option>" & vbcrlf
	response.write "<option value=""3"""
if Special=3 Then Response.write"  selected"
	response.write ">产品模块</option>" & vbcrlf	
	response.write "<option value=""4"""
if Special=4 Then Response.write"  selected"
	response.write ">下载模块</option>" & vbcrlf
End sub

'**************************************************
'函数名：GetClassPath1
'函数：取得当前ID所在树的路径----当前位置
'参数：当前节点ID,链接?后所带的参数值
'返回：字符串，（例：根类别 > 下载中心 > 网络相关 > 下载软件）
'**************************************************
Function GetClassPath1(ParentID)
    Dim Rs,Str
    Set Rs=Conn.ExeCute("Select ClassId,ParentID,ClassName From [Easyes_Class] Where ClassID="&ParentID&"")
	If Not(Rs.Eof And Rs.Bof) Then
		Str= " &gt; <a href=""?ParentID="&Rs("ClassID")&""">" & Rs("ClassName") & "</a>"
		Str=GetClassPath1(Rs("ParentID")) & Str
	Else
		Str="<a href=""?ParentID="&ParentID&""">根类别</a>" & Str			
	End If
	Rs.Close
	Set Rs=Nothing
	GetClassPath1=Str
End Function

'**************************************************
'函数名：GetClassPath2
'函数：取得当前ID所在树的栏目----当前栏目
'参数：当前节点ID,链接?后所带的参数值
'返回：字符串，（例：根类别 > 下载中心 | 网络相关 | 下载软件）
'**************************************************
Function GetClassPath2(ParentID,SpecialID)
Dim Sql
	Sql="Select ClassID,ClassName From [Easyes_Class] Where Special="&SpecialID&" and ParentID="&ParentID&" Order By UpTime"
Dim rs
	Set Rs=Conn.ExeCute(Sql)
If Rs.Eof And Rs.Bof Then
    Response.Write("<font color='#666666'>无子类别</font>")
End If
While Not Rs.Eof
    Response.Write("<a href='?ParentID="&Rs("ClassID")&"'>"&Rs("ClassName")&"</a>"&" | ")
    Rs.MoveNext
Wend
Rs.Close
End Function

'**************************************************
'//函数：递归搜当前目录下所有的下级目录（无限层型目录）
'//参数：栏目ID
'**************************************************
Function AllChildClass(ParentID)
    Dim Rs
    Set Rs=Conn.ExeCute("Select ClassID From [Easyes_Class] Where ParentID="&ParentID)
    While Not Rs.Eof
        AllChildClass=AllChildClass & "," & Rs("ClassID")
        AllChildClass=AllChildClass & AllChildClass(Rs("ClassID"))
        Rs.MoveNext
    Wend
    Rs.Close
    Set Rs=Nothing
End Function

'**************************************************
'函数名：CreateClassTree1
'作  用：生成栏目树---添加
'参  数：ParentId-----判断所在父级分类
'返回值：栏目列表
'**************************************************
Sub CreateClassTree1(ParentId,CuClassId,SpecialID)
    Dim Sql
        Sql="Select ClassID,ParentID,ClassName From [Easyes_Class] Where Special="&SpecialID&" and ParentID="&ParentId
    Dim Rs
    Set Rs=Conn.ExeCute(Sql)
    Dim radioSelected
    While Not Rs.Eof
        If CuClassId=Rs("ClassId") Then
            radioSelected="checked"
        Else
            radioSelected=""
        End If
        If Rs("ParentID")=0 Then
            Response.Write "    root1.CreateNode("&Rs("ClassID")&",-1,""<INPUT "&radioSelected&" TYPE=\""radio\"" NAME=\""radioBoxItem\"" value=\"""&Rs("ClassID")&"\"">"&Rs("ClassName")&""")" & vbCrLf
        Else
            Response.Write "    root1.CreateNode("&Rs("ClassID")&","&Rs("ParentID")&",""<INPUT "&radioSelected&" TYPE=\""radio\"" NAME=\""radioBoxItem\"" value=\"""&Rs("ClassID")&"\"">"&Rs("ClassName")&""")" & vbCrLf
        End If
        CreateClassTree1 Rs("ClassID"),CuClassId,SpecialID
    	Rs.MoveNext
    Wend
    Rs.Close
    Set Rs=Nothing
End Sub

'**************************************************
'函数名：CreateClassTree2
'作  用：生成栏目树---修改
'参  数：ParentId-----判断所在父级分类
'返回值：栏目列表
'**************************************************
Sub CreateClassTree2(ParentId,CuClassId,SpecialID)
    Dim Sql
        Sql="Select ClassID,ParentID,ClassName From [Easyes_Class] Where Special="&SpecialID&" and ParentID="&ParentId
    Dim Rs
    Set Rs=Conn.ExeCute(Sql)
    Dim radioSelected
    While Not Rs.Eof
        If CuClassID=Rs("ClassID") Then
            radioSelected="checked"
        Else
            radioSelected=""
        End If
    	If Rs("ParentID")=0 Then
            Response.Write "root2.CreateNode("&Rs("ClassID")&",-1,""<INPUT "&radioSelected&" TYPE=\""radio\"" NAME=\""radioBoxItem\"" value=\"""&Rs("ClassID")&"\"">"&Rs("ClassName")&""")" & vbCrLf
        Else
            Response.Write "root2.CreateNode("&Rs("ClassID")&","&Rs("ParentID")&",""<INPUT "&radioSelected&" TYPE=\""radio\"" NAME=\""radioBoxItem\"" value=\"""&Rs("ClassID")&"\"">"&Rs("ClassName")&""")" & vbCrLf
        End If
        CreateClassTree2 Rs("ClassID"),CuClassID,SpecialID
        Rs.MoveNext
    Wend
    Rs.Close
    Set Rs=Nothing
End Sub

'**************************************************
'取得当前Id所在树的路径---资源特性
'参数：当前节点Id,链接?后所带的参数值
'返回：字符串，（例：根类别 > 特性1 > 特性1-1 > 特性1-1-1）
'**************************************************
Function Spec_GetSpecialityPath(ID)
    Dim Sql,str
    Dim tId
        tId=Id
    Dim Counter
        Counter=0
    Dim StopRun
        StopRun=false
    Dim Rs
    Do
        Sql="Select Id,Title,ParentID From Easyes_Special Where ID="&tId
        Set Rs=Conn.ExeCute(Sql)
        If Not(Rs.Eof And Rs.Bof)Then
            str=" > <a href='?ParentID="&Rs("ID")&"'>"&Rs("Title")&"</a>"&str
            tId=CInt(Rs("ParentID"))
        Else
            StopRun=true
        End If
        Rs.Close
    Loop Until(StopRun Or tId=0)
    str="<a href='?Parent=0'>根类别</a>" & str
    Spec_GetSpecialityPath=str
End Function


'**************************************************
'函数名：SpecialityList_Add
'作  用：生成资源特性树--添加
'参  数：ParentId-----判断所在父级分类
'返回值：特性栏目
'**************************************************
Sub SpecialityList_Add(ParentId,SpecialID)
    Dim Sql
        Sql="Select * From [Easyes_Special] Where Special="&SpecialID&" and ParentID="&ParentId
    Dim Rs
    Set Rs=Conn.ExeCute(Sql)
    While Not Rs.Eof
        If Rs("ParentID")=0 Then
            Response.Write "root3.CreateNode("&Rs("Id")&",-1,""<INPUT TYPE=\""checkbox\"" NAME=\""Speciality\"" value=\"""&Rs("Id")&"\""><span onmouseover=\""showToolTip('"&Replace(Rs("Explain"),"""","'")&"',event.srcElement)\"" onmouseout=\""hiddenToolTip()\"">"&Rs("Title")&"</span>"")" & vbCrLf
        Else
            Response.Write "root3.CreateNode("&Rs("Id")&","&Rs("ParentID")&",""<INPUT TYPE=\""checkbox\"" NAME=\""Speciality\"" value=\"""&Rs("Id")&"\""><span onmouseover=\""showToolTip('"&Replace(Rs("Explain"),"""","'")&"',event.srcElement)\"" onmouseout=\""hiddenToolTip()\"">"&Rs("Title")&"</span>"")" & vbCrLf
        End If
        Call SpecialityList_Add(Rs("Id"),SpecialID)
        Rs.MoveNext
    Wend
    Rs.Close
    Set Rs=Nothing
End Sub

'**************************************************
'函数名：SpecialityList_Mdy
'作  用：生成资源特性树--资源特性
'参  数：ParentId-----判断所在父级分类
'返回值：特性栏目
'**************************************************
Sub SpecialityList_Mdy(ParentId,itemList,SpecialID)
    Dim Sql
        Sql="Select * From [Easyes_Special] Where Special="&SpecialID&" and ParentID="&ParentId
    Dim Rs
    Set Rs=Conn.ExeCute(Sql)
    Dim radioSelected
    While Not Rs.Eof
        If Instr(","&itemList&",",","&Rs("Id")&",")<>0 Then
            radioSelected="checked"
        Else
            radioSelected=""
        End If
        If Rs("ParentID")=0 Then
            Response.Write "root4.CreateNode("&Rs("Id")&",-1,""<INPUT "&radioSelected&" TYPE=\""checkbox\"" NAME=\""Speciality\"" value=\"""&Rs("Id")&"\""><span onmouseover=\""showToolTip('"&Replace(Rs("Explain"),"""","'")&"',event.srcElement)\"" onmouseout=\""hiddenToolTip()\"">"&Rs("Title")&"</span>"")" & vbCrLf
        Else
            Response.Write "root4.CreateNode("&Rs("Id")&","&Rs("ParentID")&",""<INPUT "&radioSelected&" TYPE=\""checkbox\"" NAME=\""Speciality\"" value=\"""&Rs("Id")&"\""><span onmouseover=\""showToolTip('"&Replace(Rs("Explain"),"""","'")&"',event.srcElement)\"" onmouseout=\""hiddenToolTip()\"">"&Rs("Title")&"</span>"")" & vbCrLf
        End If
        Call SpecialityList_Mdy(Rs("Id"),itemList,SpecialID)
        Rs.MoveNext
    Wend
    Rs.Close
    Set Rs=Nothing
End Sub

'**************************************************
'//【功能】标题样式设置
'//标题的颜色粗细
'**************************************************
Function TitleSytle(TitleFont,TitleColor,Title)
Select Case TitleColor
	Case 1
		Title="<strong>"&Title&"</strong>"
	Case 2
		Title="<em>"&Title&"</em>"
	Case 3
		Title="<strong><em>"&Title&"</em></strong>"
	Case Else
		Title=""&Title&""
End Select
If TitleFont<>"" Then
	Title="<span style=""color:"&TitleFont&""">"&Title&"</span>"
End If
 	TitleSytle=Title
End Function

'**************************************************
'//函数：产品编号的随即数(根据当时的时间值判断)
'//参数：空
'**************************************************
Sub ProductNum()
Dim DtNow,ranNum,GetNumString
	dtNow = Now()	
	ranNum = Int(900 * Rnd) + 100
	GetNumString = Year(dtNow) & Right("0" & Month(dtNow), 2) & Right("0" & Day(dtNow), 2) & Right("0" & Hour(dtNow), 2) & Right("0" & Minute(dtNow), 2) & Right("0" & Second(dtNow), 2) &ranNum
Response.write GetNumString
End Sub

'**************************************************
'//函数：下载地址判断
'//参数：空
'**************************************************
Function aShowDownloadUrls(DownloadUrls)
    Dim arrDownloadUrls, arrUrls, iTemp
    arrDownloadUrls = Split(DownloadUrls, "$$$")
    For iTemp = 0 To UBound(arrDownloadUrls)
        arrUrls = Split(arrDownloadUrls(iTemp), "|")
        If UBound(arrUrls) >= 1 Then
            If Left(arrUrls(1), 1) <> "/" And InStr(arrUrls(1), "://") <= 0 Then
                 NameDownUrls = NameDownUrls& "<a href='" & InstallDir & ChannelDir & "/" & UploadDir & "/" & arrUrls(1) & "'>" & arrUrls(1) & "</a><br>"
            Else
                strUrls = strUrls & "<a href='" & arrUrls(1) & "'>" & arrUrls(0) & "</a><br>"
                 NameDownUrls = NameDownUrls& "<a href='" & GetFirstSeparatorToEnd(arrDownloadUrls(iTemp), "|") & "'>" & GetFirstSeparatorToEnd(arrDownloadUrls(iTemp), "|") & "</a><br>"
            End If
        End If
    Next
	ShowDownloadUrls = NameDownUrls
End Function


'**************************************************
'//函数：下载地址判断
'//参数：DownloadUrls
'**************************************************
Function ShowDownloadUrls(DownloadUrls)
Dim arrDownloadUrls,iTemp,arrUrls,strUrls
	DownloadUrls = DownloadUrls
	arrDownloadUrls = Split(DownloadUrls, "@@@")  '依@@@分割为数组
For iTemp = 0 To UBound(arrDownloadUrls)   '读取依@@@的数组
	arrUrls = Split(arrDownloadUrls(iTemp), "|")  '重新分数组
	strUrls = strUrls & "<li><a href='" & arrUrls(1) & "' target=""_blank"">" & arrUrls(0) & "</a></li>"
Next
	ShowDownloadUrls = strUrls
End Function
%> 