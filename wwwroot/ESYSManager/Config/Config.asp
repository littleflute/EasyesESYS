﻿<%
Response.Charset="utf-8"
'///////////////////////////////////////////////////////////////////////////////////////////
'//                                     系统配制信息
'//                                 易讯工作室(2009-08-12)
'///////////////////////////////////////////////////////////////////////////////////////////

'============
'主要配制信息
'============
'网站名称
Const SiteName="易讯建站管理系统" 

'网站地址
Const SiteUrl="http://127.0.0.1"

'Session会话的保持时间，后台无操作有效时间
Const SessionTimeout=20

'发布资源时是否需要审核,"Yes”或"No”
Const AddChecked="No" 

'是否开放文件上传,"Yes”或"No”
Const EnableUploadFile="Yes"

'FileSystemObject对象名称
Const UpFileFSO="Scripting.FileSystemObject"

'上传文件大小限制，KB为单位。
Const MaxFileSize=200

'存放上传文件的目录
Const SaveUpFilesPath="/UploadFiles"

'允许上传文件类型
Const UpFileType="gif|jpg|doc|png"

'启用内容编辑器样式"1”是：Fckeditor全功能“2”是：Fckeditor精简功能
Const Editor=1

'是否开启远程文件保存到本地功能,"Yes”或"No”
Const sSaveFileSelect="Yes"

'远程图片保存目录,结尾请不要加“/”
Const sSaveFilePath="/UploadFiles" 

'远程读取文件类型
Const sFileExt="gif|jpg|doc|png"

'删除文章时，是否同时删除上传文件,"Yes”或"No”
Const DelUpFiles="Yes"
 
 '系统所在目录，结尾和开头必须加上"/","ESYSManager,ESYSPassport”目录必须再同一级目录中。
Const SiteFile="/" 

 '是否启用后台管理认证码 是:True 否:False
Const EnableSiteManageCode=True

 '后台管理认证码，这样即使有人知道了您的后台用户名和密码也不能登录后台
Const SiteManageCode="Easyes" 
%>