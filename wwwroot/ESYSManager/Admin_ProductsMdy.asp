﻿<%@language=vbscript codepage=65001%>
<%Option Explicit%>
<%
Response.CodePage=65001
Response.Charset="UTF-8"
Session.CodePage = 65001
'**************************************************************
' Software Name: EasyesESYS
' Web: http://www.Easyes.com.cn
'**************************************************************
Response.buffer=true	
Const PurviewLevel=2    '操作权限 1超级用户2高级管理员3信息录入员
%>
<!--#include file="Config/ChkPurview.asp"-->
<!--#include file="Config/Config.asp"-->
<!--#include file="Config/Conn.asp"-->
<!--#include file="Config/function.asp"-->
<!--#include file="Config/ClassList_Fun.asp"-->
<%
Select Case Request("Work")
    Case "DelReco"
        Call DelReco()
    Case "CheckReco"
        Call CheckReco()
    Case Else
End Select

'//////////////////
'//真实,虚拟删除
Sub DelReco()
Dim ComeUrl
	ComeUrl=Request.ServerVariables("HTTP_REFERER")
    Dim Id,IdList,FinishedNum
        FinishedNum=0
        Id=Request("Id")
        IdList=Split(Id,",",-1,1)
    Dim Sql,Rs
    Set Rs=Server.CreateObject("ADODB.RecordSet")
    Dim I
	Response.write "<meta http-equiv=""Content-Type"" content=""text/html; charset=gb2312""><script language=javascript>function r(s){$('curID').innerText=s;$('loadbg').style.width=250*(s/"&UBound(IdList)+1&");$('curLoad').innerText=Math.round((s/"&UBound(IdList)+1&")*100)+'%';}</script><div style='margin:20px;color:#484848;font-size:14px;line-height:25px;'><系统提示>此次操作预计删除（"&UBound(IdList)+1&"）条资料<BR>正在删除第 <div style='display:inline;color:red;' id=curID>1</div> 条...<div style='display:inline;color:#484848;;' id=curLoad></div><BR><div id=finish><img border=""0"" src=""images/Loading.gif"" width=""250"" height=""20"" /></div>"
	Response.Flush
    For I=0 To UBound(IdList)
            Sql="Select * From Easyes_Products Where ProductsID=" & CLng(IdList(I))
            Rs.Open Sql,Conn,1,3
            If Not(Rs.Eof And Rs.Bof) Then
    If DelUpFiles="Yes" Then   '删除物理删除文件
 		Dim n,aSavePathFileName
 		aSavePathFileName=RegExpExecute(rs("Content"))
 		aSavePathFileName= Split(aSavePathFileName, "|")
 		For n = 0 To UBound(aSavePathFileName)
 		   DeleteFile aSavePathFileName(i)
 		Next
 		   DeleteFile rs("Images")
 	End If
                Rs.Delete
                Rs.Update
                Rs.Close
                FinishedNum=FinishedNum+1
            End If
			Response.write "<script>document.getElementById('curID').innerHTML='"&(I+1)&"';</script>"
			Response.Flush
    Next
	Response.write "<script>document.getElementById('finish').innerHTML='删除操作已结束.';</script>"
    Response.Write("<script>alert(""<系统提示>\n此次操作预计操作（"&UBound(IdList)+1&"）条资料,实际成功操作（"&FinishedNum&"）条，未完成（"&(UBound(IdList)+1-FinishedNum)&"）条\n未完成的可能原因：\n1.管理员没有分配给你当前分类的操作权\n2.你不具有[删除/彻底删除/救回]操作权限"");window.location=""" & ComeUrl&""";</script>")
    Response.End
End Sub

'//////////////////
'//审核资源
Sub CheckReco()
Dim ComeUrl
	ComeUrl=Request.ServerVariables("HTTP_REFERER")
    Dim Id,IdList,FinishedNum
        FinishedNum=0
        Id=Request("Id")
        IdList=Split(Id,",",-1,1)
    Dim Sql,mRs
    Set mRs=Server.CreateObject("ADODB.RecordSet")
    Dim I
	Response.write "<meta http-equiv=""Content-Type"" content=""text/html; charset=gb2312""><script language=javascript>function r(s){$('curID').innerText=s;$('loadbg').style.width=250*(s/"&UBound(IdList)+1&");$('curLoad').innerText=Math.round((s/"&UBound(IdList)+1&")*100)+'%';}</script><div style='margin:20px;color:#484848;font-size:14px;line-height:25px;'><系统提示>此次操作预计审核（"&UBound(IdList)+1&"）条资料<BR>正在审核第 <div style='display:inline;color:red;' id=curID>1</div> 条...<div style='display:inline;color:#484848;;' id=curLoad></div><BR><div id=finish><img border=""0"" src=""images/Loading.gif"" width=""250"" height=""20"" /></div>"
	Response.Flush
	For I=0 To UBound(IdList)
            Sql="Select Passed From Easyes_Products Where ProductsID=" & CLng(IdList(I))
            mRs.Open Sql,Conn,1,3
            If Not(mRs.Eof And mRs.Bof) Then
                If CBool(mRs("Passed")) Then
                    mRs("Passed")=False
                Else
                    mRs("Passed")=True
                End If
                mRs.Update
                FinishedNum=FinishedNum+1
            End If
            mRs.Close
			Response.write "<script>document.getElementById('curID').innerHTML='"&(I+1)&"';</script>"
			Response.Flush
    Next
	Response.write "<script>document.getElementById('finish').innerHTML='审核操作已结束.';</script>"
    Response.Write("<script>alert(""<系统提示>\n此次操作预计审核（"&UBound(IdList)+1&"）条资料,实际成功审核（"&FinishedNum&"）条，未完成（"&(UBound(IdList)+1-FinishedNum)&"）条"");window.location=""" & ComeUrl&""";</script>")
    Response.End
End Sub
%>