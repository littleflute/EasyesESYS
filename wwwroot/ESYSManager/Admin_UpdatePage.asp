﻿<%@language=vbscript codepage=65001%>
<%Option Explicit%>
<%
Response.CodePage=65001
Response.Charset="UTF-8"
Session.CodePage = 65001
'**************************************************************
' Software Name: EasyesESYS
' Web: http://www.Easyes.com.cn
'**************************************************************
Response.buffer=true	
Const PurviewLevel=2    '操作权限 1超级用户2高级管理员3信息录入员
%>
<!--#include file="Config/ChkPurview.asp"-->
<!--#include file="Config/Config.asp"-->
<!--#include file="Config/function.asp"-->
<!--#include file="Config/Conn.asp"-->
<!--#include file="Config/ClassList_Fun.asp" -->
<!--#include file="Config/Class.FSO.asp"-->
<!--#include file="Label/StringClass.asp"-->
<!--#include file="Label/TemplateClass.asp"-->
<!--#include file="Label/MyLabel.asp"-->
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>执行高级标签</title>
<meta name="Version" content="<%=SystemVer%>">
<link href="Images/Style.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="Config/Button_Skin.js"></script>
</head>
<body>
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="tabTitleBar" height="26"><tr><td width="20" align="center"><img border="0" src="Images/file.png"></td><td>当前位置 > 执行高级标签</td></tr></table>
<table width="100%" border="0" cellpadding="0" cellspacing="1" class="tabBgColor">
<tr><td align="center" class="tabheadfont">执行高级标签（<span class="red">如果出现错误！请查看“高级标签管理”代码</span>）</td></tr>
<tr class="tdbg"><td bgcolor="#FFFFFF" class="tdbgFont"><%=UpdateLink()%></td></tr>
</table>
