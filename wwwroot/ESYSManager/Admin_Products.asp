﻿<%@language=vbscript codepage=65001%>
<%Option Explicit%>
<%
Response.CodePage=65001
Response.Charset="UTF-8"
Session.CodePage = 65001
'**************************************************************
' Software Name: EasyesESYS
' Web: http://www.Easyes.com.cn
'**************************************************************
Response.buffer=true	
Const PurviewLevel=3    '操作权限 1超级用户2高级管理员3信息录入员
%>
<!--#include file="Config/ChkPurview.asp"-->
<!--#include file="Config/Config.asp"-->
<!--#include file="Config/Conn.asp"-->
<!--#include file="Config/function.asp"-->
<!--#include file="Config/ClassList_Fun.asp"--><html>
<head>
<title>产品资源管理</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="Version" content="<%=SystemVer%>">
<link href="Images/Style.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="Config/Button_Skin.js"></script>
<script type="text/javascript" language=javascript src="Config/UpFils.js"></SCRIPT>
<script type="text/javascript" src="Config/ClassTree.js"></script>
<script type="text/javascript" src="Config/Tooltip.js"></script>
<script type="text/javascript" src="Config/InfoList.js"></script>
</head>
<body>
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="tabTitleBar" height="26"><tr><td width="20" align="center"><img border="0" src="Images/file.png"></td><td>当前位置 &gt; <a href="Admin_Products.asp">产品资源管理</a></td></tr></table>
<%
Dim Action,ParentID,i,FoundErr,ErrMsg
Action=trim(request("Action"))
if Action="Add" then
	call AddProducts()
elseif Action="SaveAdd" then
	call SaveAdd()
elseif Action="Modify" then
	call Modify()
elseif Action="SaveModify" then
	call SaveModify()
else
	call main()
end if
if FoundErr=True then
	call WriteErrMsg(ErrMsg)
end if
Call CloseConn()
Sub main()
%>
<input name="Submit32" type="button" class="button01-out" value="添加产品" onClick="window.location='Admin_Products.asp?Action=Add'" title="添加最新的产品资源信息">
<table width="100%" border="0" cellpadding="3" cellspacing="1" class="tabBgColor">
<tr> 
<td width="100%" bgcolor="#f6f6f6" class="tdbg">当前位置：<%
Dim ParentID
	ParentID=trim(request("ParentID"))
if ParentID="" then
	ParentID=0
else
	ParentID=CLng(ParentID)
end if
Dim sType
    sType=Replace(Request("sType"),"'","")
    If sType="" Then
        sType="Title"
    End If
Dim sKey
    sKey=jencode(Replace(Request("sKey"),"'",""))
	Response.write ""&GetClassPath1(ParentID)&""
%></td>
</tr>
<tr> 
<td width="100%" bgcolor="#f6f6f6" class="tdbg">
<%=GetClassPath2(ParentID,3)%></td>
</tr>
</table>
<%
Dim Rs
    Set Rs=Server.CreateObject("ADODB.RecordSet")
Dim Sql
    Sql="SELECT * From [All_Products] Where "&sType&" Like '%"&sKey&"%' And ClassID In ("& ParentID&AllChildClass(ParentID) &") Order By ProductsID DESC"
    Rs.PageSize=20
	Rs.CacheSize=Rs.PageSize
    Rs.Open Sql,Conn,1,1
Dim CurrentPage
    If Request("CurrentPage")="" Then
        CurrentPage=1
    Else
        CurrentPage=Request("CurrentPage")
    End If    
    If Not(Rs.Eof And Rs.Bof) Then
        Rs.AbsolutePage=CurrentPage
    End If
%>
<form name="form2" method="post" action="Admin_ProductsMdy.asp">
<table border="0" cellpadding="2" cellspacing="1" class="tabBgColor" width="100%">
<tr height="22"> 
<td align="center" class="tabheadfont" width="40">ID</td>
<td align="center" class="tabheadfont" width="30">选择</td>
<td align="center" class="tabheadfont">产品名称</td>
<td align="center" class="tabheadfont" width="40">点击</td>
<td align="center" class="tabheadfont" width="100">所属主类别</td>
<td align="center" class="tabheadfont" width="40">审核</td>
<td align="center" class="tabheadfont" width="40">操作</td>
</tr>
<%
  Dim I
  For I=1 To Rs.PageSize
      If Rs.Eof Then
        Exit For
    End If
%><tr class="tdbg"> 
<td align="center"><%=rs("ProductsID")%></td>
<td align="center"><input type="checkbox" name="Id" value="<%=Rs("ProductsID")%>" title="记录编号：<%=Rs("ProductsID")%>" HaveChecked="<%=CBool(Rs("Passed"))%>"></td>
<td><img border="0" src="Images/arrow_r.gif"><%Response.write "&nbsp;<a href='Admin_Products.asp?Action=Modify&ID=" & rs("ProductsID") &"' title='作者:"&Rs("Author")&"'>"&juncode(Rs("Title"))&"</a>"%> <label title="更新时间:<%=FormatDateTime(Rs("UpdateTime"),1)%>" style="cursor:hand"><font color="#808080">[<%=Rs("Date")%>]</td>
<td align="center"><%=Rs("Count")%></td>
<td align="center"><%=Rs("ClassName")%></td>
<td align="center"><%
    If CBool(Rs("Passed")) Then
        Response.write("<a href=""Admin_ProductsMdy.asp?Work=CheckReco&Id="& Rs("ProductsID") &""">已审</a>")
    Else
        Response.write("<a href=""Admin_ProductsMdy.asp?Work=CheckReco&Id="& Rs("ProductsID") &"""><font color='green'>未审</font></a>")
    End If
%></td><td align="center">
<%
response.write "<a href='Admin_Products.asp?Action=Modify&ID=" & rs("ProductsID") &"'>编辑</a>&nbsp;"
%></td>
</tr>
<%
  Rs.MoveNext
  Next
%></table>
<table width="100%" border="0" cellpadding="2" cellspacing="0">
<tr> 
<td align="right" bgcolor="#f6f6f6">
<script type="text/javascript" src="Config/PageList.js"></script>
<script type="text/javascript" language="JavaScript">
	<!--
	var plb = new PageListBar('plb',<%=Rs.PageCount%>,<%=CurrentPage%>,'&sType=<%=sType%>&sKey=<%=sKey%>&ParentID=<%=ParentID%>',20);
	document.write(plb);
	//-->
</script>
</td>
</tr>
<tr>
<td align="right" class="tdbg">
<input type="hidden" name="Work">
<input type="hidden" name="RealDel">
<label for="selectAllReco"> 
<input type="checkbox" name="checkbox2" value="checkbox" id="selectAllReco" onclick="selectAllCheckBox(form2.Id)">反选</label> 
<input name="Submit" type="button" class="button01-out" value="审 核" title="批量审核" onClick="CheckReco(form2,event.srcElement)">
<input name="Submit" type="button" class="button01-out" value="删 除" title="批量删除" onClick="DeleteReco(form2,event.srcElement)">
</td>
</tr>
</table>
</form>
<form name="form1" method="post" action="?">
<table width="100%" border="0" cellpadding="3" cellspacing="1" class="tabBgColor">
<tr><td width="67%" align="right" class="tdBg">
<input name="Work" type="hidden" id="Work" value="">搜索: 
<select name="sType" class="input_text">
<option value="Title">资源标题</option>
<option value="Author">作者名称</option>
</select><input name="ParentID" type="radio" class="Input" value="0" checked>所有 <input name="ParentID" type="radio" class="Input" value="<%=ParentID%>">当前 </td>
<td width="25%" align="right" class="tdBg"><input name="sKey" type="text" class="input_text" id="sKey" style="width:100%" value="<%=Trim(Request("sKey"))%>"></td>
<td width="5%" align="center" class="tdBg"><input name="SearchButton" type="submit" class="button02-out" value="确  定"></td>
</tr>
</table>
</form>
<%
Rs.Close
Set Rs=Nothing
End sub

Sub AddProducts()
	Call Ajax()
'添加资源开始
    Dim CurrentClassID,CurrentAuthor
    CurrentClassID=Request.Cookies("EasyesESys")("CurrentClassID")
    CurrentAuthor=Request.Cookies("EasyesESys")("CurrentAuthor")
    If Not IsNumeric(CurrentClassID) Then
        CurrentClassID = -1
    Else
        CurrentClassID = CLng(CurrentClassID)
    End If
%>
<form method="post" action="Admin_Products.asp" name="myform1" class="required-validate">
<input name="Action" type="hidden" id="Action" value="SaveAdd">
<table border="0" cellpadding="0" cellspacing="1" width="100%" class="tabBgColor">
<tr> 
<td colspan="2" align="center" class="tabheadfont">添加产品资源内容</td>
</tr>
<tr>
<td align="right" class="tdbgFont" valign="top">资源类别:</td>
<td height="25" class="tdbg" valign="top">
<table width="100%"  border="0" cellspacing="0" cellpadding="0">
<tr><td width="68%">
<script language="javascript">
var root1
root1=CreateRoot("myTree1","·请选择[资源类别]")
<%Call CreateClassTree1(0,CurrentClassID,3)%>
</script></td>
<td width="32%" align="right" valign="top">
<label for="CurrentClassID">
<input type="checkbox" id="CurrentClassID" name="CurrentClassID" value="1" <%If CurrentClassID<>-1 Then Response.Write "checked" End If%>><font color="#666666">记忆</font></label>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td align="right" class="tdbgFont">产品名称:</td>
<td height="25" class="tdbg"><input name="Title" type="text" size="51" class="input_text required" style="background-image: url('Images/Rule.gif')"></td>
</tr>
<tr>
<td align="right" class="tdbgFont">商品编号:</td>
<td height="25" class="tdbg"><input name="ProductNum" type="text" size="21" readonly class="input_text" value="<%Call ProductNum()%>">
</td>
</tr>
<tr>
<td align="right" class="tdbgFont">产品型号:</td>
<td height="25" class="tdbg"><input name="ProductsCode" type="text" size="35" class="input_text required"></td>
</tr>
<tr>
<td align="right" class="tdbgFont">产品价格:</td>
<td height="25" class="tdbg"><input name="Rmb" type="text" size="10" class="input_text required validate-number"> 人民币(仅输入数字)</td>
</tr>
<tr>
<td class="tdbgfont" align="right">资源作者:</td>
<td height="25" class="tdbgfont">
<table width="100%"  border="0" cellspacing="0" cellpadding="0">
<tr><td width="68%">
<input name="Author" type="text" size="15" class="input_text required" value="<%=CurrentAuthor%>">
<select name="font2" onChange="Author.value=this.value;" class="input_text">
<option value="" selected>选择作者</option>
<option value="佚名">佚名</option>
<option value="本站">本站</option>
<option value="不详">不详</option>
<option value="未知">未知</option>
</select></td>
<td width="32%" align="right" valign="top">
<label for="CurrentAuthor"><input type="checkbox" id="CurrentAuthor" name="CurrentAuthor" value="1" <%If CurrentAuthor<>"" Then Response.Write "checked" End If%>><font color="#666666">记忆</font></label>
</td>
</tr>
</table> 
</td>
</tr>
<tr>
<td align="right" class="tdbgFont" valign="top">资源特性:</td>
<td class="tdbg" valign="top">
<script>
var root3
root3=CreateRoot("myTree3","·请选择[资源特性]")
<%Call SpecialityList_Add(0,3)%>
</script>
</td>
</tr>
<tr> 
<td height="22" class="tdbgfont" bgcolor="#F6F6F6" colspan="2"><font color="#0000FF">资源内容</font>:<span style="cursor:hand" Id="ShowHiddenHtmlEdit" Title="显示/隐藏" onClick="if(trNewsContent.style.display==''){trNewsContent.style.display='none';ShowHiddenHtmlEdit.innerHTML='[显示]'}else{trNewsContent.style.display='';ShowHiddenHtmlEdit.innerHTML='[隐藏]'}">[隐藏]</span></td>
</tr>
<tr id="trNewsContent"> 
<td valign="top" colspan="2" class="tdbgFont"><%
Response.write"<textarea name=""Content"" style=""display:none""></textarea>"&chr(13)
call Admin_HtmlEditor()
%></td>
</tr>
<tr>
<td align="right" class="tdbgFont">产品图片:</td><td height="8" class="tdbg">
<input name="Images" type="text" size="39" class="input_text"> 
<input type="button" class="button01-out" value="上传" onClick="OpenWindowAndSetValue('UpFile/UpLoad.Asp',300,145,window,document.myform1.Images);"></td>
</tr>
<tr>
<td align="right" class="tdbgFont">产品简述:</td><td height="8" class="tdbg">
<textarea name="Text" rows="5" style="width:400;height:76" cols="20" class="input_text required"></textarea></td>
</tr>
<tr>
<td align="right" class="tdbgFont" width="85">
</td><td height="8" class="tdbg">
<input name="SaveAddButton" id="SaveAddButton" class="button02-out" type="submit" value=" 保 存 ">
<input name="Submit2" class="button02-out" type="reset" value=" 还 原 ">
<input name="Submit2" type="reset" class="button02-out" value="返 回" onClick="window.location.href='Admin_Products.asp'"></td>
</tr>
</table>
</form>
<%
End sub
sub Modify()
	Call Ajax()
Dim ID
ID=Request("ID")
Dim Sql
    Sql="Select * From [Easyes_Products] Where ProductsID=" & ID
Dim Rs
Set Rs=Conn.ExeCute(Sql)
%>
<form method="post" action="Admin_Products.asp" name="myform2" class="required-validate">
<input name="ID" type="hidden" id="ID" value="<%=Rs("ProductsID")%>">
<input name="Action" type="hidden" id="Action" value="SaveModify">
<table border="0" cellpadding="0" cellspacing="1" width="100%" class="tabBgColor">
<tr> 
<td colspan="2" align="center" class="tabheadfont">添加产品资源内容</td>
</tr>
<tr>
<td align="right" class="tdbgFont" valign="top">资源类别:</td>
<td height="25" class="tdbg" valign="top">
<script language="javascript">
var root2
root2=CreateRoot("myTree2","·请选择[资源类别]")
<%Call CreateClassTree2(0,Rs("ClassID"),3)%>
</script>
</td>
</tr>
<tr>
<td align="right" class="tdbgFont">产品名称:</td>
<td height="25" class="tdbg">
<input name="Title" type="text" size="51" class="input_text required" value="<%=juncode(Server.HTMLEncode(Rs("Title")))%>" style="background-image: url('Images/Rule.gif')"></td>
</tr>
<tr>
<td align="right" class="tdbgFont">商品编号:</td>
<td height="25" class="tdbg">
<input name="ProductNum" type="text" size="21" readonly class="input_text" value="<%=Rs("ProductNum")%>">
</td>
</tr>
<tr>
<td align="right" class="tdbgFont">产品型号:</td>
<td height="25" class="tdbg">
<input name="ProductsCode" type="text" size="35" class="input_text required" value="<%=Server.HTMLEncode(Rs("ProductsCode"))%>"></td>
</tr>
<tr>
<td align="right" class="tdbgFont">产品价格:</td>
<td height="25" class="tdbg">
<input name="Rmb" type="text" size="10" class="input_text required validate-number" value="<%=Rs("Rmb")%>"> 人民币(仅输入数字)</td>
</tr>
<tr>
<td align="right" class="tdbgFont" valign="top">资源作者:</td>
<td class="tdbg" valign="top">
<input name="Author" type="text" id="Author" size="15" class="input_text required" value="<%=juncode(Rs("Author"))%>">
<select name="font2" onChange="Author.value=this.value;" class="input_text">
<option value="" selected>选择作者</option>
<option value="佚名">佚名</option>
<option value="本站">本站</option>
<option value="不详">不详</option>
<option value="未知">未知</option>
</select>
</td>
</tr>
<tr>
<td align="right" class="tdbgFont" valign="top">资源特性:</td>
<td class="tdbg" valign="top">
<script>
var root4
root4=CreateRoot("myTree4","·请选择[资源特性]")
<%Call SpecialityList_Mdy(0,Rs("SpecialID"),3)%>
</script></td>
</tr>
<tr> 
<td height="22" class="tdbgfont" bgcolor="#F6F6F6" colspan="2"><font color="#0000FF">资源内容</font>:<span style="cursor:hand" Id="ShowHiddenHtmlEdit" Title="显示/隐藏" onClick="if(trNewsContent.style.display==''){trNewsContent.style.display='none';ShowHiddenHtmlEdit.innerHTML='[显示]'}else{trNewsContent.style.display='';ShowHiddenHtmlEdit.innerHTML='[隐藏]'}">[隐藏]</span></td>
</tr>
<tr id="trNewsContent"> 
<td valign="top" colspan="2" class="tdbgFont"><%
Response.write"<textarea name=""Content"" style=""display:none"">"&Server.HTMLEncode(Rs("Content"))&"</textarea>"&chr(13)
call Admin_HtmlEditor()
%></td>
</tr>
<tr>
<td align="right" class="tdbgFont">
产品图片:</td><td height="8" class="tdbg">
<input name="Images" type="text" size="39" class="input_text" value="<%=Rs("Images")%>"> 
<input type="button" class="button01-out" value="上传" onClick="OpenWindowAndSetValue('UpFile/UpLoad.Asp',300,145,window,document.myform2.Images);"></td>
</tr>
<tr>
<td align="right" class="tdbgFont">
产品简述:</td><td height="8" class="tdbg">
<textarea name="Text" rows="5" style="width:400 ; height:76" cols="20" class="input_text required"><%=Rs("Text")%></textarea></td>
</tr>
<tr>
<td align="right" class="tdbgFont" width="85">
</td><td height="8" class="tdbg">
<input name="SaveAddButton" id="SaveAddButton" class="button02-out" type="submit" value=" 保 存 ">
<input name="Submit2" class="button02-out" type="reset" value=" 还 原 ">
<input name="Submit2" type="reset" class="button02-out" value="返 回" onClick="window.location.href='Admin_Products.asp'"></td>
</tr>
</table>
</form>
<%
Rs.Close
Set Rs=Nothing
End Sub
%></body>
</html><%
sub SaveAdd()  '添加
Dim ClassID,Special,Title,ProductNum,ProductsCode,Rmb,Author,images,text,Content
	ClassID=Request.Form("radioBoxItem")
	Special=Replace(Request("Speciality")," ","")
	Title=trim(request.Form("Title"))
Title=jencode(Title) '日语处理
	ProductNum=request.Form("ProductNum")
	ProductsCode=trim(request.Form("ProductsCode"))
	Rmb=trim(request.Form("Rmb"))
	Author=trim(request("Author"))
Author=jencode(Author) '日语处理
	Images=trim(request("Images"))
	Text=trim(request("Text"))
	Content=trim(request("Content"))
	If ClassID="" Then
		FoundErr=True
		ErrMsg=ErrMsg & "·请设置[资源类别]！\n"
	End If
	if FoundErr=True then
		exit sub
	end if
	If sSaveFileSelect="Yes" Then
		Content=ReplaceRemoteUrl(Content,sSaveFilePath,sFileExt)
	End If
	Dim Sql
        Sql="Select Top 1 * From Easyes_Products"
    Dim Rs
    Set Rs=Server.CreateObject("ADODB.RecordSet")
        Rs.Open Sql,Conn,1,3
    rs.addnew
	rs("ClassID")=ClassID
	rs("SpecialID")=Special
	rs("Title")=Title
	rs("ProductNum")=ProductNum
	rs("ProductsCode")=ProductsCode
	rs("Rmb")=Rmb
    rs("Author")=Author
	rs("Content")=Content
	If Images="" Then
		rs("Images")=GetImg(RegExpExecute(Content))
	Else
		rs("Images")=Images
	End If
	rs("Text")=Text
If AddChecked="Yes" Then
	Rs("Passed")=False
Else
	Rs("Passed")=True
End If
   	rs("UpdateTime")=Now()
	rs.update
    rs.Close
    set rs=Nothing
'变量记录
    If Request("CurrentClassID")="1" Then
        Response.Cookies("EasyesESys")("CurrentClassID")=ClassID
    Else
        Response.Cookies("EasyesESys")("CurrentClassID")=""
    End If

    If Request("CurrentAuthor")="1" Then
        Response.Cookies("EasyesESys")("CurrentAuthor")=Author
    Else
        Response.Cookies("EasyesESys")("CurrentAuthor")=""
    End If
	Response.Redirect "Admin_Products.asp?ParentID="&ClassID&""  
end sub

sub SaveModify() '修改
Dim ID,ClassID,Special,Title,ProductNum,ProductsCode,Rmb,Author,images,text,Content
	ID=trim(request("ID"))
	ClassID=Request.Form("radioBoxItem")
	Special=Replace(Request("Speciality")," ","")
	Title=trim(request.Form("Title"))
Title=jencode(Title) '日语处理
	ProductNum=trim(request.Form("ProductNum"))
	ProductsCode=trim(request.Form("ProductsCode"))
	Rmb=trim(request.Form("Rmb"))
	Author=trim(request("Author"))
Author=jencode(Author) '日语处理
	Images=trim(request("Images"))
	Text=trim(request("Text"))
	Content=trim(request("Content"))
	If ClassID="" Then
		FoundErr=True
		ErrMsg=ErrMsg & "·请设置[资源类别]！\n"
	End If
	if FoundErr=True then
		exit sub
	end if
	If sSaveFileSelect="Yes" Then
		Content=ReplaceRemoteUrl(Content,sSaveFilePath,sFileExt)
	End If
Dim Sql
	sql="Select * From Easyes_Products Where ProductsID=" & ID
Dim rs
	Set rs=Server.CreateObject("Adodb.RecordSet")
	rs.open sql,conn,1,3
	if rs.bof and rs.EOF then
		FoundErr=True
		ErrMsg=ErrMsg & "·找不到指定的产品！\n"
		rs.close
	    set rs=Nothing
    else
		rs("ClassID")=ClassID
		rs("SpecialID")=Special
		rs("Title")=Title
		rs("ProductNum")=ProductNum
		rs("ProductsCode")=ProductsCode
		rs("Rmb")=Rmb
    	rs("Author")=Author
		rs("Content")=Content
	If Images="" Then
		rs("Images")=GetImg(RegExpExecute(Content))
	Else
		rs("Images")=Images
	End If	
		rs("Text")=Text
	If AddChecked="Yes" Then
		Rs("Passed")=False
	Else
		Rs("Passed")=True
	End If
    	rs("UpdateTime")=Now()
		rs.update
		rs.Close
		set rs=Nothing
	Response.Redirect "Admin_Products.asp?ParentID="&ClassID&""  
	end if
End sub
%>
