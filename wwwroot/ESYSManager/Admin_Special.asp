﻿<%@language=vbscript codepage=65001%>
<%Option Explicit%>
<%
Response.CodePage=65001
Response.Charset="UTF-8"
Session.CodePage = 65001
'**************************************************************
' Software Name: EasyesESYS
' Web: http://www.Easyes.com.cn
'**************************************************************
Response.buffer=true	
Const PurviewLevel=1    '操作权限 1超级用户2高级管理员3信息录入员
%>
<!--#include file="Config/ChkPurview.asp"-->
<!--#include file="Config/Config.asp"-->
<!--#include file="Config/Conn.asp"-->
<!--#include file="Config/function.asp"-->
<!--#include file="Config/ClassList_Fun.asp"--><html>
<head>
<title>特性管理</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="Version" content="<%=SystemVer%>">
<link href="Images/Style.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="Config/Button_Skin.js"></script>
</head>
<body>
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="tabTitleBar" height="26"><tr><td width="20" align="center"><img border="0" src="Images/file.png"></td><td>当前位置 &gt;  <a href="Admin_Special.asp">资源特性管理</a></td></tr></table><%
dim Action,ParentID,i,FoundErr,ErrMsg
Action=trim(request("Action"))
if Action="Add" then
	call AddSpecial()
elseif Action="SaveAdd" then
	call SaveAdd()
elseif Action="Modify" then
	call Modify()
elseif Action="SaveModify" then
	call SaveModify()
elseif Action="Del" then
	call DelSpecial()
elseif Action="UpOrder" then 
	call UpOrder() 
elseif Action="DownOrder" then 
	call DownOrder() 
else
	call main()
end if
if FoundErr=True then
	call WriteErrMsg(ErrMsg)
end if
call CloseConn()

Sub main()   '栏目列表
Dim ParentID
	ParentID=trim(request("ParentID"))
if ParentID="" then
	ParentID=0
else
	ParentID=CLng(ParentID)
end if
Dim Rs
    Set Rs=Server.CreateObject("ADODB.RecordSet")
Dim Sql
    Sql="SELECT * From [Easyes_Special] Where ParentID="&ParentID&" Order By ID desc"
    Rs.PageSize=20
	Rs.CacheSize=Rs.PageSize
    Rs.Open Sql,Conn,1,1
Dim CurrentPage
    If Request("CurrentPage")="" Then
        CurrentPage=1
    Else
        CurrentPage=Request("CurrentPage")
    End If    
    If Not(Rs.Eof And Rs.Bof) Then
        Rs.AbsolutePage=CurrentPage
    End If
Dim sKey,WorkType
%>
<input name="Submit32" type="button" class="button01-out" value="添加特性" onClick="window.location='Admin_Special.asp?Action=Add&ParentID=<%=ParentID%>'" title="在当前位置添加新特性">
<table border="0" cellpadding="2" cellspacing="1" class="tabBgColor" width="100%"><tr><td colspan="5" class="tdbg"><%=Spec_GetSpecialityPath(ParentID)%> 的子特性列表：</td></tr>
<tr class="tabHead"> 
<td class="tabheadfont" width="60">记录ID</td>
<td class="tabheadfont" width="120">资源所属模块</td>
<td class="tabheadfont">资源特性名称</td>
<td class="tabheadfont" width="100">更新时间</td>
<td class="tabheadfont" width="100">常规操作</td></tr>
<%
  Dim I
  For I=1 To Rs.PageSize
      If Rs.Eof Then
        Exit For
    End If
%>
<tr class="tdbg"> 
<td align="center" style="background-color: #FBFCFF"><%=rs("ID")%></td>
<td align="center"><%Call Channel(rs("Special"))%></td>
<td><a href="?ParentID=<%=Rs("ID")%>"><img border="0" src="Images/arrow_r.gif">&nbsp;<%=rs("Title")%></a></td>
<td align="center"><%=FormatDateTime(Rs("upTime"),2)%></td>
<td align="center"><%
	response.write "&nbsp;&nbsp;<a href='Admin_Special.asp?Action=Modify&ClassID=" & rs("ID") & "'>修改</a>&nbsp;&nbsp;"
	response.write "<a href='Admin_Special.asp?Action=Del&ClassID=" & rs("ID") & "' onClick=""return confirm('确定要删除此栏目吗？');"">删除</a>&nbsp;&nbsp;"
%></td>
</tr>
<%
  Rs.MoveNext
  Next
%></table> 
<table border="0" cellpadding="2" cellspacing="0" width="100%" width="24">
<tr> 
<td align="right" bgcolor="#f6f6f6">
<script type="text/javascript" src="Config/PageList.js"></script>
<script type="text/javascript" language="JavaScript">
	<!--
	var plb = new PageListBar('plb',<%=Rs.PageCount%>,<%=CurrentPage%>,'&ParentID=<%=ParentID%>',20);
	document.write(plb);
	//-->
</script>
</td>
</tr>
</table>
<%
Rs.Close
Set Rs=Nothing
end sub

sub AddSpecial()
Call ajax()
ParentID=trim(request("ParentID"))
if ParentID="" then
	ParentID=0
else
	ParentID=CLng(ParentID)
end if
%><form name="form1" method="post" action="Admin_Special.asp" class="required-validate">
<table class="tabBgColor" cellspacing="1" width="100%">
<tr> 
<td class="tabheadfont" colspan="2">添加资源特性</td>
</tr>
<tr> 
<td align="right" class="tdbgfont">详细位置：</td><td class="tdbg"><%=Spec_GetSpecialityPath(ParentID)%></td>
</tr>
<tr> 
<td align="right" class="tdbgfont">特性名称：</td>
<td class="tdbg"><input name="Title" class="input_text required" type="text" size="37"></td>
</tr>
<%If ParentID=0 Then%><tr> 
<td align="right" class="tdbgfont">特性模块：</td>
<td class="tdbg">
<select name="Special" class="input_text required">
<%call Admin_SpecialAdd()%></select>
</td>
</tr><%Else%><input name="Special" type="hidden" id="Special" value="<%=Request("Special")%>">
<%End If%><tr> 
<td align="right" class="tdbgfont">特性简介：</td>
<td class="tdbg">
<textarea name="Explain" class="input_text required" id="remark" style="width:300; height:80"></textarea></td>
</tr>
<tr> 
<td align="center" class="tdbg">
<input name="ParentID" type="hidden" id="ParentID" value="<%=Request("ParentID")%>">
<input name="Action" type="hidden" id="Action" value="SaveAdd"></td>
<td class="tdbg">
<input name="SaveAddButton" id="SaveAddButton" class="button02-out" type="submit" value=" 保 存 ">
<input name="Submit2" class="button02-out" type="reset" value=" 还 原 ">
<input name="Submit2" type="reset" class="button02-out" value="返 回" onClick="window.location.href='Admin_Special.asp'"></td>
</tr>
</table>
</form><%
End sub
sub Modify()
Call ajax()
	dim ClassID
	ClassID=trim(request("ClassID"))
	if ClassID="" then
		FoundErr=True
		ErrMsg=ErrMsg & "·请指定要修改单页栏目ID\n"
		exit sub
	else
		ClassID=Clng(ClassID)
	end if
Dim rs
	set rs=conn.execute("select * from [Easyes_Special] where ID=" & ClassID)
	If rs.bof and rs.eof then
		FoundErr=True
		ErrMsg=ErrMsg & "·找不到指定的栏目！\n"
	Else
%>
<form method="post" action="Admin_Special.asp" name="form2" class="required-validate">
<input name="ParentID" type="hidden" id="ParentID" value="<%=rs("ParentID")%>">
<input name="ID" type="hidden" id="ID" value="<%=rs("ID")%>">
<input name="Action" type="hidden" id="Action" value="SaveModify">
<table class="tabBgColor" cellspacing="1" width="100%">
<tr>
<td align="right" class="tabheadfont" colspan="2">编辑资源特性</td></tr>
<tr>
<td align="right" class="tdbgfont">详细位置：</td><td class="tdbg"><%=Spec_GetSpecialityPath(rs("ParentID"))%></td></tr>
<tr>
<td align="right" class="tdbgfont">特性名称：</td><td class="tdbg">
<input name="Title" class="input_text required" type="text" size="37" value="<%=rs("Title")%>"></td></tr>
<%If rs("ParentID")=0 Then%><tr> 
<td align="right" class="tdbgfont">所属模块：</td>
<td class="tdbg">
<select name="Special" class="input_text required">
<%call Admin_SpecialMdy(rs("Special"))%></select>
</td>
</tr>
<%End If%>
<tr>
<td align="right" class="tdbgfont">特性简介：</td><td class="tdbg">
<textarea name="Explain" class="input_text required" id="remark0" style="width:300; height:80" rows="1" cols="20"><%=rs("Explain")%></textarea></td></tr>
<tr><td align="right" height="24" class="tdbg"></td>
<td height="24" class="tdbg">
<input name="SaveAddButton" id="SaveAddButton" class="button02-out" type="submit" value=" 保 存 ">
<input name="Submit2" class="button02-out" type="reset" value=" 还 原 ">
<input name="Submit2" type="reset" class="button02-out" value="返 回" onClick="window.location.href='Admin_Special.asp'">
</td>
</tr>
</table>
</form>
<%
End if
	rs.close
	set rs=nothing
end sub
%></body>
</html>
<%
sub SaveAdd()
	dim ParentID,Title,Explain,Special
	ParentID=trim(CLng(Request("ParentID")))
If ParentID=0 Then
	Special=trim(request.Form("Special"))
	if Special="" then
		FoundErr=True
		ErrMsg=ErrMsg & "·请选择所属模块！\n"
	end if
End If
	Title=trim(request.Form("Title"))
	Explain=trim(request.Form("Explain"))
	if ParentID="" then
		FoundErr=True
		ErrMsg=ErrMsg & "·特性分类为不能空！\n"
	end if
	if Title="" then
		FoundErr=True
		ErrMsg=ErrMsg & "·请输入[特性名称]！\n"
	end if
	if Explain="" then
		FoundErr=True
		ErrMsg=ErrMsg & "·请输入[特性简介]！\n"
	end if
	if FoundErr=True then
		exit sub
	end if
'获得根ID和根的特性
    Dim RsRoot,RootID
    set RsRoot= server.CreateObject ("adodb.recordset")
   	sql="Select ID,Special From [Easyes_Special] Where ID="&ParentID
	RsRoot.Open sql,conn,1,1
	if RsRoot.eof or RsRoot.bof then
        RootID=0
	Else
		RootID=RsRoot("ID")
	If ParentID<>0 Then
		Special=RsRoot("Special")
	End If
    End If
	RsRoot.close
	set RsRoot=nothing
'入库	
    Dim Sql
        Sql="Select Top 1 * From [Easyes_Special] Order By ID DESC"
    Dim Rs
    Set Rs=Server.CreateObject("ADODB.RecordSet")
        Rs.Open Sql,Conn,1,3
    Rs.AddNew
    Rs("ParentID")=ParentID
    Rs("Title")=Title
    Rs("Explain")= Explain
    Rs("RootID")=RootID
    Rs("Special")=Special
    Rs("upTime")= Now
    Rs.Update
    Rs.Close
    Set Rs=Nothing
    Response.Redirect("Admin_Special.asp?ParentID="&ParentID&"")
End sub

sub SaveModify()
	dim ParentID,Title,Explain,ID,Special
	ID=trim(CLng(Request("ID")))
	ParentID=trim(CLng(Request("ParentID")))
	Title=trim(request.Form("Title"))
	Explain=trim(request.Form("Explain"))
	if ParentID="" then
		FoundErr=True
		ErrMsg=ErrMsg & "·特性分类为不能空！\n"
	end if
If ParentID=0 Then
	Special=trim(request.Form("Special"))
	if Special="" then
		FoundErr=True
		ErrMsg=ErrMsg & "·请选择所属模块！\n"
	end if
End If
	if Title="" then
		FoundErr=True
		ErrMsg=ErrMsg & "·请输入[特性名称]！\n"
	end if
	if Explain="" then
		FoundErr=True
		ErrMsg=ErrMsg & "·请输入[特性简介]！\n"
	end if
	if FoundErr=True then
		exit sub
	end if
    Dim Sql
        Sql="Select * From [Easyes_Special] Where ID="&ID
    Dim Rs
    Set Rs=Server.CreateObject("ADODB.RecordSet")
        Rs.Open Sql,Conn,1,3
    If Rs.Eof And Rs.Bof Then
        Rs.Close
        Set Rs=Nothing
        FoundErr=True
		ErrMsg=ErrMsg & "·修改[特性]，记录不存在\n"
		Exit sub
    End If
    	Rs("ParentID")=CLng(Request("ParentID"))
    	Rs("Title")= Trim(Request("Title"))
    	Rs("Explain")= Trim(Request("Explain"))
        Rs("upTime")= Now
    If ParentID=0 Then
   		Rs("Special")=Special
	End If
        Rs.Update
'判断是否更新根目录下的所有特性
If ParentID=0 Then
Dim RootID
    RootID=Rs("ID")
    conn.Execute "update [Easyes_Special] set Special="&Special&" Where RootID=" & Clng(RootID)
End If
    rs.Close
    set rs=Nothing
    Call CloseConn() 
    Response.Redirect("Admin_Special.asp?ParentID="&ParentID&"")
end sub

sub DelSpecial()
	dim ClassID,ComeUrl
	ComeUrl=Request.ServerVariables("HTTP_REFERER")
	ClassID=trim(request("ClassID"))
	if ClassID="" then
        FoundErr=True
		ErrMsg=ErrMsg & "·删除[特性]，没有找到！\n"
		Exit sub
	else
		ClassID=Clng(ClassID)
	end if
Dim Sql
    Sql="Select Count(*) As Num From [Easyes_Special] Where ParentID="&ClassID
Dim Rs
    Set Rs=Conn.ExeCute(Sql)
    If Rs("Num")>=1 Then
        FoundErr=True
		ErrMsg=ErrMsg & "·其下还包含有特性，无法删除！\n"
		Exit sub
    Else
        Sql="Delete From Easyes_Special Where Id="&ClassID
        Conn.ExeCute(Sql)
    End If
	Response.redirect ComeUrl
end sub
%>