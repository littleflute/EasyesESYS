﻿<%@language=vbscript codepage=65001%>
<%Option Explicit%>
<%
Response.CodePage=65001
Response.Charset="UTF-8"
Session.CodePage = 65001
'**************************************************************
' Software Name: EasyesESYS
' Web: http://www.Easyes.com.cn
'**************************************************************
Response.buffer=true	
Const PurviewLevel=1    '操作权限
%>
<!--#include file="Config/ChkPurview.asp"-->
<!--#include file="Config/Config.asp"-->
<!--#include file="Config/Conn.asp"-->
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>管理首页</title>
<meta name="Version" content="<%=SystemVer%>">
<link href="Images/Style.css" rel="stylesheet" type="text/css">
<style type="text/css">
.style1 {
	text-decoration: underline;
	color: #0000FF;
}
.style2 {
	color: #0000FF;
}
</style>
</head>
<body>
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="tabTitleBar" height="26"><tr><td width="20" align="center"><img border="0" src="Images/file.png"></td><td>当前位置 &gt; 系统帮助</td></tr></table>
<table width="100%" cellpadding="3" cellspacing="1" class="tabBgColor">
<tr><td class="tdbgFont"><img border="0" src="Images/arrow_r.gif" width="7" height="7"><b>最新版验证</b></td></tr>
<tr><td valign="top" class="tdbg"><%
'官方最新版本验证。(为了您的系统保存最新版本更新,防止安全漏洞，请不要删除一下代码。)
Response.write "<script src=""http://www.Easyes.com.cn/V4/VersionUTF8.asp?Version=Vip&Ver="&SystemVer&"""></script>"
%></td></tr>
<tr> <td valign="top" class="tdbg">官方网站：<a href="http://www.easyes.com.cn" target="_blank">http://www.easyes.com.cn</a></td></tr>
</table>
<table width="100%" cellpadding="3" cellspacing="1" class="tabBgColor">
<tr> 
<td class="tdbgFont"><img border="0" src="Images/arrow_r.gif" width="7" height="7"><b> 重要提醒</b></td></tr>
<tr> 
<td valign="top" class="tdbg">
1、 复制易讯建站管理系统到你的站点目录（强烈建议根目录）<br>&nbsp;&nbsp;&nbsp; 易讯建站管理系统包含4个系统目录：<span class="style1">ESYSManager、ESYSPassport、ESYSDataBase、UploadFiles</span>。删除和修改目录，可能引起系统出现未知错误。如果要更改系统目录，<span class="style1">ESYSManager、ESYSPassport</span>必须在同一级目录中，<span class="style1">ESYSDataBase、UploadFiles</span>可根据配置需要随意修改。<br>
2、 后台管理目录：ESYSManager/Admin_Login.asp。<br>
3、 使用前请修改数据库连接路径。数据库路径修改：ESYSManager/Confie/Conn.asp</td>
</tr>
</table>
<table width="100%" cellpadding="3" cellspacing="1" class="tabBgColor">
<tr> 
<td class="tdbgFont"><img border="0" src="Images/arrow_r.gif" width="7" height="7"><b> 建站帮助</b></td></tr>
<tr> 
<td valign="top" class="tdbg">
<strong>一、模板设计和标签调用须知<br></strong>1、模板必须是以ASP(根据设计要求有些页面支持HTML)为扩展名。<br>
2、每个单独的ASP文件开头必须加入，系统资源库文件调用，HTML扩展名模板除外。<br>&nbsp;&nbsp;&nbsp; 
<span class="style1">&lt;!--#Include 
File=&quot;ESYSPassport/Template.asp&quot;--&gt;</span><br>
3、模板设计完成后，开始调用标签。模板文件中使用<span lang="EN-US"><span class="style1">&lt;%=EsysLable(ID)%&gt;</span>调用标签</span>。“<span class="style1">ID</span>”等于“网站标签设置”中标签的“<span class="style1">ID</span>”<br>
二<strong>、标签设置说明<br></strong>1、栏目标签：<span class="style2">不选择包含子分类</span>，调用的是选择分类下的所有一级分类。<span class="style2">选择包含子分类</span>，调用的是选择分类下的所有下级分类。建议不选择包含子分类。<br>
2、版块标签和翻页标签：<span class="style2">不选择分类</span>，调用本模块下您限定的资源（最新，热点等）。<span class="style2">选择分类</span>，调用选择分类下您限定的资源，<span class="style2">不选择包含子分类</span>，调用的是选择分类下的所有限定资源。<span class="style2">选择包含子分类</span>，调用的是选择分类下的所有下级分类的资源。<br>
3、HTML元素盒：所在标签下资源最外层的HTML标签。如：<span class="style1">&lt;table&gt;&lt;/table&gt;</span><br>
4、HTML元素：所在标签下资源最外层标签内的子标签。如：&lt;table&gt;<span class="style1">&lt;tr&gt;&lt;/tr&gt;</span>&lt;/table&gt;<br>
5、循环HTML元素：所在标签下资源最外层标签内的子标签的显示次数。如循环2次：&lt;table&gt;<span class="style1">&lt;tr&gt;&lt;/tr&gt;&lt;tr&gt;&lt;/tr&gt;</span>&lt;/table&gt;<br>
<strong>三、标签重要输入说明<br></strong>1、HTML元素输入方法：“<span class="style2">&lt;</span>”“<span class="style2">&gt;</span>”必须替换为“<span class="style2">{</span>”“<span class="style2">}</span>”。“<span class="style2">&quot;</span>”双引号必须替换为“<span class="style2">(</span>”“<span class="style2">)</span>”例：<span class="style1">始:{table 
class=(css)} 尾:{/table}</span></td>
</tr>
</table>
<table width="100%" cellpadding="4" cellspacing="1" class="tabBgColor">
<tr>
<td class="tdbgFont"> 
<img border="0" src="Images/arrow_r.gif" width="7" height="7"><b> 易讯信息发布系统授权说明</b></td>
</tr>
<tr>
<td width="91%" bgcolor="#FFFFFF" class="tdbg"> 
<table class="tableBorder" cellspacing="1" cellpadding="3" width="100%" border="0" bgcolor="#CCCCCC">
<tr>
<td width="68" height="23" bgcolor="#F4F5F6" class="tdbgFont">当前版本</td>
<td bgcolor="#F4F5F6" class="tdbg"><strong style="color: blue">易讯建站管理系统:EasyesESYS <%=SystemVer%></strong></td>
</tr>
<tr>
<td height="23" bgcolor="#FFFFFF" class="tdbgfont">版权声明</td>
<td style="line-height: 150%" bgcolor="#FFFFFF" class="tdbg">　1、易讯建站管理系统，英文名称为&quot;EasyesESYS&quot;，系易讯网络(Easyes.com.cn)独立开发，并受中华人民共和国法律保护<br>
　2、本软件为商业授权软件,未经书面授权，不得向任何第三方提供本系统；如需用于商业，必须注册或购买本系统商业版本<br />
　2、用户自由选择是否使用,在使用中出现任何问题和由此造成的一切损失作者将不承担任何责任<br />
　4、您可以对本系统进行修改和美化，但必须保留完整的版权信息<br />
　5、本软件受中华人民共和国《着作权法》《计算机软件保护条例》等相关法律、法规保护，作者保留一切权利</td>
</tr>
</table>
</td>
</tr>
</table>
</body>
</html>