﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Content-Language" content="zh-cn" />
<meta name="author" content="EasyesESYS" />
<meta name="Copyright" content="Easyes.com.cn" />
<meta name="Description" content="易讯网络国内顶级企业建站软件开发商， 是目前中国ASP建站综合服务提供商之一。" />
<title>易讯建站管理系统(EasyesESYS)-http://www.Easyes.com.cn</title>
<link href="Images/about.css" rel="stylesheet" type="text/css" media="screen" />
</head>
<body>
<div id="main">
<!--#include file="Head.asp"-->
<div class="banner"><img alt="易讯建站系统" src="Images/About.jpg" /></div>
<div class="wrap">
<div class="sidebar">
<dl class="list_menu">
<dt>关于我们</dt>
<dd class="on">公司简介</dd>
<dd><a href="About1.asp">联系我们</a></dd>
<dd><a href="About2.asp">招贤纳士</a></dd>
<dd><a href="About3.asp">企业文化</a></dd>
</dl>
</div>
<div class="const_main">
<div class="gb_mode_title"><h3>公司简介</h3><div class="fun_r"><a href="#">更多</a></div></div>
<div class="main_txt">
<!--公司简介:开始-->
<p>创新工场(<a href="http://www.innovation-works.com">www.innovation-works.com</a>)由李开复博士创办于2009年9月，旨在帮助中国青年成功创业。</p>
<p>创新工场是一个全方位的创业平台，旨在培育创新人才和新一代高科技企业。创新工场将吸引一批优秀创业者和工程师，在李开复博士的带领下，开创出最有市场价值和商业潜力的项目，进行研发和市场运营。当项目成熟到一定程度后，自然剥离母体成为独立子公司，直至最后上市或被收购。创新工场将立足信息产业最热门领域：互联网、移动互联网和云计算，并选择相关技术作为创业的起点。</p>
<p>创新工场在未来五年将投入8亿人民币。投资牵头者为刘宇环先生创立的中经合集团，投资者还包括财富100强企业、知名创投和中美精英人士，其中有郭台铭领导的富士康科技集团、柳传志领导的联想控股有限公司、俞敏洪领导的新东方教育科技集团、YouTube创始人陈士骏等。这些已经是成功传奇的明星创业者中，很多表示愿意共同辅导青年创业者，他们的加入使创新工场如虎添翼，他们的参与将使创业精神在一批批创业者中薪火相传。<br />
<br />
<br />
&nbsp;</p>
<!--公司简介:结束-->
</div>			
</div>
</div>
<!--#include file="Bottom.asp"-->
</div>
</body>
</html>
