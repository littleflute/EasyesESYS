#EasyesESYS V4.8_UTF8
2005年开发的一个ASP企业建站CMS系统,满足中小企业快速建站需求.

----

#环境要求：
环境:WinNT IIS ASP
数据库:Access

----

#浏览器支持：
前台自行兼容开发，后台最少IE6+ Chrome Firefox 

后台因特性问题暂不支持IE8以下浏览器，请升级您的浏览器

----

#安装

目前未开发安装向导，请自行修改 ESYSManager/Config/conn.asp的配置和导入数据

----

#开发支持

开发团队：Pillar (QQ:81760805)